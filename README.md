# HoloNinja

### Summary

Using the Windows Device Portal, this desktop application can simultaneously connect to any number of Microsoft HoloLens devices. Once connected to a device, the user can add it to the "Selected Devices" list, which allows for the first six selected devices to display a live stream Mixed Reality Capture (MRC) and for all selected devices to receive commands to launch or kill a particular application on the device. This application is intended for use in HoloLens demonstration settings, allowing the demonstrator to easily launch a demonstration app, monitor what each HoloLens sees (including holograms), remotely capture images and videos, and track battery life on each connected device, making the information and functionality available at the separate online Windows Device Portals for each device into one streamlined user interface to improve device management and demonstrations.

#### Connecting to a Device
In the top left corner, enter the device's IP address (in the format `http://[IP Address]`) and the username and password for the device, then click "Connect". Note that even for devices for which a username and password have not been set, the username and password fields must be filled, so enter any random combination of characters into each box and the connection will proceed. Connected devices are initially listed under "Available Devices" until they are selected for management by the user.

###### Saving Device Information
Once at least one device has been connected to HoloNinja, the user has the option to save the address, username, and password for the connected device. Clicking "Save" will create a text file listing the device information and will open a save dialog to allow the user to modify the filename and save location. NOTE: This text file lists the username and password of each device in an unencrypted, human-readable .txt file, so use caution when saving or sharing this file to keep passwords secure. A saved device information file can also be created manually by creating a .txt file with each device's information listed in on its own line of the file, in the following format: `;[IP Address];[username];[password];`. The IP Address should NOT include `"http://"`.

###### Loading Device Information
A saved devices file can be loaded at any time using the "Load" button. This will open a file selection dialog to allow the user to select a .txt file of saved device information. The file is parsed and the devices are added automatically to the "Available Devices" list. Additional devices can be connected using the regular log-in functionality. If a device in a saved device file has already been connected manually, it will be skipped and the other devices in the saved device file will be added as normal.

#### Available and Selected Devices
Connected devices are listed under "Available Devices". Clicking on the device name and then clicking "Add" will move the device into the "Selected Devices" list. For the first six devices, this will automatically open up a VLC media player embedded in the app that will show the Mixed Reality Capture live preview from the device. Selecting a device from the "Selected Devices" list will populate the "Available Applications" list to the right of the devices list with the applications installed on that device. Clicking the "Launch" button will launch the selected app in the "Available Applications" list to all of the devices in the "Selected Devices" list, if the app is installed on the device. Similarly, clicking the "Kill" button will deactivate the selected app on all devices in the list.

#### Mixed Reality Capture
For up to six devices in the "Selected Devices" list, a VLC media player will display the MRC live preview embedded in the desktop app. The battery life of the streaming device is shown in the top right corner of the media player. When the pointer is over the media player, a command bar appears with the device name and a camera and video button.

###### Snapshot
Clicking the camera button will take an MRC Snapshot, which includes holographic images, on the device itself. The new image file is then pulled from the device and a save dialog box opens to allow the user to save the image to the local machine in addition to on the device.

###### Recording
Clicking the video button will begin an MRC Recording on the device, including holograms. The border of the media player will display as red for the duration of recording. Recording will often discontinue MRC live streaming on the device, especially on the HoloLens Emulator, but for some devices the MRC live preview will continue even during video recording. Click again on the "Stop" square that replaces the video button during recording to stop recording and open a save dialog box to save the video to the local machine in addition to on the device.

#### Output
The output box in the bottom right corner will display feedback when performing major events to keep the user informed of success or failure in execution. When the "Clear Output" checkbox is checked, the output box text will be refreshed with each new major action (connecting to a new device, launching an application, etc.). Otherwise, the output box will keep a running stream of feedback.

#### Acknowledgements
The author thanks the sample projects demonstrating the use of the Windows Device Portal Wrapper for C# UWP applications available [here](https://github.com/Microsoft/WindowsDevicePortalWrapper/tree/master/Samples) for giving inspiration and the basic code framework for connecting remotely to Windows devices. This application was developed in the course of research at Washington University in St. Louis, in the Biomedical Engineering research lab of Dr. Jon Silva, with assistance from Michael Southworth and Alex Keely.
