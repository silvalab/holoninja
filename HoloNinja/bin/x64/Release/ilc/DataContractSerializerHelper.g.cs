using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Xml;

[assembly: global::System.Reflection.AssemblyVersion("4.0.0.0")]



namespace System.Runtime.Serialization.Generated
{
    [global::System.Runtime.CompilerServices.__BlockReflection]
    public static partial class DataContractSerializerHelper
    {
        public static void InitDataContracts()
        {
            global::System.Collections.Generic.Dictionary<global::System.Type, global::System.Runtime.Serialization.DataContract> dataContracts = global::System.Runtime.Serialization.DataContract.GetDataContracts();
            PopulateContractDictionary(dataContracts);
            global::System.Collections.Generic.Dictionary<global::System.Runtime.Serialization.DataContract, global::System.Runtime.Serialization.Json.JsonReadWriteDelegates> jsonDelegates = global::System.Runtime.Serialization.Json.JsonReadWriteDelegates.GetJsonDelegates();
            PopulateJsonDelegateDictionary(
                                dataContracts, 
                                jsonDelegates
                            );
        }
        static int[] s_knownContractsLists = new int[] {
              -1, }
        ;
        // Count = 1375
        static int[] s_xmlDictionaryStrings = new int[] {
                0, // array length: 0
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                4717, // index: 4717, string: "ComputerName"
                4730, // index: 4730, string: "Language"
                4739, // index: 4739, string: "OsEdition"
                4749, // index: 4749, string: "OsEditionId"
                4761, // index: 4761, string: "OsVersion"
                4771, // index: 4771, string: "Platform"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                5, // array length: 5
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                5, // array length: 5
                4780, // index: 4780, string: "Code"
                4785, // index: 4785, string: "ErrorCode"
                4795, // index: 4795, string: "ErrorMessage"
                4808, // index: 4808, string: "Reason"
                4815, // index: 4815, string: "Success"
                5, // array length: 5
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4823, // index: 4823, string: "CrashDumps"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                4834, // index: 4834, string: "FileDate"
                4843, // index: 4843, string: "FileName"
                4852, // index: 4852, string: "FileSize"
                4861, // index: 4861, string: "PackageFullName"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4877, // index: 4877, string: "CrashDumpEnabled"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4894, // index: 4894, string: "InstalledPackages"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                4912, // index: 4912, string: "Name"
                4917, // index: 4917, string: "PackageFamilyName"
                4861, // index: 4861, string: "PackageFullName"
                4935, // index: 4935, string: "PackageOrigin"
                4949, // index: 4949, string: "PackageRelativeId"
                4967, // index: 4967, string: "Publisher"
                4977, // index: 4977, string: "Version"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                4985, // index: 4985, string: "Build"
                4991, // index: 4991, string: "Major"
                4997, // index: 4997, string: "Minor"
                5003, // index: 5003, string: "Revision"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5012, // index: 5012, string: "KnownFolders"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5025, // index: 5025, string: "Items"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                5031, // index: 5031, string: "CurrentDir"
                5042, // index: 5042, string: "DateCreated"
                4852, // index: 4852, string: "FileSize"
                5054, // index: 5054, string: "Id"
                4912, // index: 4912, string: "Name"
                5057, // index: 5057, string: "SubPath"
                5065, // index: 5065, string: "Type"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5070, // index: 5070, string: "DeviceList"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5081, // index: 5081, string: "Class"
                5087, // index: 5087, string: "Description"
                5099, // index: 5099, string: "FriendlyName"
                5112, // index: 5112, string: "ID"
                5115, // index: 5115, string: "Manufacturer"
                5128, // index: 5128, string: "ParentID"
                5137, // index: 5137, string: "ProblemCode"
                5149, // index: 5149, string: "StatusCode"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5160, // index: 5160, string: "tags"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5165, // index: 5165, string: "DumpFiles"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                4834, // index: 4834, string: "FileDate"
                4843, // index: 4843, string: "FileName"
                4852, // index: 4852, string: "FileSize"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                5175, // index: 5175, string: "autoreboot"
                5186, // index: 5186, string: "dumptype"
                5195, // index: 5195, string: "maxdumpcount"
                5208, // index: 5208, string: "overwrite"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                1094, // index: 1094, string: "Disabled"
                1103, // index: 1103, string: "CompleteMemoryDump"
                1122, // index: 1122, string: "KernelDump"
                1133, // index: 1133, string: "Minidump"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5218, // index: 5218, string: "Providers"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5228, // index: 5228, string: "GUID"
                4912, // index: 4912, string: "Name"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5233, // index: 5233, string: "Adapters"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5242, // index: 5242, string: "DHCP"
                5087, // index: 5087, string: "Description"
                5247, // index: 5247, string: "Gateways"
                5256, // index: 5256, string: "HardwareAddress"
                5272, // index: 5272, string: "Index"
                5278, // index: 5278, string: "IpAddresses"
                4912, // index: 4912, string: "Name"
                5065, // index: 5065, string: "Type"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                5290, // index: 5290, string: "Address"
                5298, // index: 5298, string: "LeaseExpires"
                5311, // index: 5311, string: "LeaseObtained"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5325, // index: 5325, string: "IpAddress"
                5335, // index: 5335, string: "Mask"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5340, // index: 5340, string: "DeviceType"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4717, // index: 4717, string: "ComputerName"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5351, // index: 5351, string: "ActivePowerScheme"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5369, // index: 5369, string: "AcOnline"
                5378, // index: 5378, string: "BatteryPresent"
                5393, // index: 5393, string: "Charging"
                5402, // index: 5402, string: "DefaultAlert1"
                5416, // index: 5416, string: "DefaultAlert2"
                5430, // index: 5430, string: "EstimatedTime"
                5444, // index: 5444, string: "MaximumCapacity"
                5460, // index: 5460, string: "RemainingCapacity"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5478, // index: 5478, string: "LowPowerState"
                5492, // index: 5492, string: "LowPowerStateAvailable"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5515, // index: 5515, string: "Processes"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                16, // array length: 16
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                16, // array length: 16
                5525, // index: 5525, string: "AppName"
                5533, // index: 5533, string: "CPUUsage"
                5542, // index: 5542, string: "ImageName"
                5552, // index: 5552, string: "IsRunning"
                5562, // index: 5562, string: "IsXAP"
                4861, // index: 4861, string: "PackageFullName"
                5568, // index: 5568, string: "PageFileUsage"
                5582, // index: 5582, string: "PrivateWorkingSet"
                5600, // index: 5600, string: "ProcessId"
                4967, // index: 4967, string: "Publisher"
                5610, // index: 5610, string: "SessionId"
                5620, // index: 5620, string: "TotalCommit"
                5632, // index: 5632, string: "UserName"
                4977, // index: 4977, string: "Version"
                5641, // index: 5641, string: "VirtualSize"
                5653, // index: 5653, string: "WorkingSetSize"
                16, // array length: 16
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                4985, // index: 4985, string: "Build"
                4991, // index: 4991, string: "Major"
                4997, // index: 4997, string: "Minor"
                5003, // index: 5003, string: "Revision"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                14, // array length: 14
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                14, // array length: 14
                5668, // index: 5668, string: "AvailablePages"
                5683, // index: 5683, string: "CommitLimit"
                5695, // index: 5695, string: "CommittedPages"
                5710, // index: 5710, string: "CpuLoad"
                5718, // index: 5718, string: "GPUData"
                5726, // index: 5726, string: "IOOtherSpeed"
                5739, // index: 5739, string: "IOReadSpeed"
                5751, // index: 5751, string: "IOWriteSpeed"
                5764, // index: 5764, string: "NetworkingData"
                5779, // index: 5779, string: "NonPagedPoolPages"
                5797, // index: 5797, string: "PageSize"
                5806, // index: 5806, string: "PagedPoolPages"
                5821, // index: 5821, string: "TotalInstalledInKb"
                5840, // index: 5840, string: "TotalPages"
                14, // array length: 14
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5851, // index: 5851, string: "AvailableAdapters"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                5869, // index: 5869, string: "DedicatedMemory"
                5885, // index: 5885, string: "DedicatedMemoryUsed"
                5087, // index: 5087, string: "Description"
                5905, // index: 5905, string: "EnginesUtilization"
                5924, // index: 5924, string: "SystemMemory"
                5937, // index: 5937, string: "SystemMemoryUsed"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5954, // index: 5954, string: "NetworkInBytes"
                5969, // index: 5969, string: "NetworkOutBytes"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5985, // index: 5985, string: "Interfaces"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                5087, // index: 5087, string: "Description"
                5228, // index: 5228, string: "GUID"
                5272, // index: 5272, string: "Index"
                5996, // index: 5996, string: "ProfilesList"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6009, // index: 6009, string: "GroupPolicyProfile"
                4912, // index: 4912, string: "Name"
                6028, // index: 6028, string: "PerUserProfile"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6043, // index: 6043, string: "AvailableNetworks"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                13, // array length: 13
                -1, // string: null
                -1, // string: null
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                13, // array length: 13
                6061, // index: 6061, string: "AlreadyConnected"
                6078, // index: 6078, string: "AuthenticationAlgorithm"
                6102, // index: 6102, string: "BSSID"
                6108, // index: 6108, string: "Channel"
                6116, // index: 6116, string: "CipherAlgorithm"
                6132, // index: 6132, string: "Connectable"
                6144, // index: 6144, string: "InfrastructureType"
                6163, // index: 6163, string: "PhysicalTypes"
                6177, // index: 6177, string: "ProfileAvailable"
                6194, // index: 6194, string: "ProfileName"
                6206, // index: 6206, string: "SSID"
                6211, // index: 6211, string: "SecurityEnabled"
                6227, // index: 6227, string: "SignalQuality"
                13, // array length: 13
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6241, // index: 6241, string: "WerReports"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6252, // index: 6252, string: "Reports"
                6260, // index: 6260, string: "User"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6265, // index: 6265, string: "CreationTime"
                4912, // index: 4912, string: "Name"
                5065, // index: 5065, string: "Type"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6278, // index: 6278, string: "Files"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                4912, // index: 4912, string: "Name"
                6284, // index: 6284, string: "Size"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6289, // index: 6289, string: "SoftwareStatus"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                6304, // index: 6304, string: "dwm.exe"
                6312, // index: 6312, string: "holoshellapp.exe"
                6329, // index: 6329, string: "holosi.exe"
                6340, // index: 6340, string: "mixedrealitycapture.exe"
                6364, // index: 6364, string: "sihost.exe"
                6375, // index: 6375, string: "spectrum.exe"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6388, // index: 6388, string: "Expected"
                6397, // index: 6397, string: "Observed"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6406, // index: 6406, string: "ipd"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6410, // index: 6410, string: "httpsRequired"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6424, // index: 6424, string: "streamId"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6433, // index: 6433, string: "mode"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                2672, // index: 2672, string: "Default"
                2680, // index: 2680, string: "Simulation"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6438, // index: 6438, string: "CurrentStage"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6451, // index: 6451, string: "MrcRecordings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6265, // index: 6265, string: "CreationTime"
                4843, // index: 4843, string: "FileName"
                4852, // index: 4852, string: "FileSize"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6465, // index: 6465, string: "MrcSettings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6477, // index: 6477, string: "Setting"
                4384, // index: 4384, string: "Value"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6485, // index: 6485, string: "IsRecording"
                6497, // index: 6497, string: "ProcessStatus"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6511, // index: 6511, string: "MrcProcess"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                6522, // index: 6522, string: "environment"
                6534, // index: 6534, string: "hands"
                6540, // index: 6540, string: "head"
                6545, // index: 6545, string: "spatialMapping"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6560, // index: 6560, string: "state"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4808, // index: 4808, string: "Reason"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6566, // index: 6566, string: "recordings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6577, // index: 6577, string: "recording"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                4785, // index: 4785, string: "ErrorCode"
                6587, // index: 6587, string: "Status"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                6594, // index: 6594, string: "TPMFamily"
                6604, // index: 6604, string: "TPMFirmware"
                6616, // index: 6616, string: "TPMManufacturer"
                6632, // index: 6632, string: "TPMRevision"
                6644, // index: 6644, string: "TPMStatus"
                6654, // index: 6654, string: "TPMType"
                6662, // index: 6662, string: "TPMVendor"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6672, // index: 6672, string: "AcpiTables"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6683, // index: 6683, string: "AzureUri"
                6692, // index: 6692, string: "DeviceId"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6701, // index: 6701, string: "AzureToken"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6712, // index: 6712, string: "AppPackages"
                6724, // index: 6724, string: "DefaultApp"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6735, // index: 6735, string: "IsStartup"
                4861, // index: 4861, string: "PackageFullName"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6712, // index: 6712, string: "AppPackages"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                6745, // index: 6745, string: "CaptureName"
                6757, // index: 6757, string: "CaptureVolume"
                6771, // index: 6771, string: "LabelErrorCode"
                6786, // index: 6786, string: "LabelStatus"
                6798, // index: 6798, string: "RenderName"
                6809, // index: 6809, string: "RenderVolume"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6822, // index: 6822, string: "output"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6829, // index: 6829, string: "PrivateInterfaces"
                6847, // index: 6847, string: "PublicInterfaces"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6864, // index: 6864, string: "SoftAPEnabled"
                6878, // index: 6878, string: "SoftApPassword"
                6893, // index: 6893, string: "SoftApSsid"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                6904, // index: 6904, string: "AllJoynOnboardingDefaultDescription"
                6940, // index: 6940, string: "AllJoynOnboardingDefaultManufacturer"
                6977, // index: 6977, string: "AllJoynOnboardingEnabled"
                7002, // index: 7002, string: "AllJoynOnboardingModelNumber"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5552, // index: 5552, string: "IsRunning"
                7031, // index: 7031, string: "IsScheduled"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7043, // index: 7043, string: "DeviceModel"
                7055, // index: 7055, string: "DeviceName"
                7066, // index: 7066, string: "OSVersion"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                7076, // index: 7076, string: "Current"
                7084, // index: 7084, string: "Timezones"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                5087, // index: 5087, string: "Description"
                5272, // index: 5272, string: "Index"
                4912, // index: 4912, string: "Name"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7076, // index: 7076, string: "Current"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                7094, // index: 7094, string: "Day"
                7098, // index: 7098, string: "Hour"
                7103, // index: 7103, string: "Minute"
                7110, // index: 7110, string: "Month"
                7116, // index: 7116, string: "Second"
                7123, // index: 7123, string: "Year"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7128, // index: 7128, string: "ControllersDrivers"
                7147, // index: 7147, string: "CurrentDriver"
                7161, // index: 7161, string: "RequestReboot"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7175, // index: 7175, string: "Orientation"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                7076, // index: 7076, string: "Current"
                7187, // index: 7187, string: "Resolutions"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5272, // index: 5272, string: "Index"
                7199, // index: 7199, string: "Resolution"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                7210, // index: 7210, string: "lastCheckTime"
                7224, // index: 7224, string: "lastFailTime"
                7237, // index: 7237, string: "lastUpdateTime"
                7252, // index: 7252, string: "stagingProgress"
                7268, // index: 7268, string: "updateState"
                7280, // index: 7280, string: "updateStatusMessage"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7300, // index: 7300, string: "Sandbox"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7308, // index: 7308, string: "Password"
                7317, // index: 7317, string: "Path"
                7322, // index: 7322, string: "Username"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7331, // index: 7331, string: "Users"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                9, // array length: 9
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                9, // array length: 9
                7337, // index: 7337, string: "AutoSignIn"
                7348, // index: 7348, string: "Delete"
                7355, // index: 7355, string: "EmailAddress"
                7368, // index: 7368, string: "Gamertag"
                7308, // index: 7308, string: "Password"
                7377, // index: 7377, string: "SignedIn"
                7386, // index: 7386, string: "SponsoredUser"
                7400, // index: 7400, string: "UserId"
                7407, // index: 7407, string: "XboxUserId"
                9, // array length: 9
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7418, // index: 7418, string: "Settings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                7427, // index: 7427, string: "Category"
                4912, // index: 4912, string: "Name"
                7436, // index: 7436, string: "RequiresReboot"
                4384, // index: 4384, string: "Value"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                7451, // index: 7451, string: "Events"
                7458, // index: 7458, string: "Frequency"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                4380, // index: 4380, string: "Key"
                4384, // index: 4384, string: "Value"
                2, // array length: 2
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                684, // index: 684, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                4417, // index: 4417, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                2, // array length: 2
                7468, // index: 7468, string: "key"
                7472, // index: 7472, string: "value"
                2, // array length: 2
                4417, // index: 4417, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                4417, // index: 4417, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7478, // index: 7478, string: "AvailableDevices"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7495, // index: 7495, string: "PairedDevices"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7509, // index: 7509, string: "PairResult"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295  // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
        };
        // Count = 6
        static global::MemberEntry[] s_dataMemberLists = new global::MemberEntry[] {
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1094, // Disabled
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1103, // CompleteMemoryDump
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1122, // KernelDump
                    Value = 2,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1133, // Minidump
                    Value = 3,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2672, // Default
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2680, // Simulation
                    Value = 1,
                }
        };
        static readonly byte[] s_dataContractMap_Hashtable = null;
        // Count=177
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractMapEntry[] s_dataContractMap = new global::DataContractMapEntry[] {
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 16, // 0x10
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 160, // 0xa0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                "11d50a3a")),
                    TableIndex = 176, // 0xb0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 224, // 0xe0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 320, // 0x140
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1, // 0x1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 17, // 0x11
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 33, // 0x21
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 2, // 0x2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 49, // 0x31
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 18, // 0x12
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 97, // 0x61
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 113, // 0x71
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 129, // 0x81
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                "d50a3a")),
                    TableIndex = 34, // 0x22
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 145, // 0x91
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 50, // 0x32
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                "ndows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 161, // 0xa1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 177, // 0xb1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 66, // 0x42
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                "9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 193, // 0xc1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 209, // 0xd1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 225, // 0xe1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 82, // 0x52
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 241, // 0xf1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 257, // 0x101
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 273, // 0x111
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 98, // 0x62
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 289, // 0x121
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 305, // 0x131
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 114, // 0x72
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 321, // 0x141
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                "5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 337, // 0x151
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 353, // 0x161
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 130, // 0x82
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 369, // 0x171
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 385, // 0x181
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 401, // 0x191
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 417, // 0x1a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 433, // 0x1b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 449, // 0x1c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 146, // 0x92
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 465, // 0x1d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 481, // 0x1e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 497, // 0x1f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 513, // 0x201
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 162, // 0xa2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 529, // 0x211
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Double, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                "d50a3a")),
                    TableIndex = 178, // 0xb2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 545, // 0x221
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 561, // 0x231
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 194, // 0xc2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 577, // 0x241
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 210, // 0xd2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 593, // 0x251
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 609, // 0x261
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 226, // 0xe2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 625, // 0x271
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                "50a3a")),
                    TableIndex = 242, // 0xf2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 641, // 0x281
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 258, // 0x102
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 657, // 0x291
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 274, // 0x112
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 673, // 0x2a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 689, // 0x2b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 290, // 0x122
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 705, // 0x2c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 721, // 0x2d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 737, // 0x2e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 753, // 0x2f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 769, // 0x301
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 785, // 0x311
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                ".UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 801, // 0x321
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                "versalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 817, // 0x331
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 833, // 0x341
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 849, // 0x351
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 306, // 0x132
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 865, // 0x361
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 881, // 0x371
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 322, // 0x142
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 897, // 0x381
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 913, // 0x391
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 929, // 0x3a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                "ersalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 945, // 0x3b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                "rapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 961, // 0x3c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 977, // 0x3d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                "UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 993, // 0x3e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                "r.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1009, // 0x3f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1025, // 0x401
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1041, // 0x411
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1057, // 0x421
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1073, // 0x431
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1089, // 0x441
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1105, // 0x451
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1121, // 0x461
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 338, // 0x152
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1137, // 0x471
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1153, // 0x481
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1169, // 0x491
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1185, // 0x4a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1201, // 0x4b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1217, // 0x4c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1233, // 0x4d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1249, // 0x4e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1265, // 0x4f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1281, // 0x501
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1297, // 0x511
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 354, // 0x162
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1313, // 0x521
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1329, // 0x531
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1345, // 0x541
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1361, // 0x551
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1377, // 0x561
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1393, // 0x571
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 370, // 0x172
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1409, // 0x581
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1425, // 0x591
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1441, // 0x5a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1457, // 0x5b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1473, // 0x5c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 386, // 0x182
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1489, // 0x5d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1505, // 0x5e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 402, // 0x192
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1521, // 0x5f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1537, // 0x601
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 418, // 0x1a2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 434, // 0x1b2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 1553, // 0x611
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 1553, // 0x611
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 1569, // 0x621
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 1569, // 0x621
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                "rsalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1585, // 0x631
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 450, // 0x1c2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1601, // 0x641
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1617, // 0x651
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1633, // 0x661
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    TableIndex = 1649, // 0x671
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 466, // 0x1d2
                }
        };
        static readonly byte[] s_dataContracts_Hashtable = null;
        // Count=21
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractEntry[] s_dataContracts = new global::DataContractEntry[] {
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 0, // boolean
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 0, // boolean
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 0, // boolean
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.BooleanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 93, // base64Binary
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 93, // base64Binary
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 93, // base64Binary
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ByteArrayDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 106, // char
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 106, // char
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 106, // char
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.CharDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 111, // dateTime
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 111, // dateTime
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 111, // dateTime
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DateTimeDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 120, // decimal
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 120, // decimal
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 120, // decimal
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DecimalDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 128, // double
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 128, // double
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 128, // double
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DoubleDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 135, // float
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 135, // float
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 135, // float
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.FloatDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 141, // guid
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 141, // guid
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 141, // guid
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.GuidDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 146, // int
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 146, // int
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 146, // int
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.IntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 150, // long
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 150, // long
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 150, // long
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.LongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 155, // anyType
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 155, // anyType
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 155, // anyType
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.ObjectDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 163, // QName
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 163, // QName
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 163, // QName
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                    "11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                    "11d50a3a")),
                    },
                    Kind = global::DataContractKind.QNameDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 169, // short
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 169, // short
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 169, // short
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 175, // byte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 175, // byte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 175, // byte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.SignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 180, // string
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 180, // string
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 180, // string
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.StringDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 187, // duration
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 187, // duration
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 187, // duration
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.TimeSpanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 196, // unsignedByte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 196, // unsignedByte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 196, // unsignedByte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 209, // unsignedInt
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 209, // unsignedInt
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 209, // unsignedInt
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedIntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 221, // unsignedLong
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 221, // unsignedLong
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 221, // unsignedLong
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedLongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 234, // unsignedShort
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 234, // unsignedShort
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 234, // unsignedShort
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 248, // anyURI
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 248, // anyURI
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 248, // anyURI
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.UriDataContract,
                }
        };
        static readonly byte[] s_classDataContracts_Hashtable = null;
        // Count=104
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::ClassDataContractEntry[] s_classDataContracts = new global::ClassDataContractEntry[] {
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 255, // DevicePortal.OperatingSystemInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 255, // DevicePortal.OperatingSystemInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 255, // DevicePortal.OperatingSystemInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1,
                    ContractNamespacesListIndex = 8,
                    MemberNamesListIndex = 10,
                    MemberNamespacesListIndex = 17,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 371, // DevicePortalException.HttpErrorResponse
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 371, // DevicePortalException.HttpErrorResponse
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 371, // DevicePortalException.HttpErrorResponse
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 24,
                    ContractNamespacesListIndex = 30,
                    MemberNamesListIndex = 32,
                    MemberNamespacesListIndex = 38,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 411, // DevicePortal.AppCrashDumpList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 411, // DevicePortal.AppCrashDumpList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 411, // DevicePortal.AppCrashDumpList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 44,
                    ContractNamespacesListIndex = 46,
                    MemberNamesListIndex = 48,
                    MemberNamespacesListIndex = 50,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 474, // DevicePortal.AppCrashDump
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 474, // DevicePortal.AppCrashDump
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 474, // DevicePortal.AppCrashDump
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 52,
                    ContractNamespacesListIndex = 57,
                    MemberNamesListIndex = 59,
                    MemberNamespacesListIndex = 64,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 500, // DevicePortal.AppCrashDumpSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 500, // DevicePortal.AppCrashDumpSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 500, // DevicePortal.AppCrashDumpSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 69,
                    ContractNamespacesListIndex = 71,
                    MemberNamesListIndex = 73,
                    MemberNamespacesListIndex = 75,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 534, // DevicePortal.AppPackages
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 534, // DevicePortal.AppPackages
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 534, // DevicePortal.AppPackages
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 77,
                    ContractNamespacesListIndex = 79,
                    MemberNamesListIndex = 81,
                    MemberNamespacesListIndex = 83,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 591, // DevicePortal.PackageInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 591, // DevicePortal.PackageInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 591, // DevicePortal.PackageInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 85,
                    ContractNamespacesListIndex = 93,
                    MemberNamesListIndex = 95,
                    MemberNamespacesListIndex = 103,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 616, // DevicePortal.PackageVersion
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 616, // DevicePortal.PackageVersion
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 616, // DevicePortal.PackageVersion
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 111,
                    ContractNamespacesListIndex = 116,
                    MemberNamesListIndex = 118,
                    MemberNamespacesListIndex = 123,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 644, // DevicePortal.KnownFolders
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 644, // DevicePortal.KnownFolders
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 644, // DevicePortal.KnownFolders
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 128,
                    ContractNamespacesListIndex = 130,
                    MemberNamesListIndex = 132,
                    MemberNamespacesListIndex = 134,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 742, // DevicePortal.FolderContents
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 742, // DevicePortal.FolderContents
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 742, // DevicePortal.FolderContents
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 136,
                    ContractNamespacesListIndex = 138,
                    MemberNamesListIndex = 140,
                    MemberNamespacesListIndex = 142,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 814, // DevicePortal.FileOrFolderInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 814, // DevicePortal.FileOrFolderInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 814, // DevicePortal.FileOrFolderInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                    "ndows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                    "ndows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 144,
                    ContractNamespacesListIndex = 152,
                    MemberNamesListIndex = 154,
                    MemberNamespacesListIndex = 162,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 851, // DevicePortal.DeviceList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 851, // DevicePortal.DeviceList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 851, // DevicePortal.DeviceList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 170,
                    ContractNamespacesListIndex = 172,
                    MemberNamesListIndex = 174,
                    MemberNamespacesListIndex = 176,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 902, // DevicePortal.Device
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 902, // DevicePortal.Device
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 902, // DevicePortal.Device
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                    "9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                    "9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 178,
                    ContractNamespacesListIndex = 187,
                    MemberNamesListIndex = 189,
                    MemberNamespacesListIndex = 198,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 922, // DevicePortal.ServiceTags
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 922, // DevicePortal.ServiceTags
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 922, // DevicePortal.ServiceTags
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 207,
                    ContractNamespacesListIndex = 209,
                    MemberNamesListIndex = 211,
                    MemberNamespacesListIndex = 213,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 947, // DevicePortal.DumpFileList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 947, // DevicePortal.DumpFileList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 947, // DevicePortal.DumpFileList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 215,
                    ContractNamespacesListIndex = 217,
                    MemberNamesListIndex = 219,
                    MemberNamespacesListIndex = 221,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1002, // DevicePortal.Dumpfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1002, // DevicePortal.Dumpfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1002, // DevicePortal.Dumpfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 223,
                    ContractNamespacesListIndex = 227,
                    MemberNamesListIndex = 229,
                    MemberNamespacesListIndex = 233,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1024, // DevicePortal.DumpFileSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1024, // DevicePortal.DumpFileSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1024, // DevicePortal.DumpFileSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 237,
                    ContractNamespacesListIndex = 242,
                    MemberNamesListIndex = 244,
                    MemberNamespacesListIndex = 249,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1142, // DevicePortal.EtwProviders
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1142, // DevicePortal.EtwProviders
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1142, // DevicePortal.EtwProviders
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 259,
                    ContractNamespacesListIndex = 261,
                    MemberNamesListIndex = 263,
                    MemberNamespacesListIndex = 265,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1204, // DevicePortal.EtwProviderInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1204, // DevicePortal.EtwProviderInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1204, // DevicePortal.EtwProviderInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 267,
                    ContractNamespacesListIndex = 270,
                    MemberNamesListIndex = 272,
                    MemberNamespacesListIndex = 275,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1233, // DevicePortal.IpConfiguration
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1233, // DevicePortal.IpConfiguration
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1233, // DevicePortal.IpConfiguration
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 278,
                    ContractNamespacesListIndex = 280,
                    MemberNamesListIndex = 282,
                    MemberNamespacesListIndex = 284,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1301, // DevicePortal.NetworkAdapterInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1301, // DevicePortal.NetworkAdapterInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1301, // DevicePortal.NetworkAdapterInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 286,
                    ContractNamespacesListIndex = 295,
                    MemberNamesListIndex = 297,
                    MemberNamespacesListIndex = 306,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1333, // DevicePortal.Dhcp
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1333, // DevicePortal.Dhcp
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1333, // DevicePortal.Dhcp
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                    "5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                    "5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 315,
                    ContractNamespacesListIndex = 319,
                    MemberNamesListIndex = 321,
                    MemberNamespacesListIndex = 325,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1351, // DevicePortal.IpAddressInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1351, // DevicePortal.IpAddressInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1351, // DevicePortal.IpAddressInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 329,
                    ContractNamespacesListIndex = 332,
                    MemberNamesListIndex = 334,
                    MemberNamespacesListIndex = 337,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1412, // DevicePortal.DeviceOsFamily
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1412, // DevicePortal.DeviceOsFamily
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1412, // DevicePortal.DeviceOsFamily
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 340,
                    ContractNamespacesListIndex = 342,
                    MemberNamesListIndex = 344,
                    MemberNamespacesListIndex = 346,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1440, // DevicePortal.DeviceName
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1440, // DevicePortal.DeviceName
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1440, // DevicePortal.DeviceName
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 348,
                    ContractNamespacesListIndex = 350,
                    MemberNamesListIndex = 352,
                    MemberNamespacesListIndex = 354,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1464, // DevicePortal.ActivePowerScheme
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1464, // DevicePortal.ActivePowerScheme
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1464, // DevicePortal.ActivePowerScheme
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 356,
                    ContractNamespacesListIndex = 358,
                    MemberNamesListIndex = 360,
                    MemberNamespacesListIndex = 362,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1495, // DevicePortal.BatteryState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1495, // DevicePortal.BatteryState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1495, // DevicePortal.BatteryState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 364,
                    ContractNamespacesListIndex = 373,
                    MemberNamesListIndex = 375,
                    MemberNamespacesListIndex = 384,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1521, // DevicePortal.PowerState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1521, // DevicePortal.PowerState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1521, // DevicePortal.PowerState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 393,
                    ContractNamespacesListIndex = 396,
                    MemberNamesListIndex = 398,
                    MemberNamespacesListIndex = 401,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1545, // DevicePortal.RunningProcesses
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1545, // DevicePortal.RunningProcesses
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1545, // DevicePortal.RunningProcesses
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 404,
                    ContractNamespacesListIndex = 406,
                    MemberNamesListIndex = 408,
                    MemberNamespacesListIndex = 410,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1613, // DevicePortal.DeviceProcessInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1613, // DevicePortal.DeviceProcessInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1613, // DevicePortal.DeviceProcessInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 412,
                    ContractNamespacesListIndex = 429,
                    MemberNamesListIndex = 431,
                    MemberNamespacesListIndex = 448,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1644, // DevicePortal.AppVersion
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1644, // DevicePortal.AppVersion
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1644, // DevicePortal.AppVersion
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 465,
                    ContractNamespacesListIndex = 470,
                    MemberNamesListIndex = 472,
                    MemberNamespacesListIndex = 477,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1668, // DevicePortal.SystemPerformanceInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1668, // DevicePortal.SystemPerformanceInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1668, // DevicePortal.SystemPerformanceInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 482,
                    ContractNamespacesListIndex = 497,
                    MemberNamesListIndex = 499,
                    MemberNamespacesListIndex = 514,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1710, // DevicePortal.GpuPerformanceData
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1710, // DevicePortal.GpuPerformanceData
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1710, // DevicePortal.GpuPerformanceData
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 529,
                    ContractNamespacesListIndex = 531,
                    MemberNamesListIndex = 533,
                    MemberNamespacesListIndex = 535,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1773, // DevicePortal.GpuAdapter
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1773, // DevicePortal.GpuAdapter
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1773, // DevicePortal.GpuAdapter
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 537,
                    ContractNamespacesListIndex = 544,
                    MemberNamesListIndex = 546,
                    MemberNamespacesListIndex = 553,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1811, // DevicePortal.NetworkPerformanceData
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1811, // DevicePortal.NetworkPerformanceData
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1811, // DevicePortal.NetworkPerformanceData
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 560,
                    ContractNamespacesListIndex = 563,
                    MemberNamesListIndex = 565,
                    MemberNamespacesListIndex = 568,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1847, // DevicePortal.WifiInterfaces
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1847, // DevicePortal.WifiInterfaces
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1847, // DevicePortal.WifiInterfaces
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 571,
                    ContractNamespacesListIndex = 573,
                    MemberNamesListIndex = 575,
                    MemberNamespacesListIndex = 577,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1909, // DevicePortal.WifiInterface
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1909, // DevicePortal.WifiInterface
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1909, // DevicePortal.WifiInterface
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 579,
                    ContractNamespacesListIndex = 584,
                    MemberNamesListIndex = 586,
                    MemberNamespacesListIndex = 591,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1975, // DevicePortal.WifiNetworkProfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1975, // DevicePortal.WifiNetworkProfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1975, // DevicePortal.WifiNetworkProfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 596,
                    ContractNamespacesListIndex = 600,
                    MemberNamesListIndex = 602,
                    MemberNamespacesListIndex = 606,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2007, // DevicePortal.WifiNetworks
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2007, // DevicePortal.WifiNetworks
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2007, // DevicePortal.WifiNetworks
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 610,
                    ContractNamespacesListIndex = 612,
                    MemberNamesListIndex = 614,
                    MemberNamespacesListIndex = 616,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2069, // DevicePortal.WifiNetworkInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2069, // DevicePortal.WifiNetworkInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2069, // DevicePortal.WifiNetworkInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 618,
                    ContractNamespacesListIndex = 632,
                    MemberNamesListIndex = 634,
                    MemberNamespacesListIndex = 648,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2109, // DevicePortal.WerDeviceReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2109, // DevicePortal.WerDeviceReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2109, // DevicePortal.WerDeviceReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 662,
                    ContractNamespacesListIndex = 664,
                    MemberNamesListIndex = 666,
                    MemberNamespacesListIndex = 668,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2174, // DevicePortal.WerUserReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2174, // DevicePortal.WerUserReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2174, // DevicePortal.WerUserReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 670,
                    ContractNamespacesListIndex = 673,
                    MemberNamesListIndex = 675,
                    MemberNamespacesListIndex = 678,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2243, // DevicePortal.WerReportInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2243, // DevicePortal.WerReportInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2243, // DevicePortal.WerReportInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 681,
                    ContractNamespacesListIndex = 685,
                    MemberNamesListIndex = 687,
                    MemberNamespacesListIndex = 691,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2277, // DevicePortal.WerFiles
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2277, // DevicePortal.WerFiles
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2277, // DevicePortal.WerFiles
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 695,
                    ContractNamespacesListIndex = 697,
                    MemberNamesListIndex = 699,
                    MemberNamespacesListIndex = 701,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2338, // DevicePortal.WerFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2338, // DevicePortal.WerFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2338, // DevicePortal.WerFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 703,
                    ContractNamespacesListIndex = 706,
                    MemberNamesListIndex = 708,
                    MemberNamespacesListIndex = 711,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2370, // DevicePortal.HolographicServices
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2370, // DevicePortal.HolographicServices
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2370, // DevicePortal.HolographicServices
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 714,
                    ContractNamespacesListIndex = 716,
                    MemberNamesListIndex = 718,
                    MemberNamespacesListIndex = 720,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2403, // DevicePortal.HolographicSoftwareStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2403, // DevicePortal.HolographicSoftwareStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2403, // DevicePortal.HolographicSoftwareStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 722,
                    ContractNamespacesListIndex = 729,
                    MemberNamesListIndex = 731,
                    MemberNamespacesListIndex = 738,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2442, // DevicePortal.ServiceStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2442, // DevicePortal.ServiceStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2442, // DevicePortal.ServiceStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 745,
                    ContractNamespacesListIndex = 748,
                    MemberNamesListIndex = 750,
                    MemberNamespacesListIndex = 753,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2469, // DevicePortal.InterPupilaryDistance
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2469, // DevicePortal.InterPupilaryDistance
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2469, // DevicePortal.InterPupilaryDistance
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 756,
                    ContractNamespacesListIndex = 758,
                    MemberNamesListIndex = 760,
                    MemberNamespacesListIndex = 762,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2504, // DevicePortal.WebManagementHttpSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2504, // DevicePortal.WebManagementHttpSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2504, // DevicePortal.WebManagementHttpSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 764,
                    ContractNamespacesListIndex = 766,
                    MemberNamesListIndex = 768,
                    MemberNamespacesListIndex = 770,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2543, // DevicePortal.PerceptionSimulationControlStreamId
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2543, // DevicePortal.PerceptionSimulationControlStreamId
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2543, // DevicePortal.PerceptionSimulationControlStreamId
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                    ".UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                    ".UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 772,
                    ContractNamespacesListIndex = 774,
                    MemberNamesListIndex = 776,
                    MemberNamespacesListIndex = 778,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2592, // DevicePortal.PerceptionSimulationControlMode
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2592, // DevicePortal.PerceptionSimulationControlMode
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2592, // DevicePortal.PerceptionSimulationControlMode
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                    "versalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                    "versalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 780,
                    ContractNamespacesListIndex = 782,
                    MemberNamesListIndex = 784,
                    MemberNamespacesListIndex = 786,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2691, // DevicePortal.ThermalStage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2691, // DevicePortal.ThermalStage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2691, // DevicePortal.ThermalStage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 791,
                    ContractNamespacesListIndex = 793,
                    MemberNamesListIndex = 795,
                    MemberNamespacesListIndex = 797,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2717, // DevicePortal.MrcFileList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2717, // DevicePortal.MrcFileList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2717, // DevicePortal.MrcFileList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 799,
                    ContractNamespacesListIndex = 801,
                    MemberNamesListIndex = 803,
                    MemberNamespacesListIndex = 805,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2781, // DevicePortal.MrcFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2781, // DevicePortal.MrcFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2781, // DevicePortal.MrcFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 807,
                    ContractNamespacesListIndex = 811,
                    MemberNamesListIndex = 813,
                    MemberNamespacesListIndex = 817,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2813, // DevicePortal.MrcSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2813, // DevicePortal.MrcSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2813, // DevicePortal.MrcSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 821,
                    ContractNamespacesListIndex = 823,
                    MemberNamesListIndex = 825,
                    MemberNamespacesListIndex = 827,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2869, // DevicePortal.MrcSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2869, // DevicePortal.MrcSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2869, // DevicePortal.MrcSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 829,
                    ContractNamespacesListIndex = 832,
                    MemberNamesListIndex = 834,
                    MemberNamespacesListIndex = 837,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2893, // DevicePortal.MrcStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2893, // DevicePortal.MrcStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2893, // DevicePortal.MrcStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 840,
                    ContractNamespacesListIndex = 843,
                    MemberNamesListIndex = 845,
                    MemberNamespacesListIndex = 848,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2916, // DevicePortal.MrcProcessStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2916, // DevicePortal.MrcProcessStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2916, // DevicePortal.MrcProcessStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 851,
                    ContractNamespacesListIndex = 853,
                    MemberNamesListIndex = 855,
                    MemberNamespacesListIndex = 857,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2946, // DevicePortal.HolographicSimulationDataTypes
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2946, // DevicePortal.HolographicSimulationDataTypes
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2946, // DevicePortal.HolographicSimulationDataTypes
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                    "ersalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                    "ersalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 859,
                    ContractNamespacesListIndex = 864,
                    MemberNamesListIndex = 866,
                    MemberNamespacesListIndex = 871,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2990, // DevicePortal.HolographicSimulationPlaybackSessionState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2990, // DevicePortal.HolographicSimulationPlaybackSessionState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2990, // DevicePortal.HolographicSimulationPlaybackSessionState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                    "rapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                    "rapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 876,
                    ContractNamespacesListIndex = 878,
                    MemberNamesListIndex = 880,
                    MemberNamespacesListIndex = 882,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3045, // DevicePortal.HolographicSimulationError
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3045, // DevicePortal.HolographicSimulationError
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3045, // DevicePortal.HolographicSimulationError
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 884,
                    ContractNamespacesListIndex = 886,
                    MemberNamesListIndex = 888,
                    MemberNamespacesListIndex = 890,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3085, // DevicePortal.HolographicSimulationPlaybackFiles
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3085, // DevicePortal.HolographicSimulationPlaybackFiles
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3085, // DevicePortal.HolographicSimulationPlaybackFiles
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                    "UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                    "UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 892,
                    ContractNamespacesListIndex = 894,
                    MemberNamesListIndex = 896,
                    MemberNamespacesListIndex = 898,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3133, // DevicePortal.HolographicSimulationRecordingStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3133, // DevicePortal.HolographicSimulationRecordingStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3133, // DevicePortal.HolographicSimulationRecordingStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                    "r.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                    "r.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 900,
                    ContractNamespacesListIndex = 902,
                    MemberNamesListIndex = 904,
                    MemberNamespacesListIndex = 906,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3183, // DevicePortal.NullResponse
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3183, // DevicePortal.NullResponse
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3183, // DevicePortal.NullResponse
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ContractNamespacesListIndex = 908,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3209, // DevicePortal.ErrorInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3209, // DevicePortal.ErrorInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3209, // DevicePortal.ErrorInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 910,
                    ContractNamespacesListIndex = 913,
                    MemberNamesListIndex = 915,
                    MemberNamespacesListIndex = 918,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3239, // DevicePortal.TpmSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3239, // DevicePortal.TpmSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3239, // DevicePortal.TpmSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 921,
                    ContractNamespacesListIndex = 929,
                    MemberNamesListIndex = 931,
                    MemberNamespacesListIndex = 939,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3268, // DevicePortal.TpmAcpiTablesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3268, // DevicePortal.TpmAcpiTablesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3268, // DevicePortal.TpmAcpiTablesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 947,
                    ContractNamespacesListIndex = 949,
                    MemberNamesListIndex = 951,
                    MemberNamespacesListIndex = 953,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3299, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3299, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3299, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 955,
                    ContractNamespacesListIndex = 958,
                    MemberNamesListIndex = 960,
                    MemberNamespacesListIndex = 963,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3341, // DevicePortal.TpmAzureTokenInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3341, // DevicePortal.TpmAzureTokenInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3341, // DevicePortal.TpmAzureTokenInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 966,
                    ContractNamespacesListIndex = 968,
                    MemberNamesListIndex = 970,
                    MemberNamespacesListIndex = 972,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3372, // DevicePortal.AppsListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3372, // DevicePortal.AppsListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3372, // DevicePortal.AppsListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 974,
                    ContractNamespacesListIndex = 977,
                    MemberNamesListIndex = 979,
                    MemberNamespacesListIndex = 982,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3429, // DevicePortal.AppPackage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3429, // DevicePortal.AppPackage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3429, // DevicePortal.AppPackage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 985,
                    ContractNamespacesListIndex = 988,
                    MemberNamesListIndex = 990,
                    MemberNamespacesListIndex = 993,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3453, // DevicePortal.HeadlessAppsListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3453, // DevicePortal.HeadlessAppsListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3453, // DevicePortal.HeadlessAppsListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 996,
                    ContractNamespacesListIndex = 998,
                    MemberNamesListIndex = 1000,
                    MemberNamespacesListIndex = 1002,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3487, // DevicePortal.AudioDeviceListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3487, // DevicePortal.AudioDeviceListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3487, // DevicePortal.AudioDeviceListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1004,
                    ContractNamespacesListIndex = 1011,
                    MemberNamesListIndex = 1013,
                    MemberNamespacesListIndex = 1020,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3520, // DevicePortal.RunCommandOutputInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3520, // DevicePortal.RunCommandOutputInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3520, // DevicePortal.RunCommandOutputInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1027,
                    ContractNamespacesListIndex = 1029,
                    MemberNamesListIndex = 1031,
                    MemberNamespacesListIndex = 1033,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3554, // DevicePortal.IscInterfacesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3554, // DevicePortal.IscInterfacesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3554, // DevicePortal.IscInterfacesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1035,
                    ContractNamespacesListIndex = 1038,
                    MemberNamesListIndex = 1040,
                    MemberNamespacesListIndex = 1043,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3585, // DevicePortal.SoftAPSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3585, // DevicePortal.SoftAPSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3585, // DevicePortal.SoftAPSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1046,
                    ContractNamespacesListIndex = 1050,
                    MemberNamesListIndex = 1052,
                    MemberNamespacesListIndex = 1056,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3617, // DevicePortal.AllJoynSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3617, // DevicePortal.AllJoynSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3617, // DevicePortal.AllJoynSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1060,
                    ContractNamespacesListIndex = 1065,
                    MemberNamesListIndex = 1067,
                    MemberNamespacesListIndex = 1072,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3650, // DevicePortal.RemoteSettingsStatusInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3650, // DevicePortal.RemoteSettingsStatusInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3650, // DevicePortal.RemoteSettingsStatusInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1077,
                    ContractNamespacesListIndex = 1080,
                    MemberNamesListIndex = 1082,
                    MemberNamespacesListIndex = 1085,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3688, // DevicePortal.IoTOSInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3688, // DevicePortal.IoTOSInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3688, // DevicePortal.IoTOSInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1088,
                    ContractNamespacesListIndex = 1092,
                    MemberNamesListIndex = 1094,
                    MemberNamespacesListIndex = 1098,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3711, // DevicePortal.TimezoneInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3711, // DevicePortal.TimezoneInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3711, // DevicePortal.TimezoneInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1102,
                    ContractNamespacesListIndex = 1105,
                    MemberNamesListIndex = 1107,
                    MemberNamespacesListIndex = 1110,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3737, // DevicePortal.Timezone
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3737, // DevicePortal.Timezone
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3737, // DevicePortal.Timezone
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1113,
                    ContractNamespacesListIndex = 1117,
                    MemberNamesListIndex = 1119,
                    MemberNamespacesListIndex = 1123,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3788, // DevicePortal.DateTimeInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3788, // DevicePortal.DateTimeInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3788, // DevicePortal.DateTimeInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1127,
                    ContractNamespacesListIndex = 1129,
                    MemberNamesListIndex = 1131,
                    MemberNamespacesListIndex = 1133,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3814, // DevicePortal.DateTimeDescription
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3814, // DevicePortal.DateTimeDescription
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3814, // DevicePortal.DateTimeDescription
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1135,
                    ContractNamespacesListIndex = 1142,
                    MemberNamesListIndex = 1144,
                    MemberNamespacesListIndex = 1151,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3847, // DevicePortal.ControllerDriverInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3847, // DevicePortal.ControllerDriverInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3847, // DevicePortal.ControllerDriverInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1158,
                    ContractNamespacesListIndex = 1162,
                    MemberNamesListIndex = 1164,
                    MemberNamespacesListIndex = 1168,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3881, // DevicePortal.DisplayOrientationInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3881, // DevicePortal.DisplayOrientationInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3881, // DevicePortal.DisplayOrientationInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1172,
                    ContractNamespacesListIndex = 1174,
                    MemberNamesListIndex = 1176,
                    MemberNamespacesListIndex = 1178,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3917, // DevicePortal.DisplayResolutionInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3917, // DevicePortal.DisplayResolutionInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3917, // DevicePortal.DisplayResolutionInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1180,
                    ContractNamespacesListIndex = 1183,
                    MemberNamesListIndex = 1185,
                    MemberNamespacesListIndex = 1188,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3952, // DevicePortal.Resolution
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3952, // DevicePortal.Resolution
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3952, // DevicePortal.Resolution
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1191,
                    ContractNamespacesListIndex = 1194,
                    MemberNamesListIndex = 1196,
                    MemberNamespacesListIndex = 1199,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4007, // DevicePortal.StatusInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4007, // DevicePortal.StatusInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4007, // DevicePortal.StatusInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1202,
                    ContractNamespacesListIndex = 1209,
                    MemberNamesListIndex = 1211,
                    MemberNamespacesListIndex = 1218,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4031, // DevicePortal.UpdateInstallTimeInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4031, // DevicePortal.UpdateInstallTimeInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4031, // DevicePortal.UpdateInstallTimeInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    ContractNamespacesListIndex = 1225,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4066, // DevicePortal.Sandbox
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4066, // DevicePortal.Sandbox
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4066, // DevicePortal.Sandbox
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1227,
                    ContractNamespacesListIndex = 1229,
                    MemberNamesListIndex = 1231,
                    MemberNamespacesListIndex = 1233,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4087, // DevicePortal.SmbInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4087, // DevicePortal.SmbInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4087, // DevicePortal.SmbInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1235,
                    ContractNamespacesListIndex = 1239,
                    MemberNamesListIndex = 1241,
                    MemberNamespacesListIndex = 1245,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4108, // DevicePortal.UserList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4108, // DevicePortal.UserList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4108, // DevicePortal.UserList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1249,
                    ContractNamespacesListIndex = 1251,
                    MemberNamesListIndex = 1253,
                    MemberNamespacesListIndex = 1255,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4159, // DevicePortal.UserInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4159, // DevicePortal.UserInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4159, // DevicePortal.UserInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1257,
                    ContractNamespacesListIndex = 1267,
                    MemberNamesListIndex = 1269,
                    MemberNamespacesListIndex = 1279,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4181, // DevicePortal.XboxSettingList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4181, // DevicePortal.XboxSettingList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4181, // DevicePortal.XboxSettingList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1289,
                    ContractNamespacesListIndex = 1291,
                    MemberNamesListIndex = 1293,
                    MemberNamespacesListIndex = 1295,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4242, // DevicePortal.XboxSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4242, // DevicePortal.XboxSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4242, // DevicePortal.XboxSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1297,
                    ContractNamespacesListIndex = 1302,
                    MemberNamesListIndex = 1304,
                    MemberNamespacesListIndex = 1309,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4267, // DevicePortal.EtwEvents
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4267, // DevicePortal.EtwEvents
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4267, // DevicePortal.EtwEvents
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1314,
                    ContractNamespacesListIndex = 1317,
                    MemberNamesListIndex = 1319,
                    MemberNamespacesListIndex = 1322,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 4357, // KeyValueOfstringstring
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 4357, // KeyValueOfstringstring
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 4357, // KeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Runtime.Serialization.KeyValue`2, System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neut" +
                                    "ral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1325,
                    ContractNamespacesListIndex = 1328,
                    MemberNamesListIndex = 1330,
                    MemberNamespacesListIndex = 1333,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 4390, // KeyValuePairOfstringstring
                        NamespaceIndex = 4417, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        StableNameIndex = 4390, // KeyValuePairOfstringstring
                        StableNameNamespaceIndex = 4417, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        TopLevelElementNameIndex = 4390, // KeyValuePairOfstringstring
                        TopLevelElementNamespaceIndex = 4417, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.KeyValuePair`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=b03f5f7f11d50a3a")),
                    },
                    ChildElementNamespacesListIndex = 1336,
                    ContractNamespacesListIndex = 1339,
                    MemberNamesListIndex = 1341,
                    MemberNamespacesListIndex = 1344,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4484, // DevicePortal.AvailableBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4484, // DevicePortal.AvailableBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4484, // DevicePortal.AvailableBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                    "rsalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                    "rsalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1347,
                    ContractNamespacesListIndex = 1349,
                    MemberNamesListIndex = 1351,
                    MemberNamespacesListIndex = 1353,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4567, // DevicePortal.BluetoothDeviceInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4567, // DevicePortal.BluetoothDeviceInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4567, // DevicePortal.BluetoothDeviceInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    ContractNamespacesListIndex = 1355,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4600, // DevicePortal.PairedBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4600, // DevicePortal.PairedBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4600, // DevicePortal.PairedBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1357,
                    ContractNamespacesListIndex = 1359,
                    MemberNamesListIndex = 1361,
                    MemberNamespacesListIndex = 1363,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4640, // DevicePortal.PairBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4640, // DevicePortal.PairBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4640, // DevicePortal.PairBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1365,
                    ContractNamespacesListIndex = 1367,
                    MemberNamesListIndex = 1369,
                    MemberNamespacesListIndex = 1371,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4678, // DevicePortal.PairResult
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4678, // DevicePortal.PairResult
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4678, // DevicePortal.PairResult
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    ContractNamespacesListIndex = 1373,
                }
        };
        static readonly byte[] s_collectionDataContracts_Hashtable = null;
        // Count=30
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::CollectionDataContractEntry[] s_collectionDataContracts = new global::CollectionDataContractEntry[] {
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 441, // ArrayOfDevicePortal.AppCrashDump
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 441, // ArrayOfDevicePortal.AppCrashDump
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 441, // ArrayOfDevicePortal.AppCrashDump
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 474, // DevicePortal.AppCrashDump
                    KeyNameIndex = -1,
                    ItemNameIndex = 474, // DevicePortal.AppCrashDump
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 559, // ArrayOfDevicePortal.PackageInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 559, // ArrayOfDevicePortal.PackageInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 559, // ArrayOfDevicePortal.PackageInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 591, // DevicePortal.PackageInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 591, // DevicePortal.PackageInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 670, // ArrayOfstring
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 670, // ArrayOfstring
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 670, // ArrayOfstring
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 180, // string
                    KeyNameIndex = -1,
                    ItemNameIndex = 180, // string
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 770, // ArrayOfDevicePortal.FileOrFolderInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 770, // ArrayOfDevicePortal.FileOrFolderInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 770, // ArrayOfDevicePortal.FileOrFolderInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 814, // DevicePortal.FileOrFolderInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 814, // DevicePortal.FileOrFolderInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                "ndows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 875, // ArrayOfDevicePortal.Device
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 875, // ArrayOfDevicePortal.Device
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 875, // ArrayOfDevicePortal.Device
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 902, // DevicePortal.Device
                    KeyNameIndex = -1,
                    ItemNameIndex = 902, // DevicePortal.Device
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                "9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 973, // ArrayOfDevicePortal.Dumpfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 973, // ArrayOfDevicePortal.Dumpfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 973, // ArrayOfDevicePortal.Dumpfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1002, // DevicePortal.Dumpfile
                    KeyNameIndex = -1,
                    ItemNameIndex = 1002, // DevicePortal.Dumpfile
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1168, // ArrayOfDevicePortal.EtwProviderInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1168, // ArrayOfDevicePortal.EtwProviderInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1168, // ArrayOfDevicePortal.EtwProviderInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1204, // DevicePortal.EtwProviderInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1204, // DevicePortal.EtwProviderInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1262, // ArrayOfDevicePortal.NetworkAdapterInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1262, // ArrayOfDevicePortal.NetworkAdapterInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1262, // ArrayOfDevicePortal.NetworkAdapterInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1301, // DevicePortal.NetworkAdapterInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1301, // DevicePortal.NetworkAdapterInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1378, // ArrayOfDevicePortal.IpAddressInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1378, // ArrayOfDevicePortal.IpAddressInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1378, // ArrayOfDevicePortal.IpAddressInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1351, // DevicePortal.IpAddressInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1351, // DevicePortal.IpAddressInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1575, // ArrayOfDevicePortal.DeviceProcessInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1575, // ArrayOfDevicePortal.DeviceProcessInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1575, // ArrayOfDevicePortal.DeviceProcessInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1613, // DevicePortal.DeviceProcessInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1613, // DevicePortal.DeviceProcessInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1742, // ArrayOfDevicePortal.GpuAdapter
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1742, // ArrayOfDevicePortal.GpuAdapter
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1742, // ArrayOfDevicePortal.GpuAdapter
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1773, // DevicePortal.GpuAdapter
                    KeyNameIndex = -1,
                    ItemNameIndex = 1773, // DevicePortal.GpuAdapter
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1797, // ArrayOfdouble
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 1797, // ArrayOfdouble
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 1797, // ArrayOfdouble
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Double, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Double, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 128, // double
                    KeyNameIndex = -1,
                    ItemNameIndex = 128, // double
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1875, // ArrayOfDevicePortal.WifiInterface
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1875, // ArrayOfDevicePortal.WifiInterface
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1875, // ArrayOfDevicePortal.WifiInterface
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1909, // DevicePortal.WifiInterface
                    KeyNameIndex = -1,
                    ItemNameIndex = 1909, // DevicePortal.WifiInterface
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1936, // ArrayOfDevicePortal.WifiNetworkProfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1936, // ArrayOfDevicePortal.WifiNetworkProfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1936, // ArrayOfDevicePortal.WifiNetworkProfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1975, // DevicePortal.WifiNetworkProfile
                    KeyNameIndex = -1,
                    ItemNameIndex = 1975, // DevicePortal.WifiNetworkProfile
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2033, // ArrayOfDevicePortal.WifiNetworkInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2033, // ArrayOfDevicePortal.WifiNetworkInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2033, // ArrayOfDevicePortal.WifiNetworkInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2069, // DevicePortal.WifiNetworkInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 2069, // DevicePortal.WifiNetworkInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2098, // ArrayOfint
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 2098, // ArrayOfint
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 2098, // ArrayOfint
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 146, // int
                    KeyNameIndex = -1,
                    ItemNameIndex = 146, // int
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2139, // ArrayOfDevicePortal.WerUserReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2139, // ArrayOfDevicePortal.WerUserReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2139, // ArrayOfDevicePortal.WerUserReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2174, // DevicePortal.WerUserReports
                    KeyNameIndex = -1,
                    ItemNameIndex = 2174, // DevicePortal.WerUserReports
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2202, // ArrayOfDevicePortal.WerReportInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2202, // ArrayOfDevicePortal.WerReportInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2202, // ArrayOfDevicePortal.WerReportInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2243, // DevicePortal.WerReportInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 2243, // DevicePortal.WerReportInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2299, // ArrayOfDevicePortal.WerFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2299, // ArrayOfDevicePortal.WerFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2299, // ArrayOfDevicePortal.WerFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2338, // DevicePortal.WerFileInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 2338, // DevicePortal.WerFileInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2742, // ArrayOfDevicePortal.MrcFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2742, // ArrayOfDevicePortal.MrcFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2742, // ArrayOfDevicePortal.MrcFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2781, // DevicePortal.MrcFileInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 2781, // DevicePortal.MrcFileInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2838, // ArrayOfDevicePortal.MrcSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2838, // ArrayOfDevicePortal.MrcSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2838, // ArrayOfDevicePortal.MrcSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2869, // DevicePortal.MrcSetting
                    KeyNameIndex = -1,
                    ItemNameIndex = 2869, // DevicePortal.MrcSetting
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3398, // ArrayOfDevicePortal.AppPackage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3398, // ArrayOfDevicePortal.AppPackage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3398, // ArrayOfDevicePortal.AppPackage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3429, // DevicePortal.AppPackage
                    KeyNameIndex = -1,
                    ItemNameIndex = 3429, // DevicePortal.AppPackage
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3759, // ArrayOfDevicePortal.Timezone
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3759, // ArrayOfDevicePortal.Timezone
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3759, // ArrayOfDevicePortal.Timezone
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3737, // DevicePortal.Timezone
                    KeyNameIndex = -1,
                    ItemNameIndex = 3737, // DevicePortal.Timezone
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3976, // ArrayOfDevicePortal.Resolution
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3976, // ArrayOfDevicePortal.Resolution
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3976, // ArrayOfDevicePortal.Resolution
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3952, // DevicePortal.Resolution
                    KeyNameIndex = -1,
                    ItemNameIndex = 3952, // DevicePortal.Resolution
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4130, // ArrayOfDevicePortal.UserInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4130, // ArrayOfDevicePortal.UserInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4130, // ArrayOfDevicePortal.UserInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4159, // DevicePortal.UserInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 4159, // DevicePortal.UserInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4210, // ArrayOfDevicePortal.XboxSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4210, // ArrayOfDevicePortal.XboxSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4210, // ArrayOfDevicePortal.XboxSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4242, // DevicePortal.XboxSetting
                    KeyNameIndex = -1,
                    ItemNameIndex = 4242, // DevicePortal.XboxSetting
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4290, // ArrayOfArrayOfKeyValueOfstringstring
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 4290, // ArrayOfArrayOfKeyValueOfstringstring
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 4290, // ArrayOfArrayOfKeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4327, // ArrayOfKeyValueOfstringstring
                    KeyNameIndex = -1,
                    ItemNameIndex = 4327, // ArrayOfKeyValueOfstringstring
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4327, // ArrayOfKeyValueOfstringstring
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 4327, // ArrayOfKeyValueOfstringstring
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 4327, // ArrayOfKeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.Dictionary`2, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b" +
                                    "03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4357, // KeyValueOfstringstring
                    KeyNameIndex = 4380, // Key
                    ItemNameIndex = 4357, // KeyValueOfstringstring
                    ValueNameIndex = 4384, // Value
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericDictionary,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4527, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4527, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4527, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4567, // DevicePortal.BluetoothDeviceInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 4567, // DevicePortal.BluetoothDeviceInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4702, // ArrayOfanyType
                        NamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 4702, // ArrayOfanyType
                        StableNameNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 4702, // ArrayOfanyType
                        TopLevelElementNamespaceIndex = 684, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 155, // anyType
                    KeyNameIndex = -1,
                    ItemNameIndex = 155, // anyType
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }
        };
        static readonly byte[] s_enumDataContracts_Hashtable = null;
        // Count=2
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::EnumDataContractEntry[] s_enumDataContracts = new global::EnumDataContractEntry[] {
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 1054, // DevicePortal.DumpFileSettings.DumpTypes
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1054, // DevicePortal.DumpFileSettings.DumpTypes
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1054, // DevicePortal.DumpFileSettings.DumpTypes
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 254,
                    MemberCount = 4,
                }, 
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2637, // DevicePortal.SimulationControlMode
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2637, // DevicePortal.SimulationControlMode
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2637, // DevicePortal.SimulationControlMode
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.5.0, Culture=neutral, PublicKeyToken=9e8585fab36bda5f")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 788,
                    MemberCount = 2,
                    MemberListIndex = 4,
                }
        };
        static readonly byte[] s_xmlDataContracts_Hashtable = null;
        // Count=0
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::XmlDataContractEntry[] s_xmlDataContracts = new global::XmlDataContractEntry[0];
        static readonly byte[] s_jsonDelegatesList_Hashtable = null;
        // Count=136
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::JsonDelegateEntry[] s_jsonDelegatesList = new global::JsonDelegateEntry[] {
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 37,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type3.WriteDevicePortal_OperatingSystemInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type2.ReadDevicePortal_OperatingSystemInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 38,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type7.WriteDevicePortalException_HttpErrorResponseToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type6.ReadDevicePortalException_HttpErrorResponseFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 39,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type11.WriteDevicePortal_AppCrashDumpListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type10.ReadDevicePortal_AppCrashDumpListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 40,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type16.WriteArrayOfDevicePortal_AppCrashDumpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type15.ReadArrayOfDevicePortal_AppCrashDumpFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type17.ReadArrayOfDevicePortal_AppCrashDumpFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 41,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type21.WriteDevicePortal_AppCrashDumpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type20.ReadDevicePortal_AppCrashDumpFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 42,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type25.WriteDevicePortal_AppCrashDumpSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type24.ReadDevicePortal_AppCrashDumpSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 43,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type29.WriteDevicePortal_AppPackagesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type28.ReadDevicePortal_AppPackagesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 44,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type34.WriteArrayOfDevicePortal_PackageInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type33.ReadArrayOfDevicePortal_PackageInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type35.ReadArrayOfDevicePortal_PackageInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 45,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type39.WriteDevicePortal_PackageInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type38.ReadDevicePortal_PackageInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 46,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type43.WriteDevicePortal_PackageVersionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type42.ReadDevicePortal_PackageVersionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 47,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type47.WriteDevicePortal_KnownFoldersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type46.ReadDevicePortal_KnownFoldersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 48,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type52.WriteArrayOfstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type51.ReadArrayOfstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type53.ReadArrayOfstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 49,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type57.WriteDevicePortal_FolderContentsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type56.ReadDevicePortal_FolderContentsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 50,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type62.WriteArrayOfDevicePortal_FileOrFolderInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type61.ReadArrayOfDevicePortal_FileOrFolderInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type63.ReadArrayOfDevicePortal_FileOrFolderInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 51,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type67.WriteDevicePortal_FileOrFolderInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type66.ReadDevicePortal_FileOrFolderInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 52,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type71.WriteDevicePortal_DeviceListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type70.ReadDevicePortal_DeviceListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 53,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type76.WriteArrayOfDevicePortal_DeviceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type75.ReadArrayOfDevicePortal_DeviceFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type77.ReadArrayOfDevicePortal_DeviceFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 54,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type81.WriteDevicePortal_DeviceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type80.ReadDevicePortal_DeviceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 55,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type85.WriteDevicePortal_ServiceTagsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type84.ReadDevicePortal_ServiceTagsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 56,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type89.WriteDevicePortal_DumpFileListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type88.ReadDevicePortal_DumpFileListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 57,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type94.WriteArrayOfDevicePortal_DumpfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type93.ReadArrayOfDevicePortal_DumpfileFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type95.ReadArrayOfDevicePortal_DumpfileFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 58,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type99.WriteDevicePortal_DumpfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type98.ReadDevicePortal_DumpfileFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 59,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type103.WriteDevicePortal_DumpFileSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type102.ReadDevicePortal_DumpFileSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 60,
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 62,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type107.WriteDevicePortal_EtwProvidersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type106.ReadDevicePortal_EtwProvidersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 63,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type112.WriteArrayOfDevicePortal_EtwProviderInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type111.ReadArrayOfDevicePortal_EtwProviderInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type113.ReadArrayOfDevicePortal_EtwProviderInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 64,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type117.WriteDevicePortal_EtwProviderInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type116.ReadDevicePortal_EtwProviderInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 65,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type121.WriteDevicePortal_IpConfigurationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type120.ReadDevicePortal_IpConfigurationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 66,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type126.WriteArrayOfDevicePortal_NetworkAdapterInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type125.ReadArrayOfDevicePortal_NetworkAdapterInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type127.ReadArrayOfDevicePortal_NetworkAdapterInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 67,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type131.WriteDevicePortal_NetworkAdapterInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type130.ReadDevicePortal_NetworkAdapterInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 68,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type135.WriteDevicePortal_DhcpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type134.ReadDevicePortal_DhcpFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 69,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type139.WriteDevicePortal_IpAddressInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type138.ReadDevicePortal_IpAddressInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 70,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type144.WriteArrayOfDevicePortal_IpAddressInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type143.ReadArrayOfDevicePortal_IpAddressInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type145.ReadArrayOfDevicePortal_IpAddressInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 71,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type149.WriteDevicePortal_DeviceOsFamilyToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type148.ReadDevicePortal_DeviceOsFamilyFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 72,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type153.WriteDevicePortal_DeviceNameToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type152.ReadDevicePortal_DeviceNameFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 73,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type157.WriteDevicePortal_ActivePowerSchemeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type156.ReadDevicePortal_ActivePowerSchemeFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 74,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type161.WriteDevicePortal_BatteryStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type160.ReadDevicePortal_BatteryStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 75,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type165.WriteDevicePortal_PowerStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type164.ReadDevicePortal_PowerStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 76,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type169.WriteDevicePortal_RunningProcessesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type168.ReadDevicePortal_RunningProcessesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 77,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type174.WriteArrayOfDevicePortal_DeviceProcessInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type173.ReadArrayOfDevicePortal_DeviceProcessInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type175.ReadArrayOfDevicePortal_DeviceProcessInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 78,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type179.WriteDevicePortal_DeviceProcessInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type178.ReadDevicePortal_DeviceProcessInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 79,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type183.WriteDevicePortal_AppVersionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type182.ReadDevicePortal_AppVersionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 80,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type187.WriteDevicePortal_SystemPerformanceInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type186.ReadDevicePortal_SystemPerformanceInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 81,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type191.WriteDevicePortal_GpuPerformanceDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type190.ReadDevicePortal_GpuPerformanceDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 82,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type196.WriteArrayOfDevicePortal_GpuAdapterToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type195.ReadArrayOfDevicePortal_GpuAdapterFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type197.ReadArrayOfDevicePortal_GpuAdapterFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 83,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type201.WriteDevicePortal_GpuAdapterToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type200.ReadDevicePortal_GpuAdapterFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 84,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type206.WriteArrayOfdoubleToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type205.ReadArrayOfdoubleFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type207.ReadArrayOfdoubleFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 85,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type211.WriteDevicePortal_NetworkPerformanceDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type210.ReadDevicePortal_NetworkPerformanceDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 86,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type215.WriteDevicePortal_WifiInterfacesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type214.ReadDevicePortal_WifiInterfacesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 87,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type220.WriteArrayOfDevicePortal_WifiInterfaceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type219.ReadArrayOfDevicePortal_WifiInterfaceFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type221.ReadArrayOfDevicePortal_WifiInterfaceFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 88,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type225.WriteDevicePortal_WifiInterfaceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type224.ReadDevicePortal_WifiInterfaceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 89,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type230.WriteArrayOfDevicePortal_WifiNetworkProfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type229.ReadArrayOfDevicePortal_WifiNetworkProfileFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type231.ReadArrayOfDevicePortal_WifiNetworkProfileFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 90,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type235.WriteDevicePortal_WifiNetworkProfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type234.ReadDevicePortal_WifiNetworkProfileFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 91,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type239.WriteDevicePortal_WifiNetworksToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type238.ReadDevicePortal_WifiNetworksFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 92,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type244.WriteArrayOfDevicePortal_WifiNetworkInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type243.ReadArrayOfDevicePortal_WifiNetworkInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type245.ReadArrayOfDevicePortal_WifiNetworkInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 93,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type249.WriteDevicePortal_WifiNetworkInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type248.ReadDevicePortal_WifiNetworkInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 94,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type254.WriteArrayOfintToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type253.ReadArrayOfintFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type255.ReadArrayOfintFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 95,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type259.WriteDevicePortal_WerDeviceReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type258.ReadDevicePortal_WerDeviceReportsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 96,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type264.WriteArrayOfDevicePortal_WerUserReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type263.ReadArrayOfDevicePortal_WerUserReportsFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type265.ReadArrayOfDevicePortal_WerUserReportsFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 97,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type269.WriteDevicePortal_WerUserReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type268.ReadDevicePortal_WerUserReportsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 98,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type274.WriteArrayOfDevicePortal_WerReportInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type273.ReadArrayOfDevicePortal_WerReportInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type275.ReadArrayOfDevicePortal_WerReportInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 99,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type279.WriteDevicePortal_WerReportInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type278.ReadDevicePortal_WerReportInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 100,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type283.WriteDevicePortal_WerFilesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type282.ReadDevicePortal_WerFilesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 101,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type288.WriteArrayOfDevicePortal_WerFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type287.ReadArrayOfDevicePortal_WerFileInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type289.ReadArrayOfDevicePortal_WerFileInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 102,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type293.WriteDevicePortal_WerFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type292.ReadDevicePortal_WerFileInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 103,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type297.WriteDevicePortal_HolographicServicesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type296.ReadDevicePortal_HolographicServicesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 104,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type301.WriteDevicePortal_HolographicSoftwareStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type300.ReadDevicePortal_HolographicSoftwareStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 105,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type305.WriteDevicePortal_ServiceStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type304.ReadDevicePortal_ServiceStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 106,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type309.WriteDevicePortal_InterPupilaryDistanceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type308.ReadDevicePortal_InterPupilaryDistanceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 107,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type313.WriteDevicePortal_WebManagementHttpSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type312.ReadDevicePortal_WebManagementHttpSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 108,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type317.WriteDevicePortal_PerceptionSimulationControlStreamIdToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type316.ReadDevicePortal_PerceptionSimulationControlStreamIdFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 109,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type321.WriteDevicePortal_PerceptionSimulationControlModeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type320.ReadDevicePortal_PerceptionSimulationControlModeFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 110,
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 112,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type325.WriteDevicePortal_ThermalStageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type324.ReadDevicePortal_ThermalStageFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 113,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type329.WriteDevicePortal_MrcFileListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type328.ReadDevicePortal_MrcFileListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 114,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type334.WriteArrayOfDevicePortal_MrcFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type333.ReadArrayOfDevicePortal_MrcFileInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type335.ReadArrayOfDevicePortal_MrcFileInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 115,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type339.WriteDevicePortal_MrcFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type338.ReadDevicePortal_MrcFileInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 116,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type343.WriteDevicePortal_MrcSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type342.ReadDevicePortal_MrcSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 117,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type348.WriteArrayOfDevicePortal_MrcSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type347.ReadArrayOfDevicePortal_MrcSettingFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type349.ReadArrayOfDevicePortal_MrcSettingFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 118,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type353.WriteDevicePortal_MrcSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type352.ReadDevicePortal_MrcSettingFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 119,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type357.WriteDevicePortal_MrcStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type356.ReadDevicePortal_MrcStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 120,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type361.WriteDevicePortal_MrcProcessStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type360.ReadDevicePortal_MrcProcessStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 121,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type365.WriteDevicePortal_HolographicSimulationDataTypesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type364.ReadDevicePortal_HolographicSimulationDataTypesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 122,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type369.WriteDevicePortal_HolographicSimulationPlaybackSessionStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type368.ReadDevicePortal_HolographicSimulationPlaybackSessionStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 123,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type373.WriteDevicePortal_HolographicSimulationErrorToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type372.ReadDevicePortal_HolographicSimulationErrorFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 124,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type377.WriteDevicePortal_HolographicSimulationPlaybackFilesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type376.ReadDevicePortal_HolographicSimulationPlaybackFilesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 125,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type381.WriteDevicePortal_HolographicSimulationRecordingStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type380.ReadDevicePortal_HolographicSimulationRecordingStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 126,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type385.WriteDevicePortal_NullResponseToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type384.ReadDevicePortal_NullResponseFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 127,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type389.WriteDevicePortal_ErrorInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type388.ReadDevicePortal_ErrorInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 128,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type393.WriteDevicePortal_TpmSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type392.ReadDevicePortal_TpmSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 129,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type397.WriteDevicePortal_TpmAcpiTablesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type396.ReadDevicePortal_TpmAcpiTablesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 130,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type401.WriteDevicePortal_TpmLogicalDeviceSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type400.ReadDevicePortal_TpmLogicalDeviceSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 131,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type405.WriteDevicePortal_TpmAzureTokenInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type404.ReadDevicePortal_TpmAzureTokenInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 132,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type409.WriteDevicePortal_AppsListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type408.ReadDevicePortal_AppsListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 133,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type414.WriteArrayOfDevicePortal_AppPackageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type413.ReadArrayOfDevicePortal_AppPackageFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type415.ReadArrayOfDevicePortal_AppPackageFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 134,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type419.WriteDevicePortal_AppPackageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type418.ReadDevicePortal_AppPackageFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 135,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type423.WriteDevicePortal_HeadlessAppsListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type422.ReadDevicePortal_HeadlessAppsListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 136,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type427.WriteDevicePortal_AudioDeviceListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type426.ReadDevicePortal_AudioDeviceListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 137,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type431.WriteDevicePortal_RunCommandOutputInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type430.ReadDevicePortal_RunCommandOutputInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 138,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type435.WriteDevicePortal_IscInterfacesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type434.ReadDevicePortal_IscInterfacesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 139,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type439.WriteDevicePortal_SoftAPSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type438.ReadDevicePortal_SoftAPSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 140,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type443.WriteDevicePortal_AllJoynSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type442.ReadDevicePortal_AllJoynSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 141,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type447.WriteDevicePortal_RemoteSettingsStatusInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type446.ReadDevicePortal_RemoteSettingsStatusInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 142,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type451.WriteDevicePortal_IoTOSInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type450.ReadDevicePortal_IoTOSInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 143,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type455.WriteDevicePortal_TimezoneInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type454.ReadDevicePortal_TimezoneInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 144,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type459.WriteDevicePortal_TimezoneToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type458.ReadDevicePortal_TimezoneFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 145,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type464.WriteArrayOfDevicePortal_TimezoneToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type463.ReadArrayOfDevicePortal_TimezoneFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type465.ReadArrayOfDevicePortal_TimezoneFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 146,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type469.WriteDevicePortal_DateTimeInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type468.ReadDevicePortal_DateTimeInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 147,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type473.WriteDevicePortal_DateTimeDescriptionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type472.ReadDevicePortal_DateTimeDescriptionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 148,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type477.WriteDevicePortal_ControllerDriverInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type476.ReadDevicePortal_ControllerDriverInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 149,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type481.WriteDevicePortal_DisplayOrientationInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type480.ReadDevicePortal_DisplayOrientationInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 150,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type485.WriteDevicePortal_DisplayResolutionInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type484.ReadDevicePortal_DisplayResolutionInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 151,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type489.WriteDevicePortal_ResolutionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type488.ReadDevicePortal_ResolutionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 152,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type494.WriteArrayOfDevicePortal_ResolutionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type493.ReadArrayOfDevicePortal_ResolutionFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type495.ReadArrayOfDevicePortal_ResolutionFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 153,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type499.WriteDevicePortal_StatusInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type498.ReadDevicePortal_StatusInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 154,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type503.WriteDevicePortal_UpdateInstallTimeInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type502.ReadDevicePortal_UpdateInstallTimeInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 155,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type507.WriteDevicePortal_SandboxToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type506.ReadDevicePortal_SandboxFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 156,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type511.WriteDevicePortal_SmbInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type510.ReadDevicePortal_SmbInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 157,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type515.WriteDevicePortal_UserListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type514.ReadDevicePortal_UserListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 158,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type520.WriteArrayOfDevicePortal_UserInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type519.ReadArrayOfDevicePortal_UserInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type521.ReadArrayOfDevicePortal_UserInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 159,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type525.WriteDevicePortal_UserInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type524.ReadDevicePortal_UserInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 160,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type529.WriteDevicePortal_XboxSettingListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type528.ReadDevicePortal_XboxSettingListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 161,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type534.WriteArrayOfDevicePortal_XboxSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type533.ReadArrayOfDevicePortal_XboxSettingFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type535.ReadArrayOfDevicePortal_XboxSettingFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 162,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type539.WriteDevicePortal_XboxSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type538.ReadDevicePortal_XboxSettingFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 163,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type543.WriteDevicePortal_EtwEventsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type542.ReadDevicePortal_EtwEventsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 164,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type548.WriteArrayOfArrayOfKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type547.ReadArrayOfArrayOfKeyValueOfstringstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type549.ReadArrayOfArrayOfKeyValueOfstringstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 165,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type554.WriteArrayOfKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type553.ReadArrayOfKeyValueOfstringstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type555.ReadArrayOfKeyValueOfstringstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 166,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type559.WriteKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type558.ReadKeyValueOfstringstringFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 168,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type563.WriteKeyValuePairOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type562.ReadKeyValuePairOfstringstringFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 170,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type567.WriteDevicePortal_AvailableBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type566.ReadDevicePortal_AvailableBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 171,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type572.WriteArrayOfDevicePortal_BluetoothDeviceInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type571.ReadArrayOfDevicePortal_BluetoothDeviceInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type573.ReadArrayOfDevicePortal_BluetoothDeviceInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 172,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type577.WriteDevicePortal_BluetoothDeviceInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type576.ReadDevicePortal_BluetoothDeviceInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 173,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type581.WriteDevicePortal_PairedBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type580.ReadDevicePortal_PairedBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 174,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type585.WriteDevicePortal_PairBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type584.ReadDevicePortal_PairBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 175,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type589.WriteDevicePortal_PairResultToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type588.ReadDevicePortal_PairResultFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 176,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type594.WriteArrayOfanyTypeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type593.ReadArrayOfanyTypeFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type595.ReadArrayOfanyTypeFromJsonIsGetOnly),
                }
        };
        static char[] s_stringPool = new char[] {
            'b','o','o','l','e','a','n','\0','h','t','t','p',':','/','/','w','w','w','.','w','3','.','o','r','g','/','2','0','0','1',
            '/','X','M','L','S','c','h','e','m','a','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','m','i','c','r',
            'o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i','z','a','t','i','o',
            'n','/','\0','b','a','s','e','6','4','B','i','n','a','r','y','\0','c','h','a','r','\0','d','a','t','e','T','i','m','e','\0',
            'd','e','c','i','m','a','l','\0','d','o','u','b','l','e','\0','f','l','o','a','t','\0','g','u','i','d','\0','i','n','t','\0',
            'l','o','n','g','\0','a','n','y','T','y','p','e','\0','Q','N','a','m','e','\0','s','h','o','r','t','\0','b','y','t','e','\0',
            's','t','r','i','n','g','\0','d','u','r','a','t','i','o','n','\0','u','n','s','i','g','n','e','d','B','y','t','e','\0','u',
            'n','s','i','g','n','e','d','I','n','t','\0','u','n','s','i','g','n','e','d','L','o','n','g','\0','u','n','s','i','g','n',
            'e','d','S','h','o','r','t','\0','a','n','y','U','R','I','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','O','p',
            'e','r','a','t','i','n','g','S','y','s','t','e','m','I','n','f','o','r','m','a','t','i','o','n','\0','h','t','t','p',':',
            '/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0',
            '4','/','0','7','/','M','i','c','r','o','s','o','f','t','.','T','o','o','l','s','.','W','i','n','d','o','w','s','D','e',
            'v','i','c','e','P','o','r','t','a','l','\0','D','e','v','i','c','e','P','o','r','t','a','l','E','x','c','e','p','t','i',
            'o','n','.','H','t','t','p','E','r','r','o','r','R','e','s','p','o','n','s','e','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','A','p','p','C','r','a','s','h','D','u','m','p','L','i','s','t','\0','A','r','r','a','y','O','f','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','A','p','p','C','r','a','s','h','D','u','m','p','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','A','p','p','C','r','a','s','h','D','u','m','p','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','A','p','p','C','r','a','s','h','D','u','m','p','S','e','t','t','i','n','g','s','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','A','p','p','P','a','c','k','a','g','e','s','\0','A','r','r','a','y','O','f','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','P','a','c','k','a','g','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','P','a','c','k','a','g','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P',
            'a','c','k','a','g','e','V','e','r','s','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','K','n','o',
            'w','n','F','o','l','d','e','r','s','\0','A','r','r','a','y','O','f','s','t','r','i','n','g','\0','h','t','t','p',':','/',
            '/','s','c','h','e','m','a','s','.','m','i','c','r','o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0',
            '/','S','e','r','i','a','l','i','z','a','t','i','o','n','/','A','r','r','a','y','s','\0','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','F','o','l','d','e','r','C','o','n','t','e','n','t','s','\0','A','r','r','a','y','O','f','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','F','i','l','e','O','r','F','o','l','d','e','r','I','n','f','o','r','m','a','t',
            'i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','F','i','l','e','O','r','F','o','l','d','e','r','I',
            'n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e',
            'L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c',
            'e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','\0','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','S','e','r','v','i','c','e','T','a','g','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'D','u','m','p','F','i','l','e','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','D','u','m','p','f','i','l','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','u','m','p','f',
            'i','l','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','u','m','p','F','i','l','e','S','e','t','t','i',
            'n','g','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','u','m','p','F','i','l','e','S','e','t','t','i',
            'n','g','s','.','D','u','m','p','T','y','p','e','s','\0','D','i','s','a','b','l','e','d','\0','C','o','m','p','l','e','t',
            'e','M','e','m','o','r','y','D','u','m','p','\0','K','e','r','n','e','l','D','u','m','p','\0','M','i','n','i','d','u','m',
            'p','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','E','t','w','P','r','o','v','i','d','e','r','s','\0','A','r',
            'r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','E','t','w','P','r','o','v','i','d','e','r','I',
            'n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','E','t','w','P','r','o','v','i','d','e','r','I','n',
            'f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','I','p','C','o','n','f','i','g','u','r','a','t','i','o',
            'n','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','N','e','t','w','o','r','k','A',
            'd','a','p','t','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','N','e','t','w','o','r',
            'k','A','d','a','p','t','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','h','c','p',
            '\0','D','e','v','i','c','e','P','o','r','t','a','l','.','I','p','A','d','d','r','e','s','s','I','n','f','o','\0','A','r',
            'r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','I','p','A','d','d','r','e','s','s','I','n','f',
            'o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','O','s','F','a','m','i','l','y','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','N','a','m','e','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','A','c','t','i','v','e','P','o','w','e','r','S','c','h','e','m','e','\0','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','B','a','t','t','e','r','y','S','t','a','t','e','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','P','o','w','e','r','S','t','a','t','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','R','u',
            'n','n','i','n','g','P','r','o','c','e','s','s','e','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','D','e','v','i','c','e','P','r','o','c','e','s','s','I','n','f','o','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','D','e','v','i','c','e','P','r','o','c','e','s','s','I','n','f','o','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','A','p','p','V','e','r','s','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','S','y','s','t','e','m','P','e','r','f','o','r','m','a','n','c','e','I','n','f','o','r','m','a','t','i','o','n','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','G','p','u','P','e','r','f','o','r','m','a','n','c','e','D','a','t',
            'a','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','G','p','u','A','d','a','p','t',
            'e','r','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','G','p','u','A','d','a','p','t','e','r','\0','A','r','r',
            'a','y','O','f','d','o','u','b','l','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','N','e','t','w','o','r',
            'k','P','e','r','f','o','r','m','a','n','c','e','D','a','t','a','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'W','i','f','i','I','n','t','e','r','f','a','c','e','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','W','i','f','i','I','n','t','e','r','f','a','c','e','\0','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','W','i','f','i','I','n','t','e','r','f','a','c','e','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r','k','P','r','o','f','i','l','e','\0','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r','k','P','r','o','f','i','l','e','\0','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r','k','s','\0','A','r','r','a','y','O','f',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r','k','I','n','f','o','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r','k','I','n','f','o','\0','A','r',
            'r','a','y','O','f','i','n','t','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','D','e','v','i','c',
            'e','R','e','p','o','r','t','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W',
            'e','r','U','s','e','r','R','e','p','o','r','t','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r',
            'U','s','e','r','R','e','p','o','r','t','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','W','e','r','R','e','p','o','r','t','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','W','e','r','R','e','p','o','r','t','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','W','e','r','F','i','l','e','s','\0','A','r','r','a','y','O','f','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','W','e','r','F','i','l','e','I','n','f','o','r','m','a','t','i','o','n','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','W','e','r','F','i','l','e','I','n','f','o','r','m','a','t','i','o','n','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','e','r','v','i','c',
            'e','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','o','f',
            't','w','a','r','e','S','t','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S','e','r','v','i',
            'c','e','S','t','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','I','n','t','e','r','P','u','p',
            'i','l','a','r','y','D','i','s','t','a','n','c','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','b',
            'M','a','n','a','g','e','m','e','n','t','H','t','t','p','S','e','t','t','i','n','g','s','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','P','e','r','c','e','p','t','i','o','n','S','i','m','u','l','a','t','i','o','n','C','o','n','t',
            'r','o','l','S','t','r','e','a','m','I','d','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','e','r','c','e',
            'p','t','i','o','n','S','i','m','u','l','a','t','i','o','n','C','o','n','t','r','o','l','M','o','d','e','\0','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','S','i','m','u','l','a','t','i','o','n','C','o','n','t','r','o','l','M','o','d',
            'e','\0','D','e','f','a','u','l','t','\0','S','i','m','u','l','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','T','h','e','r','m','a','l','S','t','a','g','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'M','r','c','F','i','l','e','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','M','r','c','F','i','l','e','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','M','r','c','F','i','l','e','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','M','r','c','S','e','t','t','i','n','g','s','\0','A','r','r','a','y','O','f','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','M','r','c','S','e','t','t','i','n','g','\0','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','M','r','c','S','e','t','t','i','n','g','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','S',
            't','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','P','r','o','c','e','s','s','S',
            't','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c',
            'S','i','m','u','l','a','t','i','o','n','D','a','t','a','T','y','p','e','s','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','P','l','a','y','b','a',
            'c','k','S','e','s','s','i','o','n','S','t','a','t','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o',
            'l','o','g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','E','r','r','o','r','\0','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','P',
            'l','a','y','b','a','c','k','F','i','l','e','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o',
            'g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','R','e','c','o','r','d','i','n','g','S','t','a','t',
            'u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','N','u','l','l','R','e','s','p','o','n','s','e','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','E','r','r','o','r','I','n','f','o','r','m','a','t','i','o','n','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','S','e','t','t','i','n','g','s','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','T','p','m','A','c','p','i','T','a','b','l','e','s','I','n','f','o','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','L','o','g','i','c','a','l','D','e','v','i','c','e','S','e',
            't','t','i','n','g','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','A','z','u',
            'r','e','T','o','k','e','n','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','s','L',
            'i','s','t','I','n','f','o','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p',
            'p','P','a','c','k','a','g','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','P','a','c','k','a',
            'g','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','e','a','d','l','e','s','s','A','p','p','s','L','i',
            's','t','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','u','d','i','o','D','e','v','i','c',
            'e','L','i','s','t','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','R','u','n','C','o','m','m',
            'a','n','d','O','u','t','p','u','t','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','I','s','c',
            'I','n','t','e','r','f','a','c','e','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S','o',
            'f','t','A','P','S','e','t','t','i','n','g','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'A','l','l','J','o','y','n','S','e','t','t','i','n','g','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','R','e','m','o','t','e','S','e','t','t','i','n','g','s','S','t','a','t','u','s','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','I','o','T','O','S','I','n','f','o','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','T','i','m','e','z','o','n','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'T','i','m','e','z','o','n','e','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','T',
            'i','m','e','z','o','n','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','a','t','e','T','i','m','e','I',
            'n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','a','t','e','T','i','m','e','D','e','s','c','r',
            'i','p','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','C','o','n','t','r','o','l','l','e','r',
            'D','r','i','v','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','i','s','p','l','a',
            'y','O','r','i','e','n','t','a','t','i','o','n','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'D','i','s','p','l','a','y','R','e','s','o','l','u','t','i','o','n','I','n','f','o','\0','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','R','e','s','o','l','u','t','i','o','n','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','R','e','s','o','l','u','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'S','t','a','t','u','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','U','p','d','a','t','e',
            'I','n','s','t','a','l','l','T','i','m','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S',
            'a','n','d','b','o','x','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S','m','b','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','U','s','e','r','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','U','s','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','U','s','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','X','b','o','x','S','e',
            't','t','i','n','g','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'X','b','o','x','S','e','t','t','i','n','g','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','X','b','o','x','S',
            'e','t','t','i','n','g','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','E','t','w','E','v','e','n','t','s','\0',
            'A','r','r','a','y','O','f','A','r','r','a','y','O','f','K','e','y','V','a','l','u','e','O','f','s','t','r','i','n','g',
            's','t','r','i','n','g','\0','A','r','r','a','y','O','f','K','e','y','V','a','l','u','e','O','f','s','t','r','i','n','g',
            's','t','r','i','n','g','\0','K','e','y','V','a','l','u','e','O','f','s','t','r','i','n','g','s','t','r','i','n','g','\0',
            'K','e','y','\0','V','a','l','u','e','\0','K','e','y','V','a','l','u','e','P','a','i','r','O','f','s','t','r','i','n','g',
            's','t','r','i','n','g','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t',
            'r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','S','y','s','t','e','m','.','C','o','l','l','e','c',
            't','i','o','n','s','.','G','e','n','e','r','i','c','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','v','a',
            'i','l','a','b','l','e','B','l','u','e','t','o','o','t','h','D','e','v','i','c','e','s','I','n','f','o','\0','A','r','r',
            'a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','B','l','u','e','t','o','o','t','h','D','e','v','i',
            'c','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','B','l','u','e','t','o','o','t','h','D',
            'e','v','i','c','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','a','i','r','e','d','B',
            'l','u','e','t','o','o','t','h','D','e','v','i','c','e','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','P','a','i','r','B','l','u','e','t','o','o','t','h','D','e','v','i','c','e','s','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','P','a','i','r','R','e','s','u','l','t','\0','A','r','r','a','y','O','f','a',
            'n','y','T','y','p','e','\0','C','o','m','p','u','t','e','r','N','a','m','e','\0','L','a','n','g','u','a','g','e','\0','O',
            's','E','d','i','t','i','o','n','\0','O','s','E','d','i','t','i','o','n','I','d','\0','O','s','V','e','r','s','i','o','n',
            '\0','P','l','a','t','f','o','r','m','\0','C','o','d','e','\0','E','r','r','o','r','C','o','d','e','\0','E','r','r','o','r',
            'M','e','s','s','a','g','e','\0','R','e','a','s','o','n','\0','S','u','c','c','e','s','s','\0','C','r','a','s','h','D','u',
            'm','p','s','\0','F','i','l','e','D','a','t','e','\0','F','i','l','e','N','a','m','e','\0','F','i','l','e','S','i','z','e',
            '\0','P','a','c','k','a','g','e','F','u','l','l','N','a','m','e','\0','C','r','a','s','h','D','u','m','p','E','n','a','b',
            'l','e','d','\0','I','n','s','t','a','l','l','e','d','P','a','c','k','a','g','e','s','\0','N','a','m','e','\0','P','a','c',
            'k','a','g','e','F','a','m','i','l','y','N','a','m','e','\0','P','a','c','k','a','g','e','O','r','i','g','i','n','\0','P',
            'a','c','k','a','g','e','R','e','l','a','t','i','v','e','I','d','\0','P','u','b','l','i','s','h','e','r','\0','V','e','r',
            's','i','o','n','\0','B','u','i','l','d','\0','M','a','j','o','r','\0','M','i','n','o','r','\0','R','e','v','i','s','i','o',
            'n','\0','K','n','o','w','n','F','o','l','d','e','r','s','\0','I','t','e','m','s','\0','C','u','r','r','e','n','t','D','i',
            'r','\0','D','a','t','e','C','r','e','a','t','e','d','\0','I','d','\0','S','u','b','P','a','t','h','\0','T','y','p','e','\0',
            'D','e','v','i','c','e','L','i','s','t','\0','C','l','a','s','s','\0','D','e','s','c','r','i','p','t','i','o','n','\0','F',
            'r','i','e','n','d','l','y','N','a','m','e','\0','I','D','\0','M','a','n','u','f','a','c','t','u','r','e','r','\0','P','a',
            'r','e','n','t','I','D','\0','P','r','o','b','l','e','m','C','o','d','e','\0','S','t','a','t','u','s','C','o','d','e','\0',
            't','a','g','s','\0','D','u','m','p','F','i','l','e','s','\0','a','u','t','o','r','e','b','o','o','t','\0','d','u','m','p',
            't','y','p','e','\0','m','a','x','d','u','m','p','c','o','u','n','t','\0','o','v','e','r','w','r','i','t','e','\0','P','r',
            'o','v','i','d','e','r','s','\0','G','U','I','D','\0','A','d','a','p','t','e','r','s','\0','D','H','C','P','\0','G','a','t',
            'e','w','a','y','s','\0','H','a','r','d','w','a','r','e','A','d','d','r','e','s','s','\0','I','n','d','e','x','\0','I','p',
            'A','d','d','r','e','s','s','e','s','\0','A','d','d','r','e','s','s','\0','L','e','a','s','e','E','x','p','i','r','e','s',
            '\0','L','e','a','s','e','O','b','t','a','i','n','e','d','\0','I','p','A','d','d','r','e','s','s','\0','M','a','s','k','\0',
            'D','e','v','i','c','e','T','y','p','e','\0','A','c','t','i','v','e','P','o','w','e','r','S','c','h','e','m','e','\0','A',
            'c','O','n','l','i','n','e','\0','B','a','t','t','e','r','y','P','r','e','s','e','n','t','\0','C','h','a','r','g','i','n',
            'g','\0','D','e','f','a','u','l','t','A','l','e','r','t','1','\0','D','e','f','a','u','l','t','A','l','e','r','t','2','\0',
            'E','s','t','i','m','a','t','e','d','T','i','m','e','\0','M','a','x','i','m','u','m','C','a','p','a','c','i','t','y','\0',
            'R','e','m','a','i','n','i','n','g','C','a','p','a','c','i','t','y','\0','L','o','w','P','o','w','e','r','S','t','a','t',
            'e','\0','L','o','w','P','o','w','e','r','S','t','a','t','e','A','v','a','i','l','a','b','l','e','\0','P','r','o','c','e',
            's','s','e','s','\0','A','p','p','N','a','m','e','\0','C','P','U','U','s','a','g','e','\0','I','m','a','g','e','N','a','m',
            'e','\0','I','s','R','u','n','n','i','n','g','\0','I','s','X','A','P','\0','P','a','g','e','F','i','l','e','U','s','a','g',
            'e','\0','P','r','i','v','a','t','e','W','o','r','k','i','n','g','S','e','t','\0','P','r','o','c','e','s','s','I','d','\0',
            'S','e','s','s','i','o','n','I','d','\0','T','o','t','a','l','C','o','m','m','i','t','\0','U','s','e','r','N','a','m','e',
            '\0','V','i','r','t','u','a','l','S','i','z','e','\0','W','o','r','k','i','n','g','S','e','t','S','i','z','e','\0','A','v',
            'a','i','l','a','b','l','e','P','a','g','e','s','\0','C','o','m','m','i','t','L','i','m','i','t','\0','C','o','m','m','i',
            't','t','e','d','P','a','g','e','s','\0','C','p','u','L','o','a','d','\0','G','P','U','D','a','t','a','\0','I','O','O','t',
            'h','e','r','S','p','e','e','d','\0','I','O','R','e','a','d','S','p','e','e','d','\0','I','O','W','r','i','t','e','S','p',
            'e','e','d','\0','N','e','t','w','o','r','k','i','n','g','D','a','t','a','\0','N','o','n','P','a','g','e','d','P','o','o',
            'l','P','a','g','e','s','\0','P','a','g','e','S','i','z','e','\0','P','a','g','e','d','P','o','o','l','P','a','g','e','s',
            '\0','T','o','t','a','l','I','n','s','t','a','l','l','e','d','I','n','K','b','\0','T','o','t','a','l','P','a','g','e','s',
            '\0','A','v','a','i','l','a','b','l','e','A','d','a','p','t','e','r','s','\0','D','e','d','i','c','a','t','e','d','M','e',
            'm','o','r','y','\0','D','e','d','i','c','a','t','e','d','M','e','m','o','r','y','U','s','e','d','\0','E','n','g','i','n',
            'e','s','U','t','i','l','i','z','a','t','i','o','n','\0','S','y','s','t','e','m','M','e','m','o','r','y','\0','S','y','s',
            't','e','m','M','e','m','o','r','y','U','s','e','d','\0','N','e','t','w','o','r','k','I','n','B','y','t','e','s','\0','N',
            'e','t','w','o','r','k','O','u','t','B','y','t','e','s','\0','I','n','t','e','r','f','a','c','e','s','\0','P','r','o','f',
            'i','l','e','s','L','i','s','t','\0','G','r','o','u','p','P','o','l','i','c','y','P','r','o','f','i','l','e','\0','P','e',
            'r','U','s','e','r','P','r','o','f','i','l','e','\0','A','v','a','i','l','a','b','l','e','N','e','t','w','o','r','k','s',
            '\0','A','l','r','e','a','d','y','C','o','n','n','e','c','t','e','d','\0','A','u','t','h','e','n','t','i','c','a','t','i',
            'o','n','A','l','g','o','r','i','t','h','m','\0','B','S','S','I','D','\0','C','h','a','n','n','e','l','\0','C','i','p','h',
            'e','r','A','l','g','o','r','i','t','h','m','\0','C','o','n','n','e','c','t','a','b','l','e','\0','I','n','f','r','a','s',
            't','r','u','c','t','u','r','e','T','y','p','e','\0','P','h','y','s','i','c','a','l','T','y','p','e','s','\0','P','r','o',
            'f','i','l','e','A','v','a','i','l','a','b','l','e','\0','P','r','o','f','i','l','e','N','a','m','e','\0','S','S','I','D',
            '\0','S','e','c','u','r','i','t','y','E','n','a','b','l','e','d','\0','S','i','g','n','a','l','Q','u','a','l','i','t','y',
            '\0','W','e','r','R','e','p','o','r','t','s','\0','R','e','p','o','r','t','s','\0','U','s','e','r','\0','C','r','e','a','t',
            'i','o','n','T','i','m','e','\0','F','i','l','e','s','\0','S','i','z','e','\0','S','o','f','t','w','a','r','e','S','t','a',
            't','u','s','\0','d','w','m','.','e','x','e','\0','h','o','l','o','s','h','e','l','l','a','p','p','.','e','x','e','\0','h',
            'o','l','o','s','i','.','e','x','e','\0','m','i','x','e','d','r','e','a','l','i','t','y','c','a','p','t','u','r','e','.',
            'e','x','e','\0','s','i','h','o','s','t','.','e','x','e','\0','s','p','e','c','t','r','u','m','.','e','x','e','\0','E','x',
            'p','e','c','t','e','d','\0','O','b','s','e','r','v','e','d','\0','i','p','d','\0','h','t','t','p','s','R','e','q','u','i',
            'r','e','d','\0','s','t','r','e','a','m','I','d','\0','m','o','d','e','\0','C','u','r','r','e','n','t','S','t','a','g','e',
            '\0','M','r','c','R','e','c','o','r','d','i','n','g','s','\0','M','r','c','S','e','t','t','i','n','g','s','\0','S','e','t',
            't','i','n','g','\0','I','s','R','e','c','o','r','d','i','n','g','\0','P','r','o','c','e','s','s','S','t','a','t','u','s',
            '\0','M','r','c','P','r','o','c','e','s','s','\0','e','n','v','i','r','o','n','m','e','n','t','\0','h','a','n','d','s','\0',
            'h','e','a','d','\0','s','p','a','t','i','a','l','M','a','p','p','i','n','g','\0','s','t','a','t','e','\0','r','e','c','o',
            'r','d','i','n','g','s','\0','r','e','c','o','r','d','i','n','g','\0','S','t','a','t','u','s','\0','T','P','M','F','a','m',
            'i','l','y','\0','T','P','M','F','i','r','m','w','a','r','e','\0','T','P','M','M','a','n','u','f','a','c','t','u','r','e',
            'r','\0','T','P','M','R','e','v','i','s','i','o','n','\0','T','P','M','S','t','a','t','u','s','\0','T','P','M','T','y','p',
            'e','\0','T','P','M','V','e','n','d','o','r','\0','A','c','p','i','T','a','b','l','e','s','\0','A','z','u','r','e','U','r',
            'i','\0','D','e','v','i','c','e','I','d','\0','A','z','u','r','e','T','o','k','e','n','\0','A','p','p','P','a','c','k','a',
            'g','e','s','\0','D','e','f','a','u','l','t','A','p','p','\0','I','s','S','t','a','r','t','u','p','\0','C','a','p','t','u',
            'r','e','N','a','m','e','\0','C','a','p','t','u','r','e','V','o','l','u','m','e','\0','L','a','b','e','l','E','r','r','o',
            'r','C','o','d','e','\0','L','a','b','e','l','S','t','a','t','u','s','\0','R','e','n','d','e','r','N','a','m','e','\0','R',
            'e','n','d','e','r','V','o','l','u','m','e','\0','o','u','t','p','u','t','\0','P','r','i','v','a','t','e','I','n','t','e',
            'r','f','a','c','e','s','\0','P','u','b','l','i','c','I','n','t','e','r','f','a','c','e','s','\0','S','o','f','t','A','P',
            'E','n','a','b','l','e','d','\0','S','o','f','t','A','p','P','a','s','s','w','o','r','d','\0','S','o','f','t','A','p','S',
            's','i','d','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i','n','g','D','e','f','a','u','l','t','D','e',
            's','c','r','i','p','t','i','o','n','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i','n','g','D','e','f',
            'a','u','l','t','M','a','n','u','f','a','c','t','u','r','e','r','\0','A','l','l','J','o','y','n','O','n','b','o','a','r',
            'd','i','n','g','E','n','a','b','l','e','d','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i','n','g','M',
            'o','d','e','l','N','u','m','b','e','r','\0','I','s','S','c','h','e','d','u','l','e','d','\0','D','e','v','i','c','e','M',
            'o','d','e','l','\0','D','e','v','i','c','e','N','a','m','e','\0','O','S','V','e','r','s','i','o','n','\0','C','u','r','r',
            'e','n','t','\0','T','i','m','e','z','o','n','e','s','\0','D','a','y','\0','H','o','u','r','\0','M','i','n','u','t','e','\0',
            'M','o','n','t','h','\0','S','e','c','o','n','d','\0','Y','e','a','r','\0','C','o','n','t','r','o','l','l','e','r','s','D',
            'r','i','v','e','r','s','\0','C','u','r','r','e','n','t','D','r','i','v','e','r','\0','R','e','q','u','e','s','t','R','e',
            'b','o','o','t','\0','O','r','i','e','n','t','a','t','i','o','n','\0','R','e','s','o','l','u','t','i','o','n','s','\0','R',
            'e','s','o','l','u','t','i','o','n','\0','l','a','s','t','C','h','e','c','k','T','i','m','e','\0','l','a','s','t','F','a',
            'i','l','T','i','m','e','\0','l','a','s','t','U','p','d','a','t','e','T','i','m','e','\0','s','t','a','g','i','n','g','P',
            'r','o','g','r','e','s','s','\0','u','p','d','a','t','e','S','t','a','t','e','\0','u','p','d','a','t','e','S','t','a','t',
            'u','s','M','e','s','s','a','g','e','\0','S','a','n','d','b','o','x','\0','P','a','s','s','w','o','r','d','\0','P','a','t',
            'h','\0','U','s','e','r','n','a','m','e','\0','U','s','e','r','s','\0','A','u','t','o','S','i','g','n','I','n','\0','D','e',
            'l','e','t','e','\0','E','m','a','i','l','A','d','d','r','e','s','s','\0','G','a','m','e','r','t','a','g','\0','S','i','g',
            'n','e','d','I','n','\0','S','p','o','n','s','o','r','e','d','U','s','e','r','\0','U','s','e','r','I','d','\0','X','b','o',
            'x','U','s','e','r','I','d','\0','S','e','t','t','i','n','g','s','\0','C','a','t','e','g','o','r','y','\0','R','e','q','u',
            'i','r','e','s','R','e','b','o','o','t','\0','E','v','e','n','t','s','\0','F','r','e','q','u','e','n','c','y','\0','k','e',
            'y','\0','v','a','l','u','e','\0','A','v','a','i','l','a','b','l','e','D','e','v','i','c','e','s','\0','P','a','i','r','e',
            'd','D','e','v','i','c','e','s','\0','P','a','i','r','R','e','s','u','l','t','\0'};
    }
}
