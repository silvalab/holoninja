﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Input;
using Microsoft.Tools.WindowsDevicePortal;
using static Microsoft.Tools.WindowsDevicePortal.DevicePortal;
using Windows.Security.Cryptography.Certificates;
using System.Threading.Tasks;
using System.Text;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using libVLCX;
using VLC;
using Windows.UI;
using VLC.Converters;
using Windows.Web.Http;
using System.Linq;
using System.Collections.Concurrent;

// The device connection framework was adapted from a sample WDP project found at https://github.com/Microsoft/WindowsDevicePortalWrapper/tree/master/Samples/SampleWdpClient.UniversalWindows

namespace HoloNinja
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // We'll need a device portal to connect to the HLs
        private DevicePortal portal;
        // and a certificate
        private Certificate certificate;

        // list of connected device portals
        private List<DevicePortal> availPortals = new List<DevicePortal>();
        // list of connected device names to display
        private List<string> availDeviceNames = new List<string>();

        // list of chosen device portals (to launch apps to)
        private List<DevicePortal> currPortals = new List<DevicePortal>();
        // list of chosen device names to display
        private List<string> currDeviceNames = new List<string>();

        // list of available apps (string app IDs) on a selected device
        private List<PackageInfo> availAppPackages = new List<PackageInfo>();
        private List<string> availAppNames = new List<string>();

        // dictionary of all portals that have been connected to the app
        private ConcurrentDictionary<string, Dictionary<string, object>> allPortals = new ConcurrentDictionary<string, Dictionary<string, object>>();

        // dictionary of display groups 
        private Dictionary<string, Dictionary<string, object>> displayGroups = new Dictionary<string, Dictionary<string, object>>();

        // portals are stored here temporarily when the vlcPlayer starts up so that the log in dialog can access the portal
        private DevicePortal streamingPortal = null;

        // lists for relevent display elements
        private List<VLC.MediaElement> vlcPlayers = new List<VLC.MediaElement>();
        private List<Border> vlcBorders = new List<Border>();
        private List<TextBox> batteryTexts = new List<TextBox>();

        // timer for updating battery levels
        private DispatcherTimer dispatcherTimer;

        // flag for if a player is updating
        bool is_player_updating = false;
        
        public MainPage()
        {
            this.InitializeComponent();

            // start with no apps to launch or devices to select
            this.EnableAppLaunch(false);
            this.EnableDeviceSelection(false);

            // start with one player            
            this.PopulatePlayerList();

            this.EnableExtraPlayers(false);
            this.HideCommandBars();

            // initial population of list view objects
            this.availDeviceList.ItemsSource = this.availDeviceNames;
            this.currDeviceList.ItemsSource = this.currDeviceNames;
            this.availAppList.ItemsSource = this.availAppNames;

            this.DispatchedTimerSetup();
            this.DisplayGroupsSetup();

            this.saveDevices.IsEnabled = false;

        }

        // updates batterylife every 5 seconds, resets frozen players,
        // and hides player 1 if it's not playing so it doesn't leave frozen images
        private void DispatcherTimer_Tick(object sender, object e)
        {
            UpdatePlayerDisplays();

            foreach (VLC.MediaElement player in vlcPlayers)
            {
                if (player.CurrentState == MediaElementState.Stopped && player.Source != null)
                {
                    outputBox.Text += player.Name + " has stopped playing. Resetting...\n";
                    string source = player.Source;
                    player.Source = null;
                    player.Source = source;
                }

                if (player == this.vlcPlayer1 && player.Source == null && !this.is_player_updating)
                {
                    player.Opacity = 0.0;
                }
            }
        }
        // clears the output textbox
        private void ClearOutput()
        {
            try
            {
#pragma warning disable IDE0030 // Use coalesce expression
                bool clearOutput = this.clearOutput.IsChecked.HasValue ? this.clearOutput.IsChecked.Value : false;
#pragma warning restore IDE0030 // Use coalesce expression
                if (clearOutput)
                {
                    this.outputBox.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }


        #region Setup
        // populates display group dictionaries
        public void DisplayGroupsSetup()
        {
            this.displayGroups.Add("Group1", new Dictionary<string, object>());
            this.displayGroups["Group1"].Add("player", this.vlcPlayer1);
            this.displayGroups["Group1"].Add("border", this.vlcBorder1);
            this.displayGroups["Group1"].Add("deviceLabel", this.playerLabel1);
            this.displayGroups["Group1"].Add("batteryText", this.battery1);
            this.displayGroups["Group1"].Add("name", "Group1");
            this.displayGroups["Group1"].Add("is_active", false);

            this.displayGroups.Add("Group2", new Dictionary<string, object>());
            this.displayGroups["Group2"].Add("player", this.vlcPlayer2);
            this.displayGroups["Group2"].Add("border", this.vlcBorder2);
            this.displayGroups["Group2"].Add("deviceLabel", this.playerLabel2);
            this.displayGroups["Group2"].Add("batteryText", this.battery2);
            this.displayGroups["Group2"].Add("name", "Group2");
            this.displayGroups["Group2"].Add("is_active", false);

            this.displayGroups.Add("Group3", new Dictionary<string, object>());
            this.displayGroups["Group3"].Add("player", this.vlcPlayer3);
            this.displayGroups["Group3"].Add("border", this.vlcBorder3);
            this.displayGroups["Group3"].Add("deviceLabel", this.playerLabel3);
            this.displayGroups["Group3"].Add("batteryText", this.battery3);
            this.displayGroups["Group3"].Add("name", "Group3");
            this.displayGroups["Group3"].Add("is_active", false);

            this.displayGroups.Add("Group4", new Dictionary<string, object>());
            this.displayGroups["Group4"].Add("player", this.vlcPlayer4);
            this.displayGroups["Group4"].Add("border", this.vlcBorder4);
            this.displayGroups["Group4"].Add("deviceLabel", this.playerLabel4);
            this.displayGroups["Group4"].Add("batteryText", this.battery4);
            this.displayGroups["Group4"].Add("name", "Group4");
            this.displayGroups["Group4"].Add("is_active", false);

            this.displayGroups.Add("Group5", new Dictionary<string, object>());
            this.displayGroups["Group5"].Add("player", this.vlcPlayer5);
            this.displayGroups["Group5"].Add("border", this.vlcBorder5);
            this.displayGroups["Group5"].Add("deviceLabel", this.playerLabel5);
            this.displayGroups["Group5"].Add("batteryText", this.battery5);
            this.displayGroups["Group5"].Add("name", "Group5");
            this.displayGroups["Group5"].Add("is_active", false);

            this.displayGroups.Add("Group6", new Dictionary<string, object>());
            this.displayGroups["Group6"].Add("player", this.vlcPlayer6);
            this.displayGroups["Group6"].Add("border", this.vlcBorder6);
            this.displayGroups["Group6"].Add("deviceLabel", this.playerLabel6);
            this.displayGroups["Group6"].Add("batteryText", this.battery6);
            this.displayGroups["Group6"].Add("name", "Group6");
            this.displayGroups["Group6"].Add("is_active", false);

        }

        // set up timer for battery life updating
        public void DispatchedTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer.Start();
        }

        // populates lists of display objects
        private void PopulatePlayerList()
        {
            vlcPlayers.Add(this.vlcPlayer1);
            vlcPlayers.Add(this.vlcPlayer2);
            vlcPlayers.Add(this.vlcPlayer3);
            vlcPlayers.Add(this.vlcPlayer4);
            vlcPlayers.Add(this.vlcPlayer5);
            vlcPlayers.Add(this.vlcPlayer6);

            vlcBorders.Add(this.vlcBorder1);
            vlcBorders.Add(this.vlcBorder2);
            vlcBorders.Add(this.vlcBorder3);
            vlcBorders.Add(this.vlcBorder4);
            vlcBorders.Add(this.vlcBorder5);
            vlcBorders.Add(this.vlcBorder6);

            batteryTexts.Add(this.battery1);
            batteryTexts.Add(this.battery2);
            batteryTexts.Add(this.battery3);
            batteryTexts.Add(this.battery4);
            batteryTexts.Add(this.battery5);
            batteryTexts.Add(this.battery6);
        }
        #endregion
        
        #region Device Connection/Selection
        // Enables Connect Button
        private void EnableConnectButton()
        {
            bool enable = (!string.IsNullOrWhiteSpace(this.address.Text) &&
                        !string.IsNullOrWhiteSpace(this.username.Text) &&
                        !string.IsNullOrWhiteSpace(this.password.Password)
                        );

            this.connectToDevice.IsEnabled = enable;
        }

        // enables use of device sign in fields and connect button
        private void EnableConnectionControls(bool enable)
        {
            this.address.IsEnabled = enable;
            this.username.IsEnabled = enable;
            this.password.IsEnabled = enable;

            this.connectToDevice.IsEnabled = enable;
            this.loadDevices.IsEnabled = enable;
            this.saveDevices.IsEnabled = enable;
        }

        // Click handler for connectToDevice Button
        private void ConnectToDevice_Click(object sender, RoutedEventArgs e)
        {
            ConnectToDevice(this.address.Text, this.username.Text, this.password.Password);
        }

        private async void ConnectToDevice(string address, string username, string password)
        {
            // disable controls for now
            this.EnableConnectionControls(false);
            this.EnableAppLaunch(false);

            // clear output text
            this.ClearOutput();

            // initiate new device portal
            DevicePortal portal = null;
            try
            {
                portal = null;
                portal = new DevicePortal(
                new DefaultDevicePortalConnection(
                        address,
                        username,
                        password));

                // initiate new stringbuilder for output text
                StringBuilder sb = new StringBuilder();

                // if there is existing output, this new output will go at the end of it
                sb.Append(this.outputBox.Text);
                sb.AppendLine("Connecting...");

                portal.ConnectionStatus += (portalArg, connectArgs) =>
                {
                    if (connectArgs.Status == DeviceConnectionStatus.Connected)
                    {
                        sb.Append("Connected to: ");
                        sb.AppendLine(portalArg.Address);
                        sb.Append("OS version: ");
                        sb.AppendLine(portalArg.OperatingSystemVersion);
                        sb.Append("Device family: ");
                        sb.AppendLine(portalArg.DeviceFamily);
                        sb.Append("Platform: ");
                        sb.AppendLine(String.Format("{0} ({1})",
                            portalArg.PlatformName,
                            portalArg.Platform.ToString()));

                    }
                    else if (connectArgs.Status == DeviceConnectionStatus.Failed)
                    {
                        sb.AppendLine("Failed to connect to the device.");
                        sb.AppendLine(connectArgs.Message);
                    }
                };

                string deviceName = "";
                try
                {
                    deviceName = await portal.GetDeviceNameAsync();
                }
                catch(Exception ex)
                {
                    outputBox.Text += ex.Message;
                }

                if (availDeviceNames.Contains(deviceName) || currDeviceNames.Contains(deviceName))
                {
                    sb.Clear();
                    sb.AppendLine("Already connected to this device!");
                }
                else
                {
                    // add portal to active portals list
                    this.availPortals.Add(portal);
                    this.availDeviceNames.Add(deviceName);
                    this.UpdateListViews();

                    this.allPortals[portal.Address] = new Dictionary<string, object>();
                    allPortals[portal.Address].Add("username", username);
                    allPortals[portal.Address].Add("password", password);
                    allPortals[portal.Address].Add("name", deviceName);
                    allPortals[portal.Address].Add("is_active", false);
                    allPortals[portal.Address].Add("portal", portal);
                    allPortals[portal.Address].Add("displayGroup", "None");

                }

                try
                {
                    // by default, we will allow untrusted connections
                    //TODO: add checkbox
                    this.certificate = await portal.GetRootDeviceCertificateAsync(true);
                    await portal.ConnectAsync(manualCertificate: this.certificate);
                }
                catch (Exception exception)
                {
                    sb.AppendLine(exception.Message);
                }
                this.saveDevices.IsEnabled = true;
                // update the output
                this.outputBox.Text += sb.ToString();
            }
            catch (Exception exception)
            {
                outputBox.Text += exception.Message;
            }

            // enable controls
            this.EnableAppLaunch(true);
            this.EnableConnectionControls(true);
            this.EnableDeviceSelection(true);

        }

        // handler for add device button
        private void AddDevice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ClearOutput();

                if (availDeviceList.SelectedItem != null)
                {
                    string deviceName = availDeviceList.SelectedItem.ToString();

                    foreach (var item in allPortals.ToArray())
                    {
                        if (item.Value["name"].Equals(deviceName))
                        {
                            allPortals[item.Key]["is_active"] = true;
                            availDeviceNames.Remove(deviceName);
                            currDeviceNames.Add(deviceName);
                            this.UpdateListViews();
                            try
                            {
                                this.UpdateStreaming();
                            }
                            catch (Exception ex)
                            {
                                outputBox.Text += ex.Message;
                            }
                        }
                    }

                    this.EnableDeviceSelection(true);
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message + "\n";
            }
        }

        // handler for remove device button
        private void RemoveDevice_Click(object sender, RoutedEventArgs e)
        {
            this.ClearOutput();

            try
            {
                if (currDeviceList.SelectedItem != null)
                {
                    string deviceName = currDeviceList.SelectedItem.ToString();

                    foreach (var item in allPortals)
                    {
                        if (item.Value["name"].Equals(deviceName))
                        {
                            allPortals[item.Key]["is_active"] = false;
                            availDeviceNames.Add(deviceName);
                            currDeviceNames.Remove(deviceName);
                            this.UpdateListViews();

                            try
                            {
                                this.UpdateStreaming();
                            }
                            catch (Exception ex)
                            {
                                outputBox.Text += ex.Message;
                            }
                        }
                    }

                    this.EnableDeviceSelection(true);
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }

        // updates the available and current device lists
        private void UpdateListViews()
        {
            this.currDeviceList.ItemsSource = null;
            this.currDeviceList.ItemsSource = currDeviceNames;
            this.availDeviceList.ItemsSource = null;
            this.availDeviceList.ItemsSource = availDeviceNames;
        }

        // enables use of device selection, adding and removing
        private void EnableDeviceSelection(bool enable)
        {
            this.availDeviceList.IsEnabled = enable;
            this.addDevice.IsEnabled = enable;
            this.currDeviceList.IsEnabled = enable;
            this.removeDevice.IsEnabled = enable;
        }

        // handler for available device selection listview
        private void ListView_AvailDeviceSelected(object sender, SelectionChangedEventArgs e)
        {
            addDevice.IsEnabled = true;
        }

        // handler for current device selection listview
        private async void ListView_CurrDeviceSelected(object sender, SelectionChangedEventArgs e)
        {
            if (currDeviceList.SelectedItem != null)
            {
                // enable removing device
                this.removeDevice.IsEnabled = true;

                DevicePortal currPortal = null;

                foreach (var item in allPortals)
                {
                    if (item.Value["name"] as string == currDeviceList.SelectedItem.ToString())
                    {
                        currPortal = item.Value["portal"] as DevicePortal;
                    }
                }

                if (currPortal != null)
                {
                    try
                    {
                        availAppPackages.Clear();
                        availAppNames.Clear();
                        AppPackages appPackages = await currPortal.GetInstalledAppPackagesAsync();
                        foreach (PackageInfo package in appPackages.Packages)
                        {
                            availAppPackages.Add(package);
                            availAppNames.Add(package.Name);
                        }
                    }
                    catch (Exception ex)
                    {
                        outputBox.Text += ex.Message;
                    }
                }
                else
                {
                    availAppPackages.Clear();
                    availAppNames.Clear();
                    outputBox.Text += "Unable to fetch installed apps list. \n";
                }
                availAppList.ItemsSource = null;
                availAppList.ItemsSource = availAppNames;
                this.EnableDeviceSelection(true);
            }
        }
        #endregion

        #region App Launch Controls
        // enables use of app selection and launch button
        private void EnableAppLaunch(bool enable)
        {
            this.launchApp.IsEnabled = enable;
            this.killApp.IsEnabled = enable;
            this.availAppList.IsEnabled = enable;
        }
        // handler for app selection listview
        private void ListView_AppSelected(object sender, SelectionChangedEventArgs e)
        {
            launchApp.IsEnabled = true;
        }

        // handler for app launch button. launches selected app on all active portals, if the app 
        // is installed on that portal.
        private async void LaunchApp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool reenableDeviceControls = false;
                if (availAppList.SelectedItem != null)
                {
                    this.ClearOutput();
                    string selectedApp = availAppList.SelectedItem.ToString();
                    string selectedAppID = null;
                    foreach (PackageInfo package in availAppPackages)
                    {
                        if (package.Name == selectedApp)
                        {
                            selectedAppID = package.AppId;
                        }
                    }
                    if (selectedAppID != null)
                    {
                        // clear output and disable controls (for now)
                        this.ClearOutput();
                        this.EnableConnectionControls(false);
                        this.EnableAppLaunch(false);

                        StringBuilder sb = new StringBuilder();
                        sb.Append(outputBox.Text);
                        sb.AppendLine("Launching to device...");
                        outputBox.Text = sb.ToString();

                        foreach (var item in allPortals)
                        {
                            if (item.Value["is_active"].Equals(true))
                            {
                                string deviceName = item.Value["name"] as string;
                                DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                                try
                                {
                                    await currPortal.LaunchApplicationAsync(selectedAppID, selectedApp);
                                    sb.AppendLine("Launched " + selectedApp + " to " + deviceName);
                                    reenableDeviceControls = true;
                                }
                                catch (Exception ex)
                                {
                                    sb.AppendLine("Failed to launch app to device" + deviceName);
                                    sb.AppendLine(ex.GetType().ToString() + " - " + ex.Message);
                                    reenableDeviceControls = true;
                                }
                            }
                        }

                        outputBox.Text = sb.ToString();

                    }
                    else
                    {
                        outputBox.Text += "Unable to find selected App ID. \n";
                        reenableDeviceControls = true;
                    }
                    this.EnableAppLaunch(reenableDeviceControls);
                    this.EnableConnectionControls(true);
                }
                else
                {
                    outputBox.Text += "No app selected to launch.\n";
                }

            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }

        // kills the selected app on all active portals. if app is not running, killing it has no effect.
        private async void KillApp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool reenableDeviceControls = false;
                if (availAppList.SelectedItem != null)
                {
                    this.ClearOutput();
                    string selectedApp = availAppList.SelectedItem.ToString();
                    string selectedAppID = null;
                    PackageInfo currPackage = null;
                    foreach (PackageInfo package in availAppPackages)
                    {
                        if (package.Name == selectedApp)
                        {
                            selectedAppID = package.AppId;
                            currPackage = package;
                        }
                    }
                    if (selectedAppID != null && currPackage != null)
                    {
                        // clear output and disable controls (for now)
                        this.ClearOutput();
                        this.EnableConnectionControls(false);
                        this.EnableAppLaunch(false);

                        StringBuilder sb = new StringBuilder();
                        sb.Append(outputBox.Text);
                        sb.AppendLine("Killing app on device...");
                        outputBox.Text = sb.ToString();

                        foreach (var item in allPortals)
                        {
                            string deviceName = item.Value["name"] as string;
                            DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                            try
                            {
                                await currPortal.TerminateApplicationAsync(currPackage.FullName);
                                sb.AppendLine(selectedApp + " is no longer running on " + deviceName);
                                reenableDeviceControls = true;
                            }
                            catch (Exception ex)
                            {
                                sb.AppendLine("Failed to kill app on device" + deviceName);
                                sb.AppendLine(ex.GetType().ToString() + " - " + ex.Message);
                                reenableDeviceControls = true;
                            }
                        }

                        outputBox.Text = sb.ToString();

                    }
                    else
                    {
                        outputBox.Text += "Unable to find selected App ID. \n";
                        reenableDeviceControls = true;
                    }
                    this.EnableAppLaunch(reenableDeviceControls);
                    this.EnableConnectionControls(true);
                }
                else
                {
                    outputBox.Text = "No app selected to kill. \n";
                }

            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }
        }
        #endregion

        #region Device Login Controls
        /// Loads a certificates asynchronously (runs on the UI thread).
        private async Task LoadCertificate()
        {
            this.ClearOutput();
            try
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.Downloads;
                filePicker.FileTypeFilter.Add(".cer");

                StorageFile file = await filePicker.PickSingleFileAsync();

                if (file != null)
                {
                    IBuffer cerBlob = await FileIO.ReadBufferAsync(file);

                    if (cerBlob != null)
                    {
                        certificate = new Certificate(cerBlob);
                    }
                }
            }
            catch (Exception exception)
            {
                this.outputBox.Text += "Failed to get cert file: " + exception.Message;
            }
        }

        // enables connection functionality
        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            EnableConnectButton();
        }

        // enables connection functionality
        private void Address_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableConnectButton();
        }

        // enables connection functionality
        private void Username_TextChanged(object sender, TextChangedEventArgs e)
        {
            EnableConnectButton();
        }

        // retrieves the username and password from portal dictionary to allow vlcPlayer 
        // access to MRC on a real (not emulated) HoloLens
        private void VlcPlayer_ShowLoginDialog(object sender, LoginDialogEventArgs e)
        {
            try
            {
                if (this.streamingPortal != null)
                {
                    e.DialogResult = new LoginDialogResult() { Username = allPortals[streamingPortal.Address]["username"] as string, Password = allPortals[streamingPortal.Address]["password"] as string };
                }
                else
                {
                    outputBox.Text += "Unable to authenticate username and password. Streaming will not be available for this device. \n";
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }
        #endregion

        #region Player Display Controls
        // show or hide players 2-6
        private void EnableExtraPlayers(bool enable)
        {
            HideElement(this.vlcBorder2, true);
            HideElement(this.vlcBorder3, true);
            HideElement(this.vlcBorder4, true);
            HideElement(this.vlcBorder5, true);
            HideElement(this.vlcBorder6, true);
        }

        // hide an individual element (e.g. the border around a player)
        private void HideElement(UIElement element, bool hide)
        {
            if (hide == true)
            {
                element.Visibility = Visibility.Collapsed;
            }
            else
            {
                element.Visibility = Visibility.Visible;
            }
        }

        // switches sources between tapped vlcPlayer and player 1 to enlarge a chosen MRC stream
        private void VlcPlayer_Tapped(object sender, TappedRoutedEventArgs e)
        {
            is_player_updating = true;

            VLC.MediaElement currPlayer = sender as VLC.MediaElement;
            string currSource = currPlayer.Source;
            string destSource = this.vlcPlayer1.Source;
            string currKey = "None";
            string destKey = "None";

            foreach (var item in allPortals)
            {
                if (currSource.Contains(item.Key))
                {
                    currKey = item.Key;
                }
                if (destSource.Contains(item.Key))
                {
                    destKey = item.Key;
                }
            }

            Dictionary<string, object> currGroup = allPortals[currKey]["displayGroup"] as Dictionary<string, object>;
            allPortals[destKey]["displayGroup"] = currGroup;
            currPlayer.Source = null;
            currPlayer.Source = destSource;

            allPortals[currKey]["displayGroup"] = displayGroups["Group1"];
            this.vlcPlayer1.Source = null;
            this.vlcPlayer1.Source = currSource;

            GetBatteryLife(allPortals[currKey]["portal"] as DevicePortal, displayGroups["Group1"]["batteryText"] as TextBox);
            GetBatteryLife(allPortals[destKey]["portal"] as DevicePortal, currGroup["batteryText"] as TextBox);

            UpdateDeviceLabel(allPortals[currKey]["name"] as string, displayGroups["Group1"]["deviceLabel"] as TextBox);
            UpdateDeviceLabel(allPortals[destKey]["name"] as string, currGroup["deviceLabel"] as TextBox);

            is_player_updating = false;
        }

        // handles showing the command bar over a player when moused over
        private void Grid_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Grid grid = sender as Grid;
            UIElementCollection children = grid.Children;
            try
            {
                Border border = children[0] as Border;
                VLC.MediaElement player = border.Child as VLC.MediaElement;
                if (player.Source != null)
                {
                    CommandBar bar = children[1] as CommandBar;
                    bar.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }
        }

        // handles hiding the command bar on a player when the mouse is not over the player
        private void Grid_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Grid grid = sender as Grid;
            UIElementCollection children = grid.Children;
            try
            {
                CommandBar bar = children[1] as CommandBar;
                bar.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                outputBox.Text = ex.Message;
            }
        }

        // updates the device name displayed in the player label
        private void UpdateDeviceLabel(string deviceName, TextBox playerLabel)
        {
            playerLabel.Text = deviceName;
        }

        // gets the battery life of the device via the portal and updates the battery text box
        private async void GetBatteryLife(DevicePortal portal, TextBox batteryText)
        {
            try
            {
                BatteryState batteryState = await portal.GetBatteryStateAsync();

                float batteryLevel = batteryState.Level;
                int batteryLevelInt = Convert.ToInt32(batteryLevel);
                batteryText.Text = batteryLevelInt.ToString() + "%";
                if (batteryLevel > 75)
                {
                    batteryText.Foreground = new SolidColorBrush(Colors.DarkGreen);
                }
                else if (batteryLevel > 50)
                {
                    batteryText.Foreground = new SolidColorBrush(Colors.GreenYellow);
                }
                else if (batteryLevel > 25)
                {
                    batteryText.Foreground = new SolidColorBrush(Colors.Yellow);
                }
                else
                {
                    batteryText.Foreground = new SolidColorBrush(Colors.Red);
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }

        // updates all battery text boxes
        private void UpdatePlayerDisplays()
        {
            try
            {
                foreach (var item in allPortals)
                {
                    if (item.Value["is_active"].Equals(true))
                    {
                        DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                        Dictionary<string, object> displayGroup = item.Value["displayGroup"] as Dictionary<string, object>;
                        TextBox batteryText = displayGroup["batteryText"] as TextBox;
                        GetBatteryLife(currPortal, batteryText);
                    }
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += "Attempted to update battery display without success. " + ex.Message + "\n";
            }

        }

        // hides command bars (label, photo, and video buttons)
        private void HideCommandBars()
        {
            this.commandBar1.Visibility = Visibility.Collapsed;
            this.commandBar2.Visibility = Visibility.Collapsed;
            this.commandBar3.Visibility = Visibility.Collapsed;
            this.commandBar4.Visibility = Visibility.Collapsed;
            this.commandBar5.Visibility = Visibility.Collapsed;
            this.commandBar6.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region Recording Handlers
        // starts a MRC recording on the device
        private async void StartRecordPlayer(VLC.MediaElement player)
        {
            this.ClearOutput();
            StringBuilder sb = new StringBuilder();
            sb.Append("Attempting to begin recording from this player...");
            outputBox.Text = sb.ToString();

            string source = player.Source;
            await Task.Delay(10);
            foreach (var item in allPortals)
            {
                if (source.Contains(item.Key))
                {
                    try
                    {
                        DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                        List<Device> devices = await currPortal.GetDeviceListAsync();
                        foreach (Device device in devices)
                        {
                            outputBox.Text += device.FriendlyName + "\n";
                        }
                        await currPortal.StartMrcRecordingAsync();
                        sb.AppendLine("Recording started successfully on " + item.Value["name"] as string + ". Live stream may not be available during recording.");
                        outputBox.Text = sb.ToString();
                    }
                    catch (Exception ex)
                    {
                        sb.AppendLine(ex.Message);
                        sb.AppendLine("Unable to begin recording.");
                        outputBox.Text = sb.ToString();
                    }
                }
            }
        }

        // stops and saves a MRC recording on the device and to the local machine
        private async void StopRecordPlayer(VLC.MediaElement player)
        {
            this.ClearOutput();
            StringBuilder sb = new StringBuilder();
            sb.Append("Attempting to stop recording from this player...");
            outputBox.Text = sb.ToString();

            string source = player.Source;
            await Task.Delay(10);

            foreach (var item in allPortals)
            {
                if (source.Contains(item.Key))
                {
                    try
                    {
                        DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                        await currPortal.StopMrcRecordingAsync();
                        string deviceName = item.Value["name"] as string;
                        string time = DateTime.Now.ToString("s");

                        sb.AppendLine("Video recording ended.");

                        MrcFileList fileList = await currPortal.GetMrcFileListAsync();
                        List<MrcFileInformation> files = fileList.Files;
                        files.Sort((MrcFileInformation file1, MrcFileInformation file2) => { return file1.CreationTimeRaw.CompareTo(file2.CreationTimeRaw); });
                        string fileName = files.Last().FileName;
                        var asciiFilename = ASCIIEncoding.ASCII.GetBytes(fileName);
                        var base64Filename = Convert.ToBase64String(asciiFilename);
                        var uriFilename = Uri.EscapeUriString(base64Filename);
                        Uri uri = new Uri("http://" + currPortal.Address + "/api/holographic/mrc/file?filename=" + Convert.ToBase64String(asciiFilename) + "&op=stream");
                        using (HttpClient httpClient = new HttpClient())
                        {
                            HttpResponseMessage message = await httpClient.GetAsync(uri);

                            var savePicker = new FileSavePicker();
                            savePicker.DefaultFileExtension = ".mp4";
                            savePicker.FileTypeChoices.Add(".mp4", new List<string> { ".mp4" });
                            savePicker.SuggestedStartLocation = PickerLocationId.VideosLibrary;
                            savePicker.SuggestedFileName = deviceName + "_" + time + ".mp4";

                            var saveFile = await savePicker.PickSaveFileAsync();

                            if (saveFile == null)
                            {
                                return;
                            }

                            sb.AppendLine("Writing file to specified location.");
                            outputBox.Text = sb.ToString();
                            using (var fileStream = await saveFile.OpenAsync(FileAccessMode.ReadWrite))
                            {
                                await message.Content.WriteToStreamAsync(fileStream);
                            }
                        }
                        //player.Source = source;
                        sb.AppendLine("Video capture and save successful!");
                        outputBox.Text = sb.ToString();


                    }
                    catch (Exception ex)
                    {
                        player.Source = source;
                        if (ex.Message != "")
                        {
                            sb.AppendLine(ex.Message);
                        }
                        sb.AppendLine("Unable to stop recording and save video.");
                        outputBox.Text = sb.ToString();
                    }
                }

            }
        }

        // takes an MRC photo on the device. will continue attempting until photo is taken successfully.
        private async void SnapshotPlayer(VLC.MediaElement player)
        {
            this.is_player_updating = true;
            this.ClearOutput();
            StringBuilder sb = new StringBuilder();
            sb.Append("Attempting to take a snapshot of this player...");
            outputBox.Text = sb.ToString();

            string source = player.Source;
            await Task.Delay(10);
            foreach (var item in allPortals)
            {
                if (source.Contains(item.Key))
                {
                    try
                    {
                        DevicePortal currPortal = item.Value["portal"] as DevicePortal;
                        string deviceName = item.Value["name"] as string;
                        string time = DateTime.Now.ToString("s");

                        player.Source = null;
                        sb.AppendLine("Live stream MRC paused.");
                        outputBox.Text = sb.ToString();

                        await currPortal.TakeMrcPhotoAsync();
                        sb.AppendLine("Photo taken successfully on device.");
                        outputBox.Text = sb.ToString();
                        MrcFileList fileList = await currPortal.GetMrcFileListAsync();
                        List<MrcFileInformation> files = fileList.Files;
                        files.Sort((MrcFileInformation file1, MrcFileInformation file2) => { return file1.CreationTimeRaw.CompareTo(file2.CreationTimeRaw); });
                        string fileName = files.Last().FileName;
                        var asciiFilename = ASCIIEncoding.ASCII.GetBytes(fileName);
                        var base64Filename = Convert.ToBase64String(asciiFilename);
                        var uriFilename = Uri.EscapeUriString(base64Filename);
                        Uri uri = new Uri("http://" + currPortal.Address + "/api/holographic/mrc/file?filename=" + Convert.ToBase64String(asciiFilename) + "&op=stream");
                        using (HttpClient httpClient = new HttpClient())
                        {
                            HttpResponseMessage message = await httpClient.GetAsync(uri);

                            var savePicker = new FileSavePicker();
                            savePicker.DefaultFileExtension = ".jpg";
                            savePicker.FileTypeChoices.Add(".jpg", new List<string> { ".jpg" });
                            savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                            savePicker.SuggestedFileName = deviceName + "_" + time + ".jpg";

                            var saveFile = await savePicker.PickSaveFileAsync();

                            if (saveFile == null)
                            {
                                player.Source = source;
                                sb.AppendLine("No save file selected. Photo will still be saved on device.");
                                outputBox.Text = sb.ToString();
                                return;
                            }

                            sb.AppendLine("Writing file to specified location.");
                            outputBox.Text = sb.ToString();
                            using (var fileStream = await saveFile.OpenAsync(FileAccessMode.ReadWrite))
                            {
                                await message.Content.WriteToStreamAsync(fileStream);
                            }
                        }
                        player.Source = source;
                        sb.AppendLine("Photo capture and save successful!");
                        outputBox.Text = sb.ToString();


                    }
                    catch (Exception ex)
                    {
                        player.Source = source;
                        if (ex.Message != "")
                        {
                            sb.AppendLine(ex.Message);
                        }
                        sb.AppendLine("Unsuccessful photo capture. Trying again...");
                        outputBox.Text = sb.ToString();
                        SnapshotPlayer(player);
                    }
                }

            }
            this.is_player_updating = false;
        }

        // retrieves live video from portal and displays it in specified vlcPlayer
        private void UpdateStreaming()
        {
            this.ClearOutput();
            try
            {
                List<string> displayGroupNames = new List<string>();
                displayGroupNames.Add("Group1");
                displayGroupNames.Add("Group2");
                displayGroupNames.Add("Group3");
                displayGroupNames.Add("Group4");
                displayGroupNames.Add("Group5");
                displayGroupNames.Add("Group6");

                // remove all display groups from portals
                foreach (var item in allPortals)
                {
                    item.Value["displayGroup"] = "None";
                }

                // loop through all display groups
                foreach (string name in displayGroupNames)
                {
                    Dictionary<string, object> group = displayGroups[name];

                    // first, set the group player source to null
                    VLC.MediaElement player = group["player"] as VLC.MediaElement;
                    player.Source = null;
                    player.Opacity = 0.0;

                    // clear the battery text and label
                    TextBox batteryText = group["batteryText"] as TextBox;
                    batteryText.Text = "";
                    TextBox deviceLabel = group["deviceLabel"] as TextBox;
                    deviceLabel.Text = "";

                    // reset the group to inactive
                    displayGroups[name]["is_active"] = false;

                    // loop through portals to find active ones
                    foreach (var item in allPortals)
                    {
                        // if this group hasn't been filled yet...
                        if (displayGroups[name]["is_active"].Equals(false))
                        {
                            // and the portal is active...
                            if (item.Value["is_active"].Equals(true) && item.Value["displayGroup"].Equals("None"))
                            {
                                // set the group to active
                                displayGroups[name]["is_active"] = true;

                                // set the player's source
                                this.streamingPortal = item.Value["portal"] as DevicePortal;
                                string sourceAddress = "http://" + streamingPortal.Address + "/api/holographic/stream/live_high.mp4?holo=true&pv=true&mic=false";
                                player.Source = sourceAddress;
                                player.Opacity = 100.0;

                                // make this the portal's assigned displayGroup
                                item.Value["displayGroup"] = group;

                                // for groups beyond group 1, the border needs to be revealed
                                if (name != "Group1")
                                {
                                    HideElement(group["border"] as Border, false);
                                }

                                // update battery text and device label
                                GetBatteryLife(streamingPortal, batteryText);
                                UpdateDeviceLabel(item.Value["name"] as string, deviceLabel);

                                outputBox.Text += player.Name + "is streaming from " + sourceAddress + "\n";

                            }
                        }

                    }
                    // if we've looped through all of the active portals, but we didn't assign one to this group, hide the border if this isn't group1
                    if (displayGroups[name]["is_active"].Equals(false) && name != "Group1")
                    {
                        HideElement(displayGroups[name]["border"] as Border, true);
                    }
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }

        }
        #endregion

        #region Camera/Video Event Handlers
        // event handler for camera button on player 1
        private void CameraButton1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SnapshotPlayer(this.vlcPlayer1);
        }

        // event handler for camera button on player 2
        private void CameraButton2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.SnapshotPlayer(this.vlcPlayer2);
        }

        // event handler for camera button on player 3
        private void CameraButton3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.SnapshotPlayer(this.vlcPlayer3);
        }

        // event handler for camera button on player 4
        private void CameraButton4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.SnapshotPlayer(this.vlcPlayer4);
        }

        // event handler for camera button on player 5
        private void CameraButton5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.SnapshotPlayer(this.vlcPlayer5);
        }

        // event handler for camera button on player 6
        private void CameraButton6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.SnapshotPlayer(this.vlcPlayer6);
        }

        // event handler for video button on player 1
        private void VideoButton1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder1.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer1);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder1.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer1);
                videoButton.Tag = "NotRecording";
            }
        }

        // event handler for video button on player 2
        private void VideoButton2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder2.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer2);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder2.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer2);
                videoButton.Tag = "NotRecording";
            }
        }

        // event handler for video button on player 3
        private void VideoButton3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder3.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer3);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder3.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer3);
                videoButton.Tag = "NotRecording";
            }
        }

        // event handler for video button on player 4
        private void VideoButton4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder4.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer4);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder4.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer4);
                videoButton.Tag = "NotRecording";
            }
        }

        // event handler for video button on player 5
        private void VideoButton5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder5.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer5);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder5.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer5);
                videoButton.Tag = "NotRecording";
            }
        }

        // event handler for video button on player 6
        private void VideoButton6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AppBarButton videoButton = sender as AppBarButton;

            if (videoButton.Tag.ToString() == "NotRecording")
            {
                videoButton.Icon = new SymbolIcon(Symbol.Stop);
                this.vlcBorder6.BorderBrush = new SolidColorBrush(Colors.Red);
                StartRecordPlayer(this.vlcPlayer6);
                videoButton.Tag = "Recording";
            }
            else
            {
                videoButton.Icon = new SymbolIcon(Symbol.Video);
                this.vlcBorder6.BorderBrush = new SolidColorBrush(Colors.Gray);
                StopRecordPlayer(this.vlcPlayer6);
                videoButton.Tag = "NotRecording";
            }
        }
        #endregion
        
        private async void loadDevices_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FileOpenPicker picker = new FileOpenPicker();
                picker.ViewMode = PickerViewMode.Thumbnail;
                picker.FileTypeFilter.Add(".txt");

                StorageFile storageFile = await picker.PickSingleFileAsync();
                string text = await FileIO.ReadTextAsync(storageFile);
                string[] elements = new string[24];
                elements = text.Split(Convert.ToChar(";"));

                for (int i = 1; i < elements.Length;)
                {
                    ConnectToDevice("http://" + elements[i], elements[i + 1], elements[i + 2]);
                    i += 4;
                }
            }
            catch (Exception ex)
            {
                outputBox.Text += ex.Message;
            }
        }

        private async void SaveDevices_Click(object sender, RoutedEventArgs e)
        {
            // save devices in the format
            // address; username; password; |
            // one device per line
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in allPortals)
            {
                stringBuilder.AppendLine(";" + item.Key + ";" + item.Value["username"] + ";" + item.Value["password"] + ";");
            }
            string saveText = stringBuilder.ToString();

            FileSavePicker savePicker = new FileSavePicker();
            string time = DateTime.Now.ToString("s");


            savePicker.DefaultFileExtension = ".txt";
            savePicker.FileTypeChoices.Add(".txt", new List<string> { ".txt" });
            savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            savePicker.SuggestedFileName = "SavedDevices" + "_" + time + ".txt";

            var saveFile = await savePicker.PickSaveFileAsync();

            if (saveFile == null)
            {
                outputBox.Text += "No save file selected. Device list will not be saved.";
                
                return;
            }
            
            outputBox.Text += "Writing file to specified location.";
            await FileIO.WriteTextAsync(saveFile, saveText);
        }
    }
}
