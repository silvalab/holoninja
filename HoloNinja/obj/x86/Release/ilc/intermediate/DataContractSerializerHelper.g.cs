using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Xml;

[assembly: global::System.Reflection.AssemblyVersion("4.0.0.0")]



namespace System.Runtime.Serialization.Generated
{
    [global::System.Runtime.CompilerServices.__BlockReflection]
    public static partial class DataContractSerializerHelper
    {
        public static void InitDataContracts()
        {
            global::System.Collections.Generic.Dictionary<global::System.Type, global::System.Runtime.Serialization.DataContract> dataContracts = global::System.Runtime.Serialization.DataContract.GetDataContracts();
            PopulateContractDictionary(dataContracts);
            global::System.Collections.Generic.Dictionary<global::System.Runtime.Serialization.DataContract, global::System.Runtime.Serialization.Json.JsonReadWriteDelegates> jsonDelegates = global::System.Runtime.Serialization.Json.JsonReadWriteDelegates.GetJsonDelegates();
            PopulateJsonDelegateDictionary(
                                dataContracts, 
                                jsonDelegates
                            );
        }
        static int[] s_knownContractsLists = new int[] {
              -1, }
        ;
        // Count = 1375
        static int[] s_xmlDictionaryStrings = new int[] {
                0, // array length: 0
                5, // array length: 5
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                5, // array length: 5
                4651, // index: 4651, string: "Code"
                4656, // index: 4656, string: "ErrorCode"
                4666, // index: 4666, string: "ErrorMessage"
                4679, // index: 4679, string: "Reason"
                4686, // index: 4686, string: "Success"
                5, // array length: 5
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4694, // index: 4694, string: "state"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4679, // index: 4679, string: "Reason"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                4700, // index: 4700, string: "Events"
                4707, // index: 4707, string: "Frequency"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                637, // index: 637, string: "Key"
                641, // index: 641, string: "Value"
                2, // array length: 2
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                674, // index: 674, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                2, // array length: 2
                4717, // index: 4717, string: "key"
                4721, // index: 4721, string: "value"
                2, // array length: 2
                674, // index: 674, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                674, // index: 674, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                4727, // index: 4727, string: "Processes"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                16, // array length: 16
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                16, // array length: 16
                4737, // index: 4737, string: "AppName"
                4745, // index: 4745, string: "CPUUsage"
                4754, // index: 4754, string: "ImageName"
                4764, // index: 4764, string: "IsRunning"
                4774, // index: 4774, string: "IsXAP"
                4780, // index: 4780, string: "PackageFullName"
                4796, // index: 4796, string: "PageFileUsage"
                4810, // index: 4810, string: "PrivateWorkingSet"
                4828, // index: 4828, string: "ProcessId"
                4838, // index: 4838, string: "Publisher"
                4848, // index: 4848, string: "SessionId"
                4858, // index: 4858, string: "TotalCommit"
                4870, // index: 4870, string: "UserName"
                4879, // index: 4879, string: "Version"
                4887, // index: 4887, string: "VirtualSize"
                4899, // index: 4899, string: "WorkingSetSize"
                16, // array length: 16
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                4914, // index: 4914, string: "Build"
                4920, // index: 4920, string: "Major"
                4926, // index: 4926, string: "Minor"
                4932, // index: 4932, string: "Revision"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                14, // array length: 14
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                14, // array length: 14
                4941, // index: 4941, string: "AvailablePages"
                4956, // index: 4956, string: "CommitLimit"
                4968, // index: 4968, string: "CommittedPages"
                4983, // index: 4983, string: "CpuLoad"
                4991, // index: 4991, string: "GPUData"
                4999, // index: 4999, string: "IOOtherSpeed"
                5012, // index: 5012, string: "IOReadSpeed"
                5024, // index: 5024, string: "IOWriteSpeed"
                5037, // index: 5037, string: "NetworkingData"
                5052, // index: 5052, string: "NonPagedPoolPages"
                5070, // index: 5070, string: "PageSize"
                5079, // index: 5079, string: "PagedPoolPages"
                5094, // index: 5094, string: "TotalInstalledInKb"
                5113, // index: 5113, string: "TotalPages"
                14, // array length: 14
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5124, // index: 5124, string: "AvailableAdapters"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                5142, // index: 5142, string: "DedicatedMemory"
                5158, // index: 5158, string: "DedicatedMemoryUsed"
                5178, // index: 5178, string: "Description"
                5190, // index: 5190, string: "EnginesUtilization"
                5209, // index: 5209, string: "SystemMemory"
                5222, // index: 5222, string: "SystemMemoryUsed"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5239, // index: 5239, string: "NetworkInBytes"
                5254, // index: 5254, string: "NetworkOutBytes"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5270, // index: 5270, string: "AvailableDevices"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5287, // index: 5287, string: "PairedDevices"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5301, // index: 5301, string: "PairResult"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                5312, // index: 5312, string: "ComputerName"
                5325, // index: 5325, string: "Language"
                5334, // index: 5334, string: "OsEdition"
                5344, // index: 5344, string: "OsEditionId"
                5356, // index: 5356, string: "OsVersion"
                5366, // index: 5366, string: "Platform"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5375, // index: 5375, string: "CrashDumps"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                5386, // index: 5386, string: "FileDate"
                5395, // index: 5395, string: "FileName"
                5404, // index: 5404, string: "FileSize"
                4780, // index: 4780, string: "PackageFullName"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5413, // index: 5413, string: "CrashDumpEnabled"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5430, // index: 5430, string: "InstalledPackages"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                5448, // index: 5448, string: "Name"
                5453, // index: 5453, string: "PackageFamilyName"
                4780, // index: 4780, string: "PackageFullName"
                5471, // index: 5471, string: "PackageOrigin"
                5485, // index: 5485, string: "PackageRelativeId"
                4838, // index: 4838, string: "Publisher"
                4879, // index: 4879, string: "Version"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                4914, // index: 4914, string: "Build"
                4920, // index: 4920, string: "Major"
                4926, // index: 4926, string: "Minor"
                4932, // index: 4932, string: "Revision"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5503, // index: 5503, string: "KnownFolders"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5516, // index: 5516, string: "Items"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                5522, // index: 5522, string: "CurrentDir"
                5533, // index: 5533, string: "DateCreated"
                5404, // index: 5404, string: "FileSize"
                5545, // index: 5545, string: "Id"
                5448, // index: 5448, string: "Name"
                5548, // index: 5548, string: "SubPath"
                5556, // index: 5556, string: "Type"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5561, // index: 5561, string: "DeviceList"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5572, // index: 5572, string: "Class"
                5178, // index: 5178, string: "Description"
                5578, // index: 5578, string: "FriendlyName"
                5591, // index: 5591, string: "ID"
                5594, // index: 5594, string: "Manufacturer"
                5607, // index: 5607, string: "ParentID"
                5616, // index: 5616, string: "ProblemCode"
                5628, // index: 5628, string: "StatusCode"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5639, // index: 5639, string: "tags"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5644, // index: 5644, string: "DumpFiles"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                5386, // index: 5386, string: "FileDate"
                5395, // index: 5395, string: "FileName"
                5404, // index: 5404, string: "FileSize"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                5654, // index: 5654, string: "autoreboot"
                5665, // index: 5665, string: "dumptype"
                5674, // index: 5674, string: "maxdumpcount"
                5687, // index: 5687, string: "overwrite"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                1860, // index: 1860, string: "Disabled"
                1869, // index: 1869, string: "CompleteMemoryDump"
                1888, // index: 1888, string: "KernelDump"
                1899, // index: 1899, string: "Minidump"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5697, // index: 5697, string: "Providers"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5707, // index: 5707, string: "GUID"
                5448, // index: 5448, string: "Name"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5712, // index: 5712, string: "Adapters"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5721, // index: 5721, string: "DHCP"
                5178, // index: 5178, string: "Description"
                5726, // index: 5726, string: "Gateways"
                5735, // index: 5735, string: "HardwareAddress"
                5751, // index: 5751, string: "Index"
                5757, // index: 5757, string: "IpAddresses"
                5448, // index: 5448, string: "Name"
                5556, // index: 5556, string: "Type"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                5769, // index: 5769, string: "Address"
                5777, // index: 5777, string: "LeaseExpires"
                5790, // index: 5790, string: "LeaseObtained"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5804, // index: 5804, string: "IpAddress"
                5814, // index: 5814, string: "Mask"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5819, // index: 5819, string: "DeviceType"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5312, // index: 5312, string: "ComputerName"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5830, // index: 5830, string: "ActivePowerScheme"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                8, // array length: 8
                5848, // index: 5848, string: "AcOnline"
                5857, // index: 5857, string: "BatteryPresent"
                5872, // index: 5872, string: "Charging"
                5881, // index: 5881, string: "DefaultAlert1"
                5895, // index: 5895, string: "DefaultAlert2"
                5909, // index: 5909, string: "EstimatedTime"
                5923, // index: 5923, string: "MaximumCapacity"
                5939, // index: 5939, string: "RemainingCapacity"
                8, // array length: 8
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5957, // index: 5957, string: "LowPowerState"
                5971, // index: 5971, string: "LowPowerStateAvailable"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                5994, // index: 5994, string: "Interfaces"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                5178, // index: 5178, string: "Description"
                5707, // index: 5707, string: "GUID"
                5751, // index: 5751, string: "Index"
                6005, // index: 6005, string: "ProfilesList"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6018, // index: 6018, string: "GroupPolicyProfile"
                5448, // index: 5448, string: "Name"
                6037, // index: 6037, string: "PerUserProfile"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6052, // index: 6052, string: "AvailableNetworks"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                13, // array length: 13
                -1, // string: null
                -1, // string: null
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                13, // array length: 13
                6070, // index: 6070, string: "AlreadyConnected"
                6087, // index: 6087, string: "AuthenticationAlgorithm"
                6111, // index: 6111, string: "BSSID"
                6117, // index: 6117, string: "Channel"
                6125, // index: 6125, string: "CipherAlgorithm"
                6141, // index: 6141, string: "Connectable"
                6153, // index: 6153, string: "InfrastructureType"
                6172, // index: 6172, string: "PhysicalTypes"
                6186, // index: 6186, string: "ProfileAvailable"
                6203, // index: 6203, string: "ProfileName"
                6215, // index: 6215, string: "SSID"
                6220, // index: 6220, string: "SecurityEnabled"
                6236, // index: 6236, string: "SignalQuality"
                13, // array length: 13
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6250, // index: 6250, string: "WerReports"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6261, // index: 6261, string: "Reports"
                6269, // index: 6269, string: "User"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6274, // index: 6274, string: "CreationTime"
                5448, // index: 5448, string: "Name"
                5556, // index: 5556, string: "Type"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6287, // index: 6287, string: "Files"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5448, // index: 5448, string: "Name"
                6293, // index: 6293, string: "Size"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6298, // index: 6298, string: "SoftwareStatus"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                6313, // index: 6313, string: "dwm.exe"
                6321, // index: 6321, string: "holoshellapp.exe"
                6338, // index: 6338, string: "holosi.exe"
                6349, // index: 6349, string: "mixedrealitycapture.exe"
                6373, // index: 6373, string: "sihost.exe"
                6384, // index: 6384, string: "spectrum.exe"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6397, // index: 6397, string: "Expected"
                6406, // index: 6406, string: "Observed"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6415, // index: 6415, string: "ipd"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6419, // index: 6419, string: "httpsRequired"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6433, // index: 6433, string: "streamId"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6442, // index: 6442, string: "mode"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                3136, // index: 3136, string: "Default"
                3144, // index: 3144, string: "Simulation"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6447, // index: 6447, string: "CurrentStage"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6460, // index: 6460, string: "MrcRecordings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6274, // index: 6274, string: "CreationTime"
                5395, // index: 5395, string: "FileName"
                5404, // index: 5404, string: "FileSize"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6474, // index: 6474, string: "MrcSettings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6486, // index: 6486, string: "Setting"
                641, // index: 641, string: "Value"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6494, // index: 6494, string: "IsRecording"
                6506, // index: 6506, string: "ProcessStatus"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6520, // index: 6520, string: "MrcProcess"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                6531, // index: 6531, string: "environment"
                6543, // index: 6543, string: "hands"
                6549, // index: 6549, string: "head"
                6554, // index: 6554, string: "spatialMapping"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6569, // index: 6569, string: "recordings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6580, // index: 6580, string: "recording"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                4656, // index: 4656, string: "ErrorCode"
                6590, // index: 6590, string: "Status"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                7, // array length: 7
                6597, // index: 6597, string: "TPMFamily"
                6607, // index: 6607, string: "TPMFirmware"
                6619, // index: 6619, string: "TPMManufacturer"
                6635, // index: 6635, string: "TPMRevision"
                6647, // index: 6647, string: "TPMStatus"
                6657, // index: 6657, string: "TPMType"
                6665, // index: 6665, string: "TPMVendor"
                7, // array length: 7
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6675, // index: 6675, string: "AcpiTables"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6686, // index: 6686, string: "AzureUri"
                6695, // index: 6695, string: "DeviceId"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6704, // index: 6704, string: "AzureToken"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6715, // index: 6715, string: "AppPackages"
                6727, // index: 6727, string: "DefaultApp"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6738, // index: 6738, string: "IsStartup"
                4780, // index: 4780, string: "PackageFullName"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6715, // index: 6715, string: "AppPackages"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                6748, // index: 6748, string: "CaptureName"
                6760, // index: 6760, string: "CaptureVolume"
                6774, // index: 6774, string: "LabelErrorCode"
                6789, // index: 6789, string: "LabelStatus"
                6801, // index: 6801, string: "RenderName"
                6812, // index: 6812, string: "RenderVolume"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                6825, // index: 6825, string: "output"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                6832, // index: 6832, string: "PrivateInterfaces"
                6850, // index: 6850, string: "PublicInterfaces"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                6867, // index: 6867, string: "SoftAPEnabled"
                6881, // index: 6881, string: "SoftApPassword"
                6896, // index: 6896, string: "SoftApSsid"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                6907, // index: 6907, string: "AllJoynOnboardingDefaultDescription"
                6943, // index: 6943, string: "AllJoynOnboardingDefaultManufacturer"
                6980, // index: 6980, string: "AllJoynOnboardingEnabled"
                7005, // index: 7005, string: "AllJoynOnboardingModelNumber"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                4764, // index: 4764, string: "IsRunning"
                7034, // index: 7034, string: "IsScheduled"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7046, // index: 7046, string: "DeviceModel"
                7058, // index: 7058, string: "DeviceName"
                7069, // index: 7069, string: "OSVersion"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                7079, // index: 7079, string: "Current"
                7087, // index: 7087, string: "Timezones"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                5178, // index: 5178, string: "Description"
                5751, // index: 5751, string: "Index"
                5448, // index: 5448, string: "Name"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7079, // index: 7079, string: "Current"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                7097, // index: 7097, string: "Day"
                7101, // index: 7101, string: "Hour"
                7106, // index: 7106, string: "Minute"
                7113, // index: 7113, string: "Month"
                7119, // index: 7119, string: "Second"
                7126, // index: 7126, string: "Year"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                526, // index: 526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7131, // index: 7131, string: "ControllersDrivers"
                7150, // index: 7150, string: "CurrentDriver"
                7164, // index: 7164, string: "RequestReboot"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7178, // index: 7178, string: "Orientation"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                7079, // index: 7079, string: "Current"
                7190, // index: 7190, string: "Resolutions"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                2, // array length: 2
                5751, // index: 5751, string: "Index"
                7202, // index: 7202, string: "Resolution"
                2, // array length: 2
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                6, // array length: 6
                7213, // index: 7213, string: "lastCheckTime"
                7227, // index: 7227, string: "lastFailTime"
                7240, // index: 7240, string: "lastUpdateTime"
                7255, // index: 7255, string: "stagingProgress"
                7271, // index: 7271, string: "updateState"
                7283, // index: 7283, string: "updateStatusMessage"
                6, // array length: 6
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7303, // index: 7303, string: "Sandbox"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                3, // array length: 3
                7311, // index: 7311, string: "Password"
                7320, // index: 7320, string: "Path"
                7325, // index: 7325, string: "Username"
                3, // array length: 3
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7334, // index: 7334, string: "Users"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                9, // array length: 9
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                9, // array length: 9
                7340, // index: 7340, string: "AutoSignIn"
                7351, // index: 7351, string: "Delete"
                7358, // index: 7358, string: "EmailAddress"
                7371, // index: 7371, string: "Gamertag"
                7311, // index: 7311, string: "Password"
                7380, // index: 7380, string: "SignedIn"
                7389, // index: 7389, string: "SponsoredUser"
                7403, // index: 7403, string: "UserId"
                7410, // index: 7410, string: "XboxUserId"
                9, // array length: 9
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                1, // array length: 1
                7421, // index: 7421, string: "Settings"
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                4, // array length: 4
                7430, // index: 7430, string: "Category"
                5448, // index: 5448, string: "Name"
                7439, // index: 7439, string: "RequiresReboot"
                641, // index: 641, string: "Value"
                4, // array length: 4
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295, // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
                295  // index: 295, string: "http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal"
        };
        // Count = 6
        static global::MemberEntry[] s_dataMemberLists = new global::MemberEntry[] {
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1860, // Disabled
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1869, // CompleteMemoryDump
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1888, // KernelDump
                    Value = 2,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1899, // Minidump
                    Value = 3,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3136, // Default
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3144, // Simulation
                    Value = 1,
                }
        };
        static readonly byte[] s_dataContractMap_Hashtable = null;
        // Count=177
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractMapEntry[] s_dataContractMap = new global::DataContractMapEntry[] {
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 16, // 0x10
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 160, // 0xa0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                "11d50a3a")),
                    TableIndex = 176, // 0xb0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 224, // 0xe0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 320, // 0x140
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1, // 0x1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                "rapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 17, // 0x11
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 33, // 0x21
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 49, // 0x31
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 2, // 0x2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 18, // 0x12
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 97, // 0x61
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 34, // 0x22
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 113, // 0x71
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 129, // 0x81
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 145, // 0x91
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 161, // 0xa1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 50, // 0x32
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 177, // 0xb1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Single, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                "d50a3a")),
                    TableIndex = 66, // 0x42
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 193, // 0xc1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                "rsalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 209, // 0xd1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 82, // 0x52
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 225, // 0xe1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 241, // 0xf1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 257, // 0x101
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 273, // 0x111
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 289, // 0x121
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 305, // 0x131
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 98, // 0x62
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 321, // 0x141
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 337, // 0x151
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 353, // 0x161
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 114, // 0x72
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 369, // 0x171
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 385, // 0x181
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 401, // 0x191
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                "d50a3a")),
                    TableIndex = 130, // 0x82
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 417, // 0x1a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 146, // 0x92
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                "ndows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 433, // 0x1b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 449, // 0x1c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 162, // 0xa2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                "9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 465, // 0x1d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 481, // 0x1e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, " +
                                "Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 497, // 0x1f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 178, // 0xb2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Cult" +
                                "ure=neutral, PublicKeyToken=null")),
                    TableIndex = 513, // 0x201
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4" +
                                ".0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 529, // 0x211
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[Microsoft.Tools.WindowsDevicePortal.DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 545, // 0x221
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 194, // 0xc2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 561, // 0x231
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 577, // 0x241
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 210, // 0xd2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 593, // 0x251
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                "4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 609, // 0x261
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 625, // 0x271
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 226, // 0xe2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 641, // 0x281
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 657, // 0x291
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 673, // 0x2a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 689, // 0x2b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 705, // 0x2c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 721, // 0x2d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 242, // 0xf2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 737, // 0x2e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 258, // 0x102
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 753, // 0x2f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 769, // 0x301
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 274, // 0x112
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 785, // 0x311
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                "50a3a")),
                    TableIndex = 290, // 0x122
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 801, // 0x321
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 306, // 0x132
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 817, // 0x331
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 322, // 0x142
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 833, // 0x341
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 849, // 0x351
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 338, // 0x152
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 865, // 0x361
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 881, // 0x371
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 897, // 0x381
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 913, // 0x391
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 929, // 0x3a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 945, // 0x3b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                ".UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 961, // 0x3c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                "versalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 977, // 0x3d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 993, // 0x3e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1009, // 0x3f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 354, // 0x162
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1025, // 0x401
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1041, // 0x411
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 370, // 0x172
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1057, // 0x421
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1073, // 0x431
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1089, // 0x441
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                "ersalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1105, // 0x451
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                "UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1121, // 0x461
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                "r.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1137, // 0x471
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1153, // 0x481
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1169, // 0x491
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1185, // 0x4a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1201, // 0x4b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1217, // 0x4c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1233, // 0x4d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1249, // 0x4e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 386, // 0x182
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1265, // 0x4f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1281, // 0x501
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1297, // 0x511
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1313, // 0x521
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1329, // 0x531
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1345, // 0x541
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1361, // 0x551
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1377, // 0x561
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1393, // 0x571
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1409, // 0x581
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1425, // 0x591
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 402, // 0x192
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1441, // 0x5a1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1457, // 0x5b1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1473, // 0x5c1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1489, // 0x5d1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1505, // 0x5e1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1521, // 0x5f1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 418, // 0x1a2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1537, // 0x601
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1553, // 0x611
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1569, // 0x621
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1585, // 0x631
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1601, // 0x641
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 434, // 0x1b2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1617, // 0x651
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1633, // 0x661
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 450, // 0x1c2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    TableIndex = 1649, // 0x671
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 466, // 0x1d2
                }
        };
        static readonly byte[] s_dataContracts_Hashtable = null;
        // Count=21
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractEntry[] s_dataContracts = new global::DataContractEntry[] {
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 0, // boolean
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 0, // boolean
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 0, // boolean
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.BooleanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 93, // base64Binary
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 93, // base64Binary
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 93, // base64Binary
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ByteArrayDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 106, // char
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 106, // char
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 106, // char
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.CharDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 111, // dateTime
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 111, // dateTime
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 111, // dateTime
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DateTimeDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 120, // decimal
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 120, // decimal
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 120, // decimal
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DecimalDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 128, // double
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 128, // double
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 128, // double
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DoubleDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 135, // float
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 135, // float
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 135, // float
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.FloatDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 141, // guid
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 141, // guid
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 141, // guid
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.GuidDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 146, // int
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 146, // int
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 146, // int
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.IntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 150, // long
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 150, // long
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 150, // long
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.LongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 155, // anyType
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 155, // anyType
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 155, // anyType
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.ObjectDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 163, // QName
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 163, // QName
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 163, // QName
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                    "11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Xml.ReaderWriter, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
                                    "11d50a3a")),
                    },
                    Kind = global::DataContractKind.QNameDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 169, // short
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 169, // short
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 169, // short
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 175, // byte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 175, // byte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 175, // byte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.SignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 180, // string
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 180, // string
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 180, // string
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.StringDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 187, // duration
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 187, // duration
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 187, // duration
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.TimeSpanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 196, // unsignedByte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 196, // unsignedByte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 196, // unsignedByte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 209, // unsignedInt
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 209, // unsignedInt
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 209, // unsignedInt
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedIntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 221, // unsignedLong
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 221, // unsignedLong
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 221, // unsignedLong
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedLongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 234, // unsignedShort
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 234, // unsignedShort
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 234, // unsignedShort
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 248, // anyURI
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 248, // anyURI
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 248, // anyURI
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.UriDataContract,
                }
        };
        static readonly byte[] s_classDataContracts_Hashtable = null;
        // Count=104
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::ClassDataContractEntry[] s_classDataContracts = new global::ClassDataContractEntry[] {
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 255, // DevicePortalException.HttpErrorResponse
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 255, // DevicePortalException.HttpErrorResponse
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 255, // DevicePortalException.HttpErrorResponse
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortalException+HttpErrorResponse, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1,
                    ContractNamespacesListIndex = 7,
                    MemberNamesListIndex = 9,
                    MemberNamespacesListIndex = 15,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 371, // DevicePortal.HolographicSimulationPlaybackSessionState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 371, // DevicePortal.HolographicSimulationPlaybackSessionState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 371, // DevicePortal.HolographicSimulationPlaybackSessionState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                    "rapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackSessionState, WindowsDevicePortalW" +
                                    "rapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 21,
                    ContractNamespacesListIndex = 23,
                    MemberNamesListIndex = 25,
                    MemberNamespacesListIndex = 27,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 426, // DevicePortal.HolographicSimulationError
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 426, // DevicePortal.HolographicSimulationError
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 426, // DevicePortal.HolographicSimulationError
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationError, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 29,
                    ContractNamespacesListIndex = 31,
                    MemberNamesListIndex = 33,
                    MemberNamespacesListIndex = 35,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 466, // DevicePortal.EtwEvents
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 466, // DevicePortal.EtwEvents
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 466, // DevicePortal.EtwEvents
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwEvents, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 37,
                    ContractNamespacesListIndex = 40,
                    MemberNamesListIndex = 42,
                    MemberNamespacesListIndex = 45,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 614, // KeyValueOfstringstring
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 614, // KeyValueOfstringstring
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 614, // KeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Runtime.Serialization.KeyValue`2, System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neut" +
                                    "ral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 48,
                    ContractNamespacesListIndex = 51,
                    MemberNamesListIndex = 53,
                    MemberNamespacesListIndex = 56,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 647, // KeyValuePairOfstringstring
                        NamespaceIndex = 674, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        StableNameIndex = 647, // KeyValuePairOfstringstring
                        StableNameNamespaceIndex = 674, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        TopLevelElementNameIndex = 647, // KeyValuePairOfstringstring
                        TopLevelElementNamespaceIndex = 674, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.KeyValuePair`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=b03f5f7f11d50a3a")),
                    },
                    ChildElementNamespacesListIndex = 59,
                    ContractNamespacesListIndex = 62,
                    MemberNamesListIndex = 64,
                    MemberNamespacesListIndex = 67,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 741, // DevicePortal.RunningProcesses
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 741, // DevicePortal.RunningProcesses
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 741, // DevicePortal.RunningProcesses
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunningProcesses, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 70,
                    ContractNamespacesListIndex = 72,
                    MemberNamesListIndex = 74,
                    MemberNamespacesListIndex = 76,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 809, // DevicePortal.DeviceProcessInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 809, // DevicePortal.DeviceProcessInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 809, // DevicePortal.DeviceProcessInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 78,
                    ContractNamespacesListIndex = 95,
                    MemberNamesListIndex = 97,
                    MemberNamespacesListIndex = 114,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 840, // DevicePortal.AppVersion
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 840, // DevicePortal.AppVersion
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 840, // DevicePortal.AppVersion
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppVersion, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 131,
                    ContractNamespacesListIndex = 136,
                    MemberNamesListIndex = 138,
                    MemberNamespacesListIndex = 143,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 864, // DevicePortal.SystemPerformanceInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 864, // DevicePortal.SystemPerformanceInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 864, // DevicePortal.SystemPerformanceInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SystemPerformanceInformation, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 148,
                    ContractNamespacesListIndex = 163,
                    MemberNamesListIndex = 165,
                    MemberNamespacesListIndex = 180,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 906, // DevicePortal.GpuPerformanceData
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 906, // DevicePortal.GpuPerformanceData
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 906, // DevicePortal.GpuPerformanceData
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuPerformanceData, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 195,
                    ContractNamespacesListIndex = 197,
                    MemberNamesListIndex = 199,
                    MemberNamespacesListIndex = 201,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 969, // DevicePortal.GpuAdapter
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 969, // DevicePortal.GpuAdapter
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 969, // DevicePortal.GpuAdapter
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 203,
                    ContractNamespacesListIndex = 210,
                    MemberNamesListIndex = 212,
                    MemberNamespacesListIndex = 219,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1006, // DevicePortal.NetworkPerformanceData
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1006, // DevicePortal.NetworkPerformanceData
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1006, // DevicePortal.NetworkPerformanceData
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkPerformanceData, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 226,
                    ContractNamespacesListIndex = 229,
                    MemberNamesListIndex = 231,
                    MemberNamespacesListIndex = 234,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1042, // DevicePortal.AvailableBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1042, // DevicePortal.AvailableBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1042, // DevicePortal.AvailableBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                    "rsalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AvailableBluetoothDevicesInfo, WindowsDevicePortalWrapper.Unive" +
                                    "rsalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 237,
                    ContractNamespacesListIndex = 239,
                    MemberNamesListIndex = 241,
                    MemberNamespacesListIndex = 243,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1125, // DevicePortal.BluetoothDeviceInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1125, // DevicePortal.BluetoothDeviceInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1125, // DevicePortal.BluetoothDeviceInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    ContractNamespacesListIndex = 245,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1158, // DevicePortal.PairedBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1158, // DevicePortal.PairedBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1158, // DevicePortal.PairedBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairedBluetoothDevicesInfo, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 247,
                    ContractNamespacesListIndex = 249,
                    MemberNamesListIndex = 251,
                    MemberNamespacesListIndex = 253,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1198, // DevicePortal.PairBluetoothDevicesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1198, // DevicePortal.PairBluetoothDevicesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1198, // DevicePortal.PairBluetoothDevicesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairBluetoothDevicesInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 255,
                    ContractNamespacesListIndex = 257,
                    MemberNamesListIndex = 259,
                    MemberNamespacesListIndex = 261,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1236, // DevicePortal.PairResult
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1236, // DevicePortal.PairResult
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1236, // DevicePortal.PairResult
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PairResult, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    ContractNamespacesListIndex = 263,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1260, // DevicePortal.OperatingSystemInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1260, // DevicePortal.OperatingSystemInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1260, // DevicePortal.OperatingSystemInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+OperatingSystemInformation, WindowsDevicePortalWrapper.Universa" +
                                    "lWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 265,
                    ContractNamespacesListIndex = 272,
                    MemberNamesListIndex = 274,
                    MemberNamespacesListIndex = 281,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1300, // DevicePortal.AppCrashDumpList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1300, // DevicePortal.AppCrashDumpList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1300, // DevicePortal.AppCrashDumpList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpList, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 288,
                    ContractNamespacesListIndex = 290,
                    MemberNamesListIndex = 292,
                    MemberNamespacesListIndex = 294,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1363, // DevicePortal.AppCrashDump
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1363, // DevicePortal.AppCrashDump
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1363, // DevicePortal.AppCrashDump
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 296,
                    ContractNamespacesListIndex = 301,
                    MemberNamesListIndex = 303,
                    MemberNamespacesListIndex = 308,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1389, // DevicePortal.AppCrashDumpSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1389, // DevicePortal.AppCrashDumpSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1389, // DevicePortal.AppCrashDumpSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDumpSettings, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 313,
                    ContractNamespacesListIndex = 315,
                    MemberNamesListIndex = 317,
                    MemberNamespacesListIndex = 319,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1423, // DevicePortal.AppPackages
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1423, // DevicePortal.AppPackages
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1423, // DevicePortal.AppPackages
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackages, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 321,
                    ContractNamespacesListIndex = 323,
                    MemberNamesListIndex = 325,
                    MemberNamespacesListIndex = 327,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1480, // DevicePortal.PackageInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1480, // DevicePortal.PackageInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1480, // DevicePortal.PackageInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 329,
                    ContractNamespacesListIndex = 337,
                    MemberNamesListIndex = 339,
                    MemberNamespacesListIndex = 347,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1505, // DevicePortal.PackageVersion
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1505, // DevicePortal.PackageVersion
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1505, // DevicePortal.PackageVersion
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageVersion, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 355,
                    ContractNamespacesListIndex = 360,
                    MemberNamesListIndex = 362,
                    MemberNamespacesListIndex = 367,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1533, // DevicePortal.KnownFolders
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1533, // DevicePortal.KnownFolders
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1533, // DevicePortal.KnownFolders
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+KnownFolders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 372,
                    ContractNamespacesListIndex = 374,
                    MemberNamesListIndex = 376,
                    MemberNamespacesListIndex = 378,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1573, // DevicePortal.FolderContents
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1573, // DevicePortal.FolderContents
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1573, // DevicePortal.FolderContents
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FolderContents, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 380,
                    ContractNamespacesListIndex = 382,
                    MemberNamesListIndex = 384,
                    MemberNamespacesListIndex = 386,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1645, // DevicePortal.FileOrFolderInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1645, // DevicePortal.FileOrFolderInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1645, // DevicePortal.FileOrFolderInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                    "ndows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                    "ndows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 388,
                    ContractNamespacesListIndex = 396,
                    MemberNamesListIndex = 398,
                    MemberNamespacesListIndex = 406,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1682, // DevicePortal.DeviceList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1682, // DevicePortal.DeviceList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1682, // DevicePortal.DeviceList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceList, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 414,
                    ContractNamespacesListIndex = 416,
                    MemberNamesListIndex = 418,
                    MemberNamespacesListIndex = 420,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1733, // DevicePortal.Device
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1733, // DevicePortal.Device
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1733, // DevicePortal.Device
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                    "9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                    "9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 422,
                    ContractNamespacesListIndex = 431,
                    MemberNamesListIndex = 433,
                    MemberNamespacesListIndex = 442,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1753, // DevicePortal.ServiceTags
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1753, // DevicePortal.ServiceTags
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1753, // DevicePortal.ServiceTags
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceTags, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 451,
                    ContractNamespacesListIndex = 453,
                    MemberNamesListIndex = 455,
                    MemberNamespacesListIndex = 457,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1778, // DumpFileList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1778, // DumpFileList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1778, // DumpFileList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, " +
                                    "Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileList, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, " +
                                    "Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 459,
                    ContractNamespacesListIndex = 461,
                    MemberNamesListIndex = 463,
                    MemberNamespacesListIndex = 465,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1807, // Dumpfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1807, // Dumpfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1807, // Dumpfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Cult" +
                                    "ure=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Cult" +
                                    "ure=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 467,
                    ContractNamespacesListIndex = 471,
                    MemberNamesListIndex = 473,
                    MemberNamespacesListIndex = 477,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1816, // DumpFileSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1816, // DumpFileSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1816, // DumpFileSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4" +
                                    ".0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4" +
                                    ".0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 481,
                    ContractNamespacesListIndex = 486,
                    MemberNamesListIndex = 488,
                    MemberNamespacesListIndex = 493,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1908, // DevicePortal.EtwProviders
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1908, // DevicePortal.EtwProviders
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1908, // DevicePortal.EtwProviders
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviders, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 503,
                    ContractNamespacesListIndex = 505,
                    MemberNamesListIndex = 507,
                    MemberNamespacesListIndex = 509,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1970, // DevicePortal.EtwProviderInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1970, // DevicePortal.EtwProviderInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1970, // DevicePortal.EtwProviderInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 511,
                    ContractNamespacesListIndex = 514,
                    MemberNamesListIndex = 516,
                    MemberNamespacesListIndex = 519,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1999, // DevicePortal.IpConfiguration
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1999, // DevicePortal.IpConfiguration
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1999, // DevicePortal.IpConfiguration
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpConfiguration, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 522,
                    ContractNamespacesListIndex = 524,
                    MemberNamesListIndex = 526,
                    MemberNamespacesListIndex = 528,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2067, // DevicePortal.NetworkAdapterInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2067, // DevicePortal.NetworkAdapterInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2067, // DevicePortal.NetworkAdapterInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 530,
                    ContractNamespacesListIndex = 539,
                    MemberNamesListIndex = 541,
                    MemberNamespacesListIndex = 550,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2099, // DevicePortal.Dhcp
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2099, // DevicePortal.Dhcp
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2099, // DevicePortal.Dhcp
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                    "4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Dhcp, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9." +
                                    "4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 559,
                    ContractNamespacesListIndex = 563,
                    MemberNamesListIndex = 565,
                    MemberNamespacesListIndex = 569,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2117, // DevicePortal.IpAddressInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2117, // DevicePortal.IpAddressInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2117, // DevicePortal.IpAddressInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 573,
                    ContractNamespacesListIndex = 576,
                    MemberNamesListIndex = 578,
                    MemberNamespacesListIndex = 581,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2178, // DevicePortal.DeviceOsFamily
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2178, // DevicePortal.DeviceOsFamily
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2178, // DevicePortal.DeviceOsFamily
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceOsFamily, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 584,
                    ContractNamespacesListIndex = 586,
                    MemberNamesListIndex = 588,
                    MemberNamespacesListIndex = 590,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2206, // DevicePortal.DeviceName
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2206, // DevicePortal.DeviceName
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2206, // DevicePortal.DeviceName
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceName, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 592,
                    ContractNamespacesListIndex = 594,
                    MemberNamesListIndex = 596,
                    MemberNamespacesListIndex = 598,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2230, // DevicePortal.ActivePowerScheme
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2230, // DevicePortal.ActivePowerScheme
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2230, // DevicePortal.ActivePowerScheme
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ActivePowerScheme, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 600,
                    ContractNamespacesListIndex = 602,
                    MemberNamesListIndex = 604,
                    MemberNamespacesListIndex = 606,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2261, // DevicePortal.BatteryState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2261, // DevicePortal.BatteryState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2261, // DevicePortal.BatteryState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BatteryState, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 608,
                    ContractNamespacesListIndex = 617,
                    MemberNamesListIndex = 619,
                    MemberNamespacesListIndex = 628,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2287, // DevicePortal.PowerState
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2287, // DevicePortal.PowerState
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2287, // DevicePortal.PowerState
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PowerState, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 637,
                    ContractNamespacesListIndex = 640,
                    MemberNamesListIndex = 642,
                    MemberNamespacesListIndex = 645,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2311, // DevicePortal.WifiInterfaces
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2311, // DevicePortal.WifiInterfaces
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2311, // DevicePortal.WifiInterfaces
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterfaces, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 648,
                    ContractNamespacesListIndex = 650,
                    MemberNamesListIndex = 652,
                    MemberNamespacesListIndex = 654,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2373, // DevicePortal.WifiInterface
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2373, // DevicePortal.WifiInterface
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2373, // DevicePortal.WifiInterface
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 656,
                    ContractNamespacesListIndex = 661,
                    MemberNamesListIndex = 663,
                    MemberNamespacesListIndex = 668,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2439, // DevicePortal.WifiNetworkProfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2439, // DevicePortal.WifiNetworkProfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2439, // DevicePortal.WifiNetworkProfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 673,
                    ContractNamespacesListIndex = 677,
                    MemberNamesListIndex = 679,
                    MemberNamespacesListIndex = 683,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2471, // DevicePortal.WifiNetworks
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2471, // DevicePortal.WifiNetworks
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2471, // DevicePortal.WifiNetworks
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworks, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 687,
                    ContractNamespacesListIndex = 689,
                    MemberNamesListIndex = 691,
                    MemberNamespacesListIndex = 693,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2533, // DevicePortal.WifiNetworkInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2533, // DevicePortal.WifiNetworkInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2533, // DevicePortal.WifiNetworkInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 695,
                    ContractNamespacesListIndex = 709,
                    MemberNamesListIndex = 711,
                    MemberNamespacesListIndex = 725,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2573, // DevicePortal.WerDeviceReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2573, // DevicePortal.WerDeviceReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2573, // DevicePortal.WerDeviceReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerDeviceReports, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 739,
                    ContractNamespacesListIndex = 741,
                    MemberNamesListIndex = 743,
                    MemberNamespacesListIndex = 745,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2638, // DevicePortal.WerUserReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2638, // DevicePortal.WerUserReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2638, // DevicePortal.WerUserReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                    "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 747,
                    ContractNamespacesListIndex = 750,
                    MemberNamesListIndex = 752,
                    MemberNamespacesListIndex = 755,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2707, // DevicePortal.WerReportInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2707, // DevicePortal.WerReportInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2707, // DevicePortal.WerReportInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 758,
                    ContractNamespacesListIndex = 762,
                    MemberNamesListIndex = 764,
                    MemberNamespacesListIndex = 768,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2741, // DevicePortal.WerFiles
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2741, // DevicePortal.WerFiles
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2741, // DevicePortal.WerFiles
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFiles, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 772,
                    ContractNamespacesListIndex = 774,
                    MemberNamesListIndex = 776,
                    MemberNamespacesListIndex = 778,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2802, // DevicePortal.WerFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2802, // DevicePortal.WerFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2802, // DevicePortal.WerFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 780,
                    ContractNamespacesListIndex = 783,
                    MemberNamesListIndex = 785,
                    MemberNamespacesListIndex = 788,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2834, // DevicePortal.HolographicServices
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2834, // DevicePortal.HolographicServices
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2834, // DevicePortal.HolographicServices
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicServices, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 791,
                    ContractNamespacesListIndex = 793,
                    MemberNamesListIndex = 795,
                    MemberNamespacesListIndex = 797,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2867, // DevicePortal.HolographicSoftwareStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2867, // DevicePortal.HolographicSoftwareStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2867, // DevicePortal.HolographicSoftwareStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSoftwareStatus, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 799,
                    ContractNamespacesListIndex = 806,
                    MemberNamesListIndex = 808,
                    MemberNamespacesListIndex = 815,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2906, // DevicePortal.ServiceStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2906, // DevicePortal.ServiceStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2906, // DevicePortal.ServiceStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ServiceStatus, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 822,
                    ContractNamespacesListIndex = 825,
                    MemberNamesListIndex = 827,
                    MemberNamespacesListIndex = 830,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2933, // DevicePortal.InterPupilaryDistance
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2933, // DevicePortal.InterPupilaryDistance
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2933, // DevicePortal.InterPupilaryDistance
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+InterPupilaryDistance, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 833,
                    ContractNamespacesListIndex = 835,
                    MemberNamesListIndex = 837,
                    MemberNamespacesListIndex = 839,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2968, // DevicePortal.WebManagementHttpSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2968, // DevicePortal.WebManagementHttpSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2968, // DevicePortal.WebManagementHttpSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WebManagementHttpSettings, WindowsDevicePortalWrapper.Universal" +
                                    "Windows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 841,
                    ContractNamespacesListIndex = 843,
                    MemberNamesListIndex = 845,
                    MemberNamespacesListIndex = 847,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3007, // DevicePortal.PerceptionSimulationControlStreamId
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3007, // DevicePortal.PerceptionSimulationControlStreamId
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3007, // DevicePortal.PerceptionSimulationControlStreamId
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                    ".UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlStreamId, WindowsDevicePortalWrapper" +
                                    ".UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 849,
                    ContractNamespacesListIndex = 851,
                    MemberNamesListIndex = 853,
                    MemberNamespacesListIndex = 855,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3056, // DevicePortal.PerceptionSimulationControlMode
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3056, // DevicePortal.PerceptionSimulationControlMode
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3056, // DevicePortal.PerceptionSimulationControlMode
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                    "versalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PerceptionSimulationControlMode, WindowsDevicePortalWrapper.Uni" +
                                    "versalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 857,
                    ContractNamespacesListIndex = 859,
                    MemberNamesListIndex = 861,
                    MemberNamespacesListIndex = 863,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3155, // DevicePortal.ThermalStage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3155, // DevicePortal.ThermalStage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3155, // DevicePortal.ThermalStage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ThermalStage, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 868,
                    ContractNamespacesListIndex = 870,
                    MemberNamesListIndex = 872,
                    MemberNamespacesListIndex = 874,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3181, // DevicePortal.MrcFileList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3181, // DevicePortal.MrcFileList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3181, // DevicePortal.MrcFileList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileList, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 876,
                    ContractNamespacesListIndex = 878,
                    MemberNamesListIndex = 880,
                    MemberNamespacesListIndex = 882,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3245, // DevicePortal.MrcFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3245, // DevicePortal.MrcFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3245, // DevicePortal.MrcFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 884,
                    ContractNamespacesListIndex = 888,
                    MemberNamesListIndex = 890,
                    MemberNamespacesListIndex = 894,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3277, // DevicePortal.MrcSettings
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3277, // DevicePortal.MrcSettings
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3277, // DevicePortal.MrcSettings
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSettings, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 898,
                    ContractNamespacesListIndex = 900,
                    MemberNamesListIndex = 902,
                    MemberNamespacesListIndex = 904,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3333, // DevicePortal.MrcSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3333, // DevicePortal.MrcSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3333, // DevicePortal.MrcSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 906,
                    ContractNamespacesListIndex = 909,
                    MemberNamesListIndex = 911,
                    MemberNamespacesListIndex = 914,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3357, // DevicePortal.MrcStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3357, // DevicePortal.MrcStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3357, // DevicePortal.MrcStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcStatus, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 917,
                    ContractNamespacesListIndex = 920,
                    MemberNamesListIndex = 922,
                    MemberNamespacesListIndex = 925,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3380, // DevicePortal.MrcProcessStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3380, // DevicePortal.MrcProcessStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3380, // DevicePortal.MrcProcessStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcProcessStatus, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 928,
                    ContractNamespacesListIndex = 930,
                    MemberNamesListIndex = 932,
                    MemberNamespacesListIndex = 934,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3410, // DevicePortal.HolographicSimulationDataTypes
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3410, // DevicePortal.HolographicSimulationDataTypes
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3410, // DevicePortal.HolographicSimulationDataTypes
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                    "ersalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationDataTypes, WindowsDevicePortalWrapper.Univ" +
                                    "ersalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 936,
                    ContractNamespacesListIndex = 941,
                    MemberNamesListIndex = 943,
                    MemberNamespacesListIndex = 948,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3454, // DevicePortal.HolographicSimulationPlaybackFiles
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3454, // DevicePortal.HolographicSimulationPlaybackFiles
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3454, // DevicePortal.HolographicSimulationPlaybackFiles
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                    "UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationPlaybackFiles, WindowsDevicePortalWrapper." +
                                    "UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 953,
                    ContractNamespacesListIndex = 955,
                    MemberNamesListIndex = 957,
                    MemberNamespacesListIndex = 959,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3502, // DevicePortal.HolographicSimulationRecordingStatus
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3502, // DevicePortal.HolographicSimulationRecordingStatus
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3502, // DevicePortal.HolographicSimulationRecordingStatus
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                    "r.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HolographicSimulationRecordingStatus, WindowsDevicePortalWrappe" +
                                    "r.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 961,
                    ContractNamespacesListIndex = 963,
                    MemberNamesListIndex = 965,
                    MemberNamespacesListIndex = 967,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3552, // DevicePortal.NullResponse
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3552, // DevicePortal.NullResponse
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3552, // DevicePortal.NullResponse
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NullResponse, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ContractNamespacesListIndex = 969,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3578, // DevicePortal.ErrorInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3578, // DevicePortal.ErrorInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3578, // DevicePortal.ErrorInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ErrorInformation, WindowsDevicePortalWrapper.UniversalWindows, " +
                                    "Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 971,
                    ContractNamespacesListIndex = 974,
                    MemberNamesListIndex = 976,
                    MemberNamespacesListIndex = 979,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3608, // DevicePortal.TpmSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3608, // DevicePortal.TpmSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3608, // DevicePortal.TpmSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 982,
                    ContractNamespacesListIndex = 990,
                    MemberNamesListIndex = 992,
                    MemberNamespacesListIndex = 1000,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3637, // DevicePortal.TpmAcpiTablesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3637, // DevicePortal.TpmAcpiTablesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3637, // DevicePortal.TpmAcpiTablesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAcpiTablesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1008,
                    ContractNamespacesListIndex = 1010,
                    MemberNamesListIndex = 1012,
                    MemberNamespacesListIndex = 1014,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3668, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3668, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3668, // DevicePortal.TpmLogicalDeviceSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmLogicalDeviceSettingsInfo, WindowsDevicePortalWrapper.Univer" +
                                    "salWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1016,
                    ContractNamespacesListIndex = 1019,
                    MemberNamesListIndex = 1021,
                    MemberNamespacesListIndex = 1024,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3710, // DevicePortal.TpmAzureTokenInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3710, // DevicePortal.TpmAzureTokenInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3710, // DevicePortal.TpmAzureTokenInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TpmAzureTokenInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1027,
                    ContractNamespacesListIndex = 1029,
                    MemberNamesListIndex = 1031,
                    MemberNamespacesListIndex = 1033,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3741, // DevicePortal.AppsListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3741, // DevicePortal.AppsListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3741, // DevicePortal.AppsListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppsListInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1035,
                    ContractNamespacesListIndex = 1038,
                    MemberNamesListIndex = 1040,
                    MemberNamespacesListIndex = 1043,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3798, // DevicePortal.AppPackage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3798, // DevicePortal.AppPackage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3798, // DevicePortal.AppPackage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1046,
                    ContractNamespacesListIndex = 1049,
                    MemberNamesListIndex = 1051,
                    MemberNamespacesListIndex = 1054,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3822, // DevicePortal.HeadlessAppsListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3822, // DevicePortal.HeadlessAppsListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3822, // DevicePortal.HeadlessAppsListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+HeadlessAppsListInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1057,
                    ContractNamespacesListIndex = 1059,
                    MemberNamesListIndex = 1061,
                    MemberNamespacesListIndex = 1063,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3856, // DevicePortal.AudioDeviceListInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3856, // DevicePortal.AudioDeviceListInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3856, // DevicePortal.AudioDeviceListInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AudioDeviceListInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1065,
                    ContractNamespacesListIndex = 1072,
                    MemberNamesListIndex = 1074,
                    MemberNamespacesListIndex = 1081,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3889, // DevicePortal.RunCommandOutputInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3889, // DevicePortal.RunCommandOutputInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3889, // DevicePortal.RunCommandOutputInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RunCommandOutputInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1088,
                    ContractNamespacesListIndex = 1090,
                    MemberNamesListIndex = 1092,
                    MemberNamespacesListIndex = 1094,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3923, // DevicePortal.IscInterfacesInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3923, // DevicePortal.IscInterfacesInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3923, // DevicePortal.IscInterfacesInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IscInterfacesInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                    " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1096,
                    ContractNamespacesListIndex = 1099,
                    MemberNamesListIndex = 1101,
                    MemberNamespacesListIndex = 1104,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3954, // DevicePortal.SoftAPSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3954, // DevicePortal.SoftAPSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3954, // DevicePortal.SoftAPSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SoftAPSettingsInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                    ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1107,
                    ContractNamespacesListIndex = 1111,
                    MemberNamesListIndex = 1113,
                    MemberNamespacesListIndex = 1117,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3986, // DevicePortal.AllJoynSettingsInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3986, // DevicePortal.AllJoynSettingsInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3986, // DevicePortal.AllJoynSettingsInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AllJoynSettingsInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1121,
                    ContractNamespacesListIndex = 1126,
                    MemberNamesListIndex = 1128,
                    MemberNamespacesListIndex = 1133,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4019, // DevicePortal.RemoteSettingsStatusInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4019, // DevicePortal.RemoteSettingsStatusInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4019, // DevicePortal.RemoteSettingsStatusInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+RemoteSettingsStatusInfo, WindowsDevicePortalWrapper.UniversalW" +
                                    "indows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1138,
                    ContractNamespacesListIndex = 1141,
                    MemberNamesListIndex = 1143,
                    MemberNamespacesListIndex = 1146,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4057, // DevicePortal.IoTOSInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4057, // DevicePortal.IoTOSInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4057, // DevicePortal.IoTOSInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IoTOSInfo, WindowsDevicePortalWrapper.UniversalWindows, Version" +
                                    "=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1149,
                    ContractNamespacesListIndex = 1153,
                    MemberNamesListIndex = 1155,
                    MemberNamespacesListIndex = 1159,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4080, // DevicePortal.TimezoneInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4080, // DevicePortal.TimezoneInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4080, // DevicePortal.TimezoneInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+TimezoneInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1163,
                    ContractNamespacesListIndex = 1166,
                    MemberNamesListIndex = 1168,
                    MemberNamespacesListIndex = 1171,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4106, // DevicePortal.Timezone
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4106, // DevicePortal.Timezone
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4106, // DevicePortal.Timezone
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1174,
                    ContractNamespacesListIndex = 1178,
                    MemberNamesListIndex = 1180,
                    MemberNamespacesListIndex = 1184,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4157, // DevicePortal.DateTimeInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4157, // DevicePortal.DateTimeInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4157, // DevicePortal.DateTimeInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeInfo, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                    "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1188,
                    ContractNamespacesListIndex = 1190,
                    MemberNamesListIndex = 1192,
                    MemberNamespacesListIndex = 1194,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4183, // DevicePortal.DateTimeDescription
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4183, // DevicePortal.DateTimeDescription
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4183, // DevicePortal.DateTimeDescription
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DateTimeDescription, WindowsDevicePortalWrapper.UniversalWindow" +
                                    "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1196,
                    ContractNamespacesListIndex = 1203,
                    MemberNamesListIndex = 1205,
                    MemberNamespacesListIndex = 1212,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4216, // DevicePortal.ControllerDriverInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4216, // DevicePortal.ControllerDriverInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4216, // DevicePortal.ControllerDriverInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+ControllerDriverInfo, WindowsDevicePortalWrapper.UniversalWindo" +
                                    "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1219,
                    ContractNamespacesListIndex = 1223,
                    MemberNamesListIndex = 1225,
                    MemberNamespacesListIndex = 1229,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4250, // DevicePortal.DisplayOrientationInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4250, // DevicePortal.DisplayOrientationInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4250, // DevicePortal.DisplayOrientationInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayOrientationInfo, WindowsDevicePortalWrapper.UniversalWin" +
                                    "dows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1233,
                    ContractNamespacesListIndex = 1235,
                    MemberNamesListIndex = 1237,
                    MemberNamespacesListIndex = 1239,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4286, // DevicePortal.DisplayResolutionInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4286, // DevicePortal.DisplayResolutionInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4286, // DevicePortal.DisplayResolutionInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DisplayResolutionInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1241,
                    ContractNamespacesListIndex = 1244,
                    MemberNamesListIndex = 1246,
                    MemberNamespacesListIndex = 1249,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4321, // DevicePortal.Resolution
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4321, // DevicePortal.Resolution
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4321, // DevicePortal.Resolution
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1252,
                    ContractNamespacesListIndex = 1255,
                    MemberNamesListIndex = 1257,
                    MemberNamespacesListIndex = 1260,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4376, // DevicePortal.StatusInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4376, // DevicePortal.StatusInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4376, // DevicePortal.StatusInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+StatusInfo, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                    "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1263,
                    ContractNamespacesListIndex = 1270,
                    MemberNamesListIndex = 1272,
                    MemberNamespacesListIndex = 1279,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4400, // DevicePortal.UpdateInstallTimeInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4400, // DevicePortal.UpdateInstallTimeInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4400, // DevicePortal.UpdateInstallTimeInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UpdateInstallTimeInfo, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    ContractNamespacesListIndex = 1286,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4435, // DevicePortal.Sandbox
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4435, // DevicePortal.Sandbox
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4435, // DevicePortal.Sandbox
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Sandbox, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1288,
                    ContractNamespacesListIndex = 1290,
                    MemberNamesListIndex = 1292,
                    MemberNamespacesListIndex = 1294,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4456, // DevicePortal.SmbInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4456, // DevicePortal.SmbInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4456, // DevicePortal.SmbInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SmbInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0" +
                                    ".9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1296,
                    ContractNamespacesListIndex = 1300,
                    MemberNamesListIndex = 1302,
                    MemberNamespacesListIndex = 1306,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4477, // DevicePortal.UserList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4477, // DevicePortal.UserList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4477, // DevicePortal.UserList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserList, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1310,
                    ContractNamespacesListIndex = 1312,
                    MemberNamesListIndex = 1314,
                    MemberNamespacesListIndex = 1316,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4528, // DevicePortal.UserInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4528, // DevicePortal.UserInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4528, // DevicePortal.UserInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                    "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1318,
                    ContractNamespacesListIndex = 1328,
                    MemberNamesListIndex = 1330,
                    MemberNamespacesListIndex = 1340,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4550, // DevicePortal.XboxSettingList
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4550, // DevicePortal.XboxSettingList
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4550, // DevicePortal.XboxSettingList
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSettingList, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                    "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1350,
                    ContractNamespacesListIndex = 1352,
                    MemberNamesListIndex = 1354,
                    MemberNamespacesListIndex = 1356,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4611, // DevicePortal.XboxSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4611, // DevicePortal.XboxSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4611, // DevicePortal.XboxSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                    "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    HasDataContract = true,
                    ChildElementNamespacesListIndex = 1358,
                    ContractNamespacesListIndex = 1363,
                    MemberNamesListIndex = 1365,
                    MemberNamespacesListIndex = 1370,
                }
        };
        static readonly byte[] s_collectionDataContracts_Hashtable = null;
        // Count=30
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::CollectionDataContractEntry[] s_collectionDataContracts = new global::CollectionDataContractEntry[] {
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 489, // ArrayOfArrayOfKeyValueOfstringstring
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 489, // ArrayOfArrayOfKeyValueOfstringstring
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 489, // ArrayOfArrayOfKeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 584, // ArrayOfKeyValueOfstringstring
                    KeyNameIndex = -1,
                    ItemNameIndex = 584, // ArrayOfKeyValueOfstringstring
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 584, // ArrayOfKeyValueOfstringstring
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 584, // ArrayOfKeyValueOfstringstring
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 584, // ArrayOfKeyValueOfstringstring
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.Dictionary`2, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b" +
                                    "03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 614, // KeyValueOfstringstring
                    KeyNameIndex = 637, // Key
                    ItemNameIndex = 614, // KeyValueOfstringstring
                    ValueNameIndex = 641, // Value
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericDictionary,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.1.1, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 771, // ArrayOfDevicePortal.DeviceProcessInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 771, // ArrayOfDevicePortal.DeviceProcessInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 771, // ArrayOfDevicePortal.DeviceProcessInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 809, // DevicePortal.DeviceProcessInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 809, // DevicePortal.DeviceProcessInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+DeviceProcessInfo, WindowsDevicePortalWrapper.UniversalWindows," +
                                " Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 938, // ArrayOfDevicePortal.GpuAdapter
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 938, // ArrayOfDevicePortal.GpuAdapter
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 938, // ArrayOfDevicePortal.GpuAdapter
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 969, // DevicePortal.GpuAdapter
                    KeyNameIndex = -1,
                    ItemNameIndex = 969, // DevicePortal.GpuAdapter
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+GpuAdapter, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 993, // ArrayOffloat
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 993, // ArrayOffloat
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 993, // ArrayOffloat
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Single, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Single, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 135, // float
                    KeyNameIndex = -1,
                    ItemNameIndex = 135, // float
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1085, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1085, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1085, // ArrayOfDevicePortal.BluetoothDeviceInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1125, // DevicePortal.BluetoothDeviceInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1125, // DevicePortal.BluetoothDeviceInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+BluetoothDeviceInfo, WindowsDevicePortalWrapper.UniversalWindow" +
                                "s, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1330, // ArrayOfDevicePortal.AppCrashDump
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1330, // ArrayOfDevicePortal.AppCrashDump
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1330, // ArrayOfDevicePortal.AppCrashDump
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1363, // DevicePortal.AppCrashDump
                    KeyNameIndex = -1,
                    ItemNameIndex = 1363, // DevicePortal.AppCrashDump
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppCrashDump, WindowsDevicePortalWrapper.UniversalWindows, Vers" +
                                "ion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1448, // ArrayOfDevicePortal.PackageInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1448, // ArrayOfDevicePortal.PackageInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1448, // ArrayOfDevicePortal.PackageInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1480, // DevicePortal.PackageInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1480, // DevicePortal.PackageInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+PackageInfo, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1559, // ArrayOfstring
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 1559, // ArrayOfstring
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 1559, // ArrayOfstring
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publ" +
                                    "icKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11" +
                                    "d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 180, // string
                    KeyNameIndex = -1,
                    ItemNameIndex = 180, // string
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1601, // ArrayOfDevicePortal.FileOrFolderInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1601, // ArrayOfDevicePortal.FileOrFolderInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1601, // ArrayOfDevicePortal.FileOrFolderInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1645, // DevicePortal.FileOrFolderInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 1645, // DevicePortal.FileOrFolderInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+FileOrFolderInformation, WindowsDevicePortalWrapper.UniversalWi" +
                                "ndows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1706, // ArrayOfDevicePortal.Device
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1706, // ArrayOfDevicePortal.Device
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1706, // ArrayOfDevicePortal.Device
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1733, // DevicePortal.Device
                    KeyNameIndex = -1,
                    ItemNameIndex = 1733, // DevicePortal.Device
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Device, WindowsDevicePortalWrapper.UniversalWindows, Version=0." +
                                "9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1791, // ArrayOfDumpfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1791, // ArrayOfDumpfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1791, // ArrayOfDumpfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1807, // Dumpfile
                    KeyNameIndex = -1,
                    ItemNameIndex = 1807, // Dumpfile
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.Dumpfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Cult" +
                                "ure=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 1934, // ArrayOfDevicePortal.EtwProviderInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1934, // ArrayOfDevicePortal.EtwProviderInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1934, // ArrayOfDevicePortal.EtwProviderInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 1970, // DevicePortal.EtwProviderInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 1970, // DevicePortal.EtwProviderInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+EtwProviderInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2028, // ArrayOfDevicePortal.NetworkAdapterInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2028, // ArrayOfDevicePortal.NetworkAdapterInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2028, // ArrayOfDevicePortal.NetworkAdapterInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2067, // DevicePortal.NetworkAdapterInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 2067, // DevicePortal.NetworkAdapterInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+NetworkAdapterInfo, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2144, // ArrayOfDevicePortal.IpAddressInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2144, // ArrayOfDevicePortal.IpAddressInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2144, // ArrayOfDevicePortal.IpAddressInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2117, // DevicePortal.IpAddressInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 2117, // DevicePortal.IpAddressInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+IpAddressInfo, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2339, // ArrayOfDevicePortal.WifiInterface
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2339, // ArrayOfDevicePortal.WifiInterface
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2339, // ArrayOfDevicePortal.WifiInterface
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2373, // DevicePortal.WifiInterface
                    KeyNameIndex = -1,
                    ItemNameIndex = 2373, // DevicePortal.WifiInterface
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiInterface, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2400, // ArrayOfDevicePortal.WifiNetworkProfile
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2400, // ArrayOfDevicePortal.WifiNetworkProfile
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2400, // ArrayOfDevicePortal.WifiNetworkProfile
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2439, // DevicePortal.WifiNetworkProfile
                    KeyNameIndex = -1,
                    ItemNameIndex = 2439, // DevicePortal.WifiNetworkProfile
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkProfile, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2497, // ArrayOfDevicePortal.WifiNetworkInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2497, // ArrayOfDevicePortal.WifiNetworkInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2497, // ArrayOfDevicePortal.WifiNetworkInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2533, // DevicePortal.WifiNetworkInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 2533, // DevicePortal.WifiNetworkInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WifiNetworkInfo, WindowsDevicePortalWrapper.UniversalWindows, V" +
                                "ersion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2562, // ArrayOfint
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 2562, // ArrayOfint
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 2562, // ArrayOfint
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1[[System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=b03f5f7f11d50a3a]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 146, // int
                    KeyNameIndex = -1,
                    ItemNameIndex = 146, // int
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2603, // ArrayOfDevicePortal.WerUserReports
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2603, // ArrayOfDevicePortal.WerUserReports
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2603, // ArrayOfDevicePortal.WerUserReports
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2638, // DevicePortal.WerUserReports
                    KeyNameIndex = -1,
                    ItemNameIndex = 2638, // DevicePortal.WerUserReports
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerUserReports, WindowsDevicePortalWrapper.UniversalWindows, Ve" +
                                "rsion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2666, // ArrayOfDevicePortal.WerReportInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2666, // ArrayOfDevicePortal.WerReportInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2666, // ArrayOfDevicePortal.WerReportInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2707, // DevicePortal.WerReportInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 2707, // DevicePortal.WerReportInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerReportInformation, WindowsDevicePortalWrapper.UniversalWindo" +
                                "ws, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2763, // ArrayOfDevicePortal.WerFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 2763, // ArrayOfDevicePortal.WerFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 2763, // ArrayOfDevicePortal.WerFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 2802, // DevicePortal.WerFileInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 2802, // DevicePortal.WerFileInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+WerFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3206, // ArrayOfDevicePortal.MrcFileInformation
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3206, // ArrayOfDevicePortal.MrcFileInformation
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3206, // ArrayOfDevicePortal.MrcFileInformation
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3245, // DevicePortal.MrcFileInformation
                    KeyNameIndex = -1,
                    ItemNameIndex = 3245, // DevicePortal.MrcFileInformation
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcFileInformation, WindowsDevicePortalWrapper.UniversalWindows" +
                                ", Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3302, // ArrayOfDevicePortal.MrcSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3302, // ArrayOfDevicePortal.MrcSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3302, // ArrayOfDevicePortal.MrcSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3333, // DevicePortal.MrcSetting
                    KeyNameIndex = -1,
                    ItemNameIndex = 3333, // DevicePortal.MrcSetting
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+MrcSetting, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 3767, // ArrayOfDevicePortal.AppPackage
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3767, // ArrayOfDevicePortal.AppPackage
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3767, // ArrayOfDevicePortal.AppPackage
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 3798, // DevicePortal.AppPackage
                    KeyNameIndex = -1,
                    ItemNameIndex = 3798, // DevicePortal.AppPackage
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+AppPackage, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4128, // ArrayOfDevicePortal.Timezone
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4128, // ArrayOfDevicePortal.Timezone
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4128, // ArrayOfDevicePortal.Timezone
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4106, // DevicePortal.Timezone
                    KeyNameIndex = -1,
                    ItemNameIndex = 4106, // DevicePortal.Timezone
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Timezone, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4345, // ArrayOfDevicePortal.Resolution
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4345, // ArrayOfDevicePortal.Resolution
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4345, // ArrayOfDevicePortal.Resolution
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4321, // DevicePortal.Resolution
                    KeyNameIndex = -1,
                    ItemNameIndex = 4321, // DevicePortal.Resolution
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+Resolution, WindowsDevicePortalWrapper.UniversalWindows, Versio" +
                                "n=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4499, // ArrayOfDevicePortal.UserInfo
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4499, // ArrayOfDevicePortal.UserInfo
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4499, // ArrayOfDevicePortal.UserInfo
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4528, // DevicePortal.UserInfo
                    KeyNameIndex = -1,
                    ItemNameIndex = 4528, // DevicePortal.UserInfo
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+UserInfo, WindowsDevicePortalWrapper.UniversalWindows, Version=" +
                                "0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4579, // ArrayOfDevicePortal.XboxSetting
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 4579, // ArrayOfDevicePortal.XboxSetting
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 4579, // ArrayOfDevicePortal.XboxSetting
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null]], System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Collections, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7" +
                                    "f11d50a3a")),
                    },
                    CollectionItemNameIndex = 4611, // DevicePortal.XboxSetting
                    KeyNameIndex = -1,
                    ItemNameIndex = 4611, // DevicePortal.XboxSetting
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+XboxSetting, WindowsDevicePortalWrapper.UniversalWindows, Versi" +
                                "on=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 4636, // ArrayOfanyType
                        NamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 4636, // ArrayOfanyType
                        StableNameNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 4636, // ArrayOfanyType
                        TopLevelElementNamespaceIndex = 526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    CollectionItemNameIndex = 155, // anyType
                    KeyNameIndex = -1,
                    ItemNameIndex = 155, // anyType
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }
        };
        static readonly byte[] s_enumDataContracts_Hashtable = null;
        // Count=2
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::EnumDataContractEntry[] s_enumDataContracts = new global::EnumDataContractEntry[] {
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 1833, // DumpFileSettings.DumpTypes
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 1833, // DumpFileSettings.DumpTypes
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 1833, // DumpFileSettings.DumpTypes
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DumpFileSettings+DumpTypes, WindowsDevicePortalWrapper.UniversalWindows, Ver" +
                                    "sion=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 498,
                    MemberCount = 4,
                }, 
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 3101, // DevicePortal.SimulationControlMode
                        NamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        StableNameIndex = 3101, // DevicePortal.SimulationControlMode
                        StableNameNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        TopLevelElementNameIndex = 3101, // DevicePortal.SimulationControlMode
                        TopLevelElementNamespaceIndex = 295, // http://schemas.datacontract.org/2004/07/Microsoft.Tools.WindowsDevicePortal
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("Microsoft.Tools.WindowsDevicePortal.DevicePortal+SimulationControlMode, WindowsDevicePortalWrapper.UniversalWind" +
                                    "ows, Version=0.9.4.0, Culture=neutral, PublicKeyToken=null")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 865,
                    MemberCount = 2,
                    MemberListIndex = 4,
                }
        };
        static readonly byte[] s_xmlDataContracts_Hashtable = null;
        // Count=0
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::XmlDataContractEntry[] s_xmlDataContracts = new global::XmlDataContractEntry[0];
        static readonly byte[] s_jsonDelegatesList_Hashtable = null;
        // Count=136
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::JsonDelegateEntry[] s_jsonDelegatesList = new global::JsonDelegateEntry[] {
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 37,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type3.WriteDevicePortalException_HttpErrorResponseToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type2.ReadDevicePortalException_HttpErrorResponseFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 38,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type7.WriteDevicePortal_HolographicSimulationPlaybackSessionStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type6.ReadDevicePortal_HolographicSimulationPlaybackSessionStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 39,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type11.WriteDevicePortal_HolographicSimulationErrorToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type10.ReadDevicePortal_HolographicSimulationErrorFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 40,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type15.WriteDevicePortal_EtwEventsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type14.ReadDevicePortal_EtwEventsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 41,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type20.WriteArrayOfArrayOfKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type19.ReadArrayOfArrayOfKeyValueOfstringstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type21.ReadArrayOfArrayOfKeyValueOfstringstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 42,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type26.WriteArrayOfKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type25.ReadArrayOfKeyValueOfstringstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type27.ReadArrayOfKeyValueOfstringstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 43,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type31.WriteKeyValueOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type30.ReadKeyValueOfstringstringFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 45,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type35.WriteKeyValuePairOfstringstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type34.ReadKeyValuePairOfstringstringFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 47,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type39.WriteDevicePortal_RunningProcessesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type38.ReadDevicePortal_RunningProcessesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 48,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type44.WriteArrayOfDevicePortal_DeviceProcessInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type43.ReadArrayOfDevicePortal_DeviceProcessInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type45.ReadArrayOfDevicePortal_DeviceProcessInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 49,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type49.WriteDevicePortal_DeviceProcessInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type48.ReadDevicePortal_DeviceProcessInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 50,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type53.WriteDevicePortal_AppVersionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type52.ReadDevicePortal_AppVersionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 51,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type57.WriteDevicePortal_SystemPerformanceInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type56.ReadDevicePortal_SystemPerformanceInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 52,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type61.WriteDevicePortal_GpuPerformanceDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type60.ReadDevicePortal_GpuPerformanceDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 53,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type66.WriteArrayOfDevicePortal_GpuAdapterToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type65.ReadArrayOfDevicePortal_GpuAdapterFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type67.ReadArrayOfDevicePortal_GpuAdapterFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 54,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type71.WriteDevicePortal_GpuAdapterToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type70.ReadDevicePortal_GpuAdapterFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 55,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type76.WriteArrayOffloatToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type75.ReadArrayOffloatFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type77.ReadArrayOffloatFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 56,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type81.WriteDevicePortal_NetworkPerformanceDataToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type80.ReadDevicePortal_NetworkPerformanceDataFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 57,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type85.WriteDevicePortal_AvailableBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type84.ReadDevicePortal_AvailableBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 58,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type90.WriteArrayOfDevicePortal_BluetoothDeviceInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type89.ReadArrayOfDevicePortal_BluetoothDeviceInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type91.ReadArrayOfDevicePortal_BluetoothDeviceInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 59,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type95.WriteDevicePortal_BluetoothDeviceInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type94.ReadDevicePortal_BluetoothDeviceInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 60,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type99.WriteDevicePortal_PairedBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type98.ReadDevicePortal_PairedBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 61,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type103.WriteDevicePortal_PairBluetoothDevicesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type102.ReadDevicePortal_PairBluetoothDevicesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 62,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type107.WriteDevicePortal_PairResultToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type106.ReadDevicePortal_PairResultFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 63,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type111.WriteDevicePortal_OperatingSystemInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type110.ReadDevicePortal_OperatingSystemInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 64,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type115.WriteDevicePortal_AppCrashDumpListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type114.ReadDevicePortal_AppCrashDumpListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 65,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type120.WriteArrayOfDevicePortal_AppCrashDumpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type119.ReadArrayOfDevicePortal_AppCrashDumpFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type121.ReadArrayOfDevicePortal_AppCrashDumpFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 66,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type125.WriteDevicePortal_AppCrashDumpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type124.ReadDevicePortal_AppCrashDumpFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 67,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type129.WriteDevicePortal_AppCrashDumpSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type128.ReadDevicePortal_AppCrashDumpSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 68,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type133.WriteDevicePortal_AppPackagesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type132.ReadDevicePortal_AppPackagesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 69,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type138.WriteArrayOfDevicePortal_PackageInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type137.ReadArrayOfDevicePortal_PackageInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type139.ReadArrayOfDevicePortal_PackageInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 70,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type143.WriteDevicePortal_PackageInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type142.ReadDevicePortal_PackageInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 71,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type147.WriteDevicePortal_PackageVersionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type146.ReadDevicePortal_PackageVersionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 72,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type151.WriteDevicePortal_KnownFoldersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type150.ReadDevicePortal_KnownFoldersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 73,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type156.WriteArrayOfstringToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type155.ReadArrayOfstringFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type157.ReadArrayOfstringFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 74,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type161.WriteDevicePortal_FolderContentsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type160.ReadDevicePortal_FolderContentsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 75,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type166.WriteArrayOfDevicePortal_FileOrFolderInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type165.ReadArrayOfDevicePortal_FileOrFolderInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type167.ReadArrayOfDevicePortal_FileOrFolderInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 76,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type171.WriteDevicePortal_FileOrFolderInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type170.ReadDevicePortal_FileOrFolderInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 77,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type175.WriteDevicePortal_DeviceListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type174.ReadDevicePortal_DeviceListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 78,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type180.WriteArrayOfDevicePortal_DeviceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type179.ReadArrayOfDevicePortal_DeviceFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type181.ReadArrayOfDevicePortal_DeviceFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 79,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type185.WriteDevicePortal_DeviceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type184.ReadDevicePortal_DeviceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 80,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type189.WriteDevicePortal_ServiceTagsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type188.ReadDevicePortal_ServiceTagsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 81,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type193.WriteDumpFileListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type192.ReadDumpFileListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 82,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type198.WriteArrayOfDumpfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type197.ReadArrayOfDumpfileFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type199.ReadArrayOfDumpfileFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 83,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type203.WriteDumpfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type202.ReadDumpfileFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 84,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type207.WriteDumpFileSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type206.ReadDumpFileSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 85,
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 87,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type211.WriteDevicePortal_EtwProvidersToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type210.ReadDevicePortal_EtwProvidersFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 88,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type216.WriteArrayOfDevicePortal_EtwProviderInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type215.ReadArrayOfDevicePortal_EtwProviderInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type217.ReadArrayOfDevicePortal_EtwProviderInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 89,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type221.WriteDevicePortal_EtwProviderInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type220.ReadDevicePortal_EtwProviderInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 90,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type225.WriteDevicePortal_IpConfigurationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type224.ReadDevicePortal_IpConfigurationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 91,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type230.WriteArrayOfDevicePortal_NetworkAdapterInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type229.ReadArrayOfDevicePortal_NetworkAdapterInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type231.ReadArrayOfDevicePortal_NetworkAdapterInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 92,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type235.WriteDevicePortal_NetworkAdapterInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type234.ReadDevicePortal_NetworkAdapterInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 93,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type239.WriteDevicePortal_DhcpToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type238.ReadDevicePortal_DhcpFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 94,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type243.WriteDevicePortal_IpAddressInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type242.ReadDevicePortal_IpAddressInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 95,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type248.WriteArrayOfDevicePortal_IpAddressInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type247.ReadArrayOfDevicePortal_IpAddressInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type249.ReadArrayOfDevicePortal_IpAddressInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 96,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type253.WriteDevicePortal_DeviceOsFamilyToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type252.ReadDevicePortal_DeviceOsFamilyFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 97,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type257.WriteDevicePortal_DeviceNameToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type256.ReadDevicePortal_DeviceNameFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 98,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type261.WriteDevicePortal_ActivePowerSchemeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type260.ReadDevicePortal_ActivePowerSchemeFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 99,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type265.WriteDevicePortal_BatteryStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type264.ReadDevicePortal_BatteryStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 100,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type269.WriteDevicePortal_PowerStateToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type268.ReadDevicePortal_PowerStateFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 101,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type273.WriteDevicePortal_WifiInterfacesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type272.ReadDevicePortal_WifiInterfacesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 102,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type278.WriteArrayOfDevicePortal_WifiInterfaceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type277.ReadArrayOfDevicePortal_WifiInterfaceFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type279.ReadArrayOfDevicePortal_WifiInterfaceFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 103,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type283.WriteDevicePortal_WifiInterfaceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type282.ReadDevicePortal_WifiInterfaceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 104,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type288.WriteArrayOfDevicePortal_WifiNetworkProfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type287.ReadArrayOfDevicePortal_WifiNetworkProfileFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type289.ReadArrayOfDevicePortal_WifiNetworkProfileFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 105,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type293.WriteDevicePortal_WifiNetworkProfileToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type292.ReadDevicePortal_WifiNetworkProfileFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 106,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type297.WriteDevicePortal_WifiNetworksToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type296.ReadDevicePortal_WifiNetworksFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 107,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type302.WriteArrayOfDevicePortal_WifiNetworkInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type301.ReadArrayOfDevicePortal_WifiNetworkInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type303.ReadArrayOfDevicePortal_WifiNetworkInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 108,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type307.WriteDevicePortal_WifiNetworkInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type306.ReadDevicePortal_WifiNetworkInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 109,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type312.WriteArrayOfintToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type311.ReadArrayOfintFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type313.ReadArrayOfintFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 110,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type317.WriteDevicePortal_WerDeviceReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type316.ReadDevicePortal_WerDeviceReportsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 111,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type322.WriteArrayOfDevicePortal_WerUserReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type321.ReadArrayOfDevicePortal_WerUserReportsFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type323.ReadArrayOfDevicePortal_WerUserReportsFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 112,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type327.WriteDevicePortal_WerUserReportsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type326.ReadDevicePortal_WerUserReportsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 113,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type332.WriteArrayOfDevicePortal_WerReportInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type331.ReadArrayOfDevicePortal_WerReportInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type333.ReadArrayOfDevicePortal_WerReportInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 114,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type337.WriteDevicePortal_WerReportInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type336.ReadDevicePortal_WerReportInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 115,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type341.WriteDevicePortal_WerFilesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type340.ReadDevicePortal_WerFilesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 116,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type346.WriteArrayOfDevicePortal_WerFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type345.ReadArrayOfDevicePortal_WerFileInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type347.ReadArrayOfDevicePortal_WerFileInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 117,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type351.WriteDevicePortal_WerFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type350.ReadDevicePortal_WerFileInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 118,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type355.WriteDevicePortal_HolographicServicesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type354.ReadDevicePortal_HolographicServicesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 119,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type359.WriteDevicePortal_HolographicSoftwareStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type358.ReadDevicePortal_HolographicSoftwareStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 120,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type363.WriteDevicePortal_ServiceStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type362.ReadDevicePortal_ServiceStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 121,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type367.WriteDevicePortal_InterPupilaryDistanceToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type366.ReadDevicePortal_InterPupilaryDistanceFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 122,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type371.WriteDevicePortal_WebManagementHttpSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type370.ReadDevicePortal_WebManagementHttpSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 123,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type375.WriteDevicePortal_PerceptionSimulationControlStreamIdToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type374.ReadDevicePortal_PerceptionSimulationControlStreamIdFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 124,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type379.WriteDevicePortal_PerceptionSimulationControlModeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type378.ReadDevicePortal_PerceptionSimulationControlModeFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 125,
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 127,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type383.WriteDevicePortal_ThermalStageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type382.ReadDevicePortal_ThermalStageFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 128,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type387.WriteDevicePortal_MrcFileListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type386.ReadDevicePortal_MrcFileListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 129,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type392.WriteArrayOfDevicePortal_MrcFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type391.ReadArrayOfDevicePortal_MrcFileInformationFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type393.ReadArrayOfDevicePortal_MrcFileInformationFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 130,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type397.WriteDevicePortal_MrcFileInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type396.ReadDevicePortal_MrcFileInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 131,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type401.WriteDevicePortal_MrcSettingsToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type400.ReadDevicePortal_MrcSettingsFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 132,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type406.WriteArrayOfDevicePortal_MrcSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type405.ReadArrayOfDevicePortal_MrcSettingFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type407.ReadArrayOfDevicePortal_MrcSettingFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 133,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type411.WriteDevicePortal_MrcSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type410.ReadDevicePortal_MrcSettingFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 134,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type415.WriteDevicePortal_MrcStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type414.ReadDevicePortal_MrcStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 135,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type419.WriteDevicePortal_MrcProcessStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type418.ReadDevicePortal_MrcProcessStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 136,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type423.WriteDevicePortal_HolographicSimulationDataTypesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type422.ReadDevicePortal_HolographicSimulationDataTypesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 137,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type427.WriteDevicePortal_HolographicSimulationPlaybackFilesToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type426.ReadDevicePortal_HolographicSimulationPlaybackFilesFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 138,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type431.WriteDevicePortal_HolographicSimulationRecordingStatusToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type430.ReadDevicePortal_HolographicSimulationRecordingStatusFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 139,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type435.WriteDevicePortal_NullResponseToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type434.ReadDevicePortal_NullResponseFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 140,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type439.WriteDevicePortal_ErrorInformationToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type438.ReadDevicePortal_ErrorInformationFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 141,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type443.WriteDevicePortal_TpmSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type442.ReadDevicePortal_TpmSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 142,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type447.WriteDevicePortal_TpmAcpiTablesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type446.ReadDevicePortal_TpmAcpiTablesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 143,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type451.WriteDevicePortal_TpmLogicalDeviceSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type450.ReadDevicePortal_TpmLogicalDeviceSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 144,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type455.WriteDevicePortal_TpmAzureTokenInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type454.ReadDevicePortal_TpmAzureTokenInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 145,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type459.WriteDevicePortal_AppsListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type458.ReadDevicePortal_AppsListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 146,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type464.WriteArrayOfDevicePortal_AppPackageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type463.ReadArrayOfDevicePortal_AppPackageFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type465.ReadArrayOfDevicePortal_AppPackageFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 147,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type469.WriteDevicePortal_AppPackageToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type468.ReadDevicePortal_AppPackageFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 148,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type473.WriteDevicePortal_HeadlessAppsListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type472.ReadDevicePortal_HeadlessAppsListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 149,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type477.WriteDevicePortal_AudioDeviceListInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type476.ReadDevicePortal_AudioDeviceListInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 150,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type481.WriteDevicePortal_RunCommandOutputInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type480.ReadDevicePortal_RunCommandOutputInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 151,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type485.WriteDevicePortal_IscInterfacesInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type484.ReadDevicePortal_IscInterfacesInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 152,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type489.WriteDevicePortal_SoftAPSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type488.ReadDevicePortal_SoftAPSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 153,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type493.WriteDevicePortal_AllJoynSettingsInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type492.ReadDevicePortal_AllJoynSettingsInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 154,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type497.WriteDevicePortal_RemoteSettingsStatusInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type496.ReadDevicePortal_RemoteSettingsStatusInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 155,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type501.WriteDevicePortal_IoTOSInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type500.ReadDevicePortal_IoTOSInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 156,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type505.WriteDevicePortal_TimezoneInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type504.ReadDevicePortal_TimezoneInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 157,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type509.WriteDevicePortal_TimezoneToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type508.ReadDevicePortal_TimezoneFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 158,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type514.WriteArrayOfDevicePortal_TimezoneToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type513.ReadArrayOfDevicePortal_TimezoneFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type515.ReadArrayOfDevicePortal_TimezoneFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 159,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type519.WriteDevicePortal_DateTimeInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type518.ReadDevicePortal_DateTimeInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 160,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type523.WriteDevicePortal_DateTimeDescriptionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type522.ReadDevicePortal_DateTimeDescriptionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 161,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type527.WriteDevicePortal_ControllerDriverInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type526.ReadDevicePortal_ControllerDriverInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 162,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type531.WriteDevicePortal_DisplayOrientationInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type530.ReadDevicePortal_DisplayOrientationInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 163,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type535.WriteDevicePortal_DisplayResolutionInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type534.ReadDevicePortal_DisplayResolutionInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 164,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type539.WriteDevicePortal_ResolutionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type538.ReadDevicePortal_ResolutionFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 165,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type544.WriteArrayOfDevicePortal_ResolutionToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type543.ReadArrayOfDevicePortal_ResolutionFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type545.ReadArrayOfDevicePortal_ResolutionFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 166,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type549.WriteDevicePortal_StatusInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type548.ReadDevicePortal_StatusInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 167,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type553.WriteDevicePortal_UpdateInstallTimeInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type552.ReadDevicePortal_UpdateInstallTimeInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 168,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type557.WriteDevicePortal_SandboxToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type556.ReadDevicePortal_SandboxFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 169,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type561.WriteDevicePortal_SmbInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type560.ReadDevicePortal_SmbInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 170,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type565.WriteDevicePortal_UserListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type564.ReadDevicePortal_UserListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 171,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type570.WriteArrayOfDevicePortal_UserInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type569.ReadArrayOfDevicePortal_UserInfoFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type571.ReadArrayOfDevicePortal_UserInfoFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 172,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type575.WriteDevicePortal_UserInfoToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type574.ReadDevicePortal_UserInfoFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 173,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type579.WriteDevicePortal_XboxSettingListToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type578.ReadDevicePortal_XboxSettingListFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 174,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type584.WriteArrayOfDevicePortal_XboxSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type583.ReadArrayOfDevicePortal_XboxSettingFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type585.ReadArrayOfDevicePortal_XboxSettingFromJsonIsGetOnly),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 175,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassWriterDelegate>(global::Type589.WriteDevicePortal_XboxSettingToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatClassReaderDelegate>(global::Type588.ReadDevicePortal_XboxSettingFromJson),
                }, 
                new global::JsonDelegateEntry() {
                    DataContractMapIndex = 176,
                    IsCollection = true,
                    WriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionWriterDelegate>(global::Type594.WriteArrayOfanyTypeToJson),
                    ReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatCollectionReaderDelegate>(global::Type593.ReadArrayOfanyTypeFromJson),
                    GetOnlyReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.Json.JsonFormatGetOnlyCollectionReaderDelegate>(global::Type595.ReadArrayOfanyTypeFromJsonIsGetOnly),
                }
        };
        static char[] s_stringPool = new char[] {
            'b','o','o','l','e','a','n','\0','h','t','t','p',':','/','/','w','w','w','.','w','3','.','o','r','g','/','2','0','0','1',
            '/','X','M','L','S','c','h','e','m','a','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','m','i','c','r',
            'o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i','z','a','t','i','o',
            'n','/','\0','b','a','s','e','6','4','B','i','n','a','r','y','\0','c','h','a','r','\0','d','a','t','e','T','i','m','e','\0',
            'd','e','c','i','m','a','l','\0','d','o','u','b','l','e','\0','f','l','o','a','t','\0','g','u','i','d','\0','i','n','t','\0',
            'l','o','n','g','\0','a','n','y','T','y','p','e','\0','Q','N','a','m','e','\0','s','h','o','r','t','\0','b','y','t','e','\0',
            's','t','r','i','n','g','\0','d','u','r','a','t','i','o','n','\0','u','n','s','i','g','n','e','d','B','y','t','e','\0','u',
            'n','s','i','g','n','e','d','I','n','t','\0','u','n','s','i','g','n','e','d','L','o','n','g','\0','u','n','s','i','g','n',
            'e','d','S','h','o','r','t','\0','a','n','y','U','R','I','\0','D','e','v','i','c','e','P','o','r','t','a','l','E','x','c',
            'e','p','t','i','o','n','.','H','t','t','p','E','r','r','o','r','R','e','s','p','o','n','s','e','\0','h','t','t','p',':',
            '/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0',
            '4','/','0','7','/','M','i','c','r','o','s','o','f','t','.','T','o','o','l','s','.','W','i','n','d','o','w','s','D','e',
            'v','i','c','e','P','o','r','t','a','l','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r',
            'a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','P','l','a','y','b','a','c','k','S','e','s','s','i','o','n',
            'S','t','a','t','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c',
            'S','i','m','u','l','a','t','i','o','n','E','r','r','o','r','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','E',
            't','w','E','v','e','n','t','s','\0','A','r','r','a','y','O','f','A','r','r','a','y','O','f','K','e','y','V','a','l','u',
            'e','O','f','s','t','r','i','n','g','s','t','r','i','n','g','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s',
            '.','m','i','c','r','o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i',
            'z','a','t','i','o','n','/','A','r','r','a','y','s','\0','A','r','r','a','y','O','f','K','e','y','V','a','l','u','e','O',
            'f','s','t','r','i','n','g','s','t','r','i','n','g','\0','K','e','y','V','a','l','u','e','O','f','s','t','r','i','n','g',
            's','t','r','i','n','g','\0','K','e','y','\0','V','a','l','u','e','\0','K','e','y','V','a','l','u','e','P','a','i','r','O',
            'f','s','t','r','i','n','g','s','t','r','i','n','g','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','d',
            'a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','S','y','s','t','e','m',
            '.','C','o','l','l','e','c','t','i','o','n','s','.','G','e','n','e','r','i','c','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','R','u','n','n','i','n','g','P','r','o','c','e','s','s','e','s','\0','A','r','r','a','y','O','f','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','P','r','o','c','e','s','s','I','n','f','o','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','P','r','o','c','e','s','s','I','n','f','o','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','V','e','r','s','i','o','n','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','S','y','s','t','e','m','P','e','r','f','o','r','m','a','n','c','e','I','n','f','o','r','m',
            'a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','G','p','u','P','e','r','f','o','r','m','a',
            'n','c','e','D','a','t','a','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','G','p',
            'u','A','d','a','p','t','e','r','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','G','p','u','A','d','a','p','t',
            'e','r','\0','A','r','r','a','y','O','f','f','l','o','a','t','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','N',
            'e','t','w','o','r','k','P','e','r','f','o','r','m','a','n','c','e','D','a','t','a','\0','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','A','v','a','i','l','a','b','l','e','B','l','u','e','t','o','o','t','h','D','e','v','i','c','e','s',
            'I','n','f','o','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','B','l','u','e','t',
            'o','o','t','h','D','e','v','i','c','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','B','l',
            'u','e','t','o','o','t','h','D','e','v','i','c','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','P','a','i','r','e','d','B','l','u','e','t','o','o','t','h','D','e','v','i','c','e','s','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','P','a','i','r','B','l','u','e','t','o','o','t','h','D','e','v','i','c','e',
            's','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','a','i','r','R','e','s','u','l','t','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','O','p','e','r','a','t','i','n','g','S','y','s','t','e','m','I','n',
            'f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','C','r','a','s',
            'h','D','u','m','p','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'A','p','p','C','r','a','s','h','D','u','m','p','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','C',
            'r','a','s','h','D','u','m','p','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','C','r','a','s','h',
            'D','u','m','p','S','e','t','t','i','n','g','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','p','p','P',
            'a','c','k','a','g','e','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','P','a',
            'c','k','a','g','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','a','c','k','a','g','e',
            'I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','a','c','k','a','g','e','V','e','r','s','i',
            'o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','K','n','o','w','n','F','o','l','d','e','r','s','\0','A',
            'r','r','a','y','O','f','s','t','r','i','n','g','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','F','o','l','d',
            'e','r','C','o','n','t','e','n','t','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','F','i','l','e','O','r','F','o','l','d','e','r','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','F','i','l','e','O','r','F','o','l','d','e','r','I','n','f','o','r','m','a','t','i','o',
            'n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','L','i','s','t','\0','A','r','r','a',
            'y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','D','e','v','i','c','e','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','D','e','v','i','c','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S','e','r','v',
            'i','c','e','T','a','g','s','\0','D','u','m','p','F','i','l','e','L','i','s','t','\0','A','r','r','a','y','O','f','D','u',
            'm','p','f','i','l','e','\0','D','u','m','p','f','i','l','e','\0','D','u','m','p','F','i','l','e','S','e','t','t','i','n',
            'g','s','\0','D','u','m','p','F','i','l','e','S','e','t','t','i','n','g','s','.','D','u','m','p','T','y','p','e','s','\0',
            'D','i','s','a','b','l','e','d','\0','C','o','m','p','l','e','t','e','M','e','m','o','r','y','D','u','m','p','\0','K','e',
            'r','n','e','l','D','u','m','p','\0','M','i','n','i','d','u','m','p','\0','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','E','t','w','P','r','o','v','i','d','e','r','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r',
            't','a','l','.','E','t','w','P','r','o','v','i','d','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','E','t','w','P','r','o','v','i','d','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','I','p','C','o','n','f','i','g','u','r','a','t','i','o','n','\0','A','r','r','a','y','O','f','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','N','e','t','w','o','r','k','A','d','a','p','t','e','r','I','n','f','o','\0','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','N','e','t','w','o','r','k','A','d','a','p','t','e','r','I','n','f','o','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','D','h','c','p','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'I','p','A','d','d','r','e','s','s','I','n','f','o','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r',
            't','a','l','.','I','p','A','d','d','r','e','s','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','D','e','v','i','c','e','O','s','F','a','m','i','l','y','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D',
            'e','v','i','c','e','N','a','m','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A','c','t','i','v','e','P',
            'o','w','e','r','S','c','h','e','m','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','B','a','t','t','e','r',
            'y','S','t','a','t','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','o','w','e','r','S','t','a','t','e',
            '\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','I','n','t','e','r','f','a','c','e','s','\0','A',
            'r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','I','n','t','e','r','f','a',
            'c','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','I','n','t','e','r','f','a','c','e','\0',
            'A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w','o','r',
            'k','P','r','o','f','i','l','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e','t','w',
            'o','r','k','P','r','o','f','i','l','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i','N','e',
            't','w','o','r','k','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f',
            'i','N','e','t','w','o','r','k','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','i','f','i',
            'N','e','t','w','o','r','k','I','n','f','o','\0','A','r','r','a','y','O','f','i','n','t','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','W','e','r','D','e','v','i','c','e','R','e','p','o','r','t','s','\0','A','r','r','a','y','O','f',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','U','s','e','r','R','e','p','o','r','t','s','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','W','e','r','U','s','e','r','R','e','p','o','r','t','s','\0','A','r','r','a',
            'y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','R','e','p','o','r','t','I','n','f','o','r',
            'm','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','R','e','p','o','r','t','I',
            'n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','F','i','l',
            'e','s','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','F','i','l','e',
            'I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','W','e','r','F','i',
            'l','e','I','n','f','o','r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l',
            'o','g','r','a','p','h','i','c','S','e','r','v','i','c','e','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'H','o','l','o','g','r','a','p','h','i','c','S','o','f','t','w','a','r','e','S','t','a','t','u','s','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','S','e','r','v','i','c','e','S','t','a','t','u','s','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','I','n','t','e','r','P','u','p','i','l','a','r','y','D','i','s','t','a','n','c','e','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','W','e','b','M','a','n','a','g','e','m','e','n','t','H','t','t','p','S','e',
            't','t','i','n','g','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','P','e','r','c','e','p','t','i','o','n',
            'S','i','m','u','l','a','t','i','o','n','C','o','n','t','r','o','l','S','t','r','e','a','m','I','d','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','P','e','r','c','e','p','t','i','o','n','S','i','m','u','l','a','t','i','o','n','C',
            'o','n','t','r','o','l','M','o','d','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S','i','m','u','l','a',
            't','i','o','n','C','o','n','t','r','o','l','M','o','d','e','\0','D','e','f','a','u','l','t','\0','S','i','m','u','l','a',
            't','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','T','h','e','r','m','a','l','S','t','a','g','e',
            '\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','F','i','l','e','L','i','s','t','\0','A','r','r','a',
            'y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','F','i','l','e','I','n','f','o','r','m','a',
            't','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','F','i','l','e','I','n','f','o','r',
            'm','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','S','e','t','t','i','n','g',
            's','\0','A','r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','S','e','t','t','i',
            'n','g','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','M','r','c','S','e','t','t','i','n','g','\0','D','e','v',
            'i','c','e','P','o','r','t','a','l','.','M','r','c','S','t','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','M','r','c','P','r','o','c','e','s','s','S','t','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','D','a','t','a','T','y',
            'p','e','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','i',
            'm','u','l','a','t','i','o','n','P','l','a','y','b','a','c','k','F','i','l','e','s','\0','D','e','v','i','c','e','P','o',
            'r','t','a','l','.','H','o','l','o','g','r','a','p','h','i','c','S','i','m','u','l','a','t','i','o','n','R','e','c','o',
            'r','d','i','n','g','S','t','a','t','u','s','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','N','u','l','l','R',
            'e','s','p','o','n','s','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','E','r','r','o','r','I','n','f','o',
            'r','m','a','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','S','e','t','t','i','n',
            'g','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','A','c','p','i','T','a','b',
            'l','e','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','T','p','m','L','o','g','i','c','a',
            'l','D','e','v','i','c','e','S','e','t','t','i','n','g','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','T','p','m','A','z','u','r','e','T','o','k','e','n','I','n','f','o','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','A','p','p','s','L','i','s','t','I','n','f','o','\0','A','r','r','a','y','O','f','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','A','p','p','P','a','c','k','a','g','e','\0','D','e','v','i','c','e','P','o','r','t','a','l',
            '.','A','p','p','P','a','c','k','a','g','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','H','e','a','d','l',
            'e','s','s','A','p','p','s','L','i','s','t','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','A',
            'u','d','i','o','D','e','v','i','c','e','L','i','s','t','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a',
            'l','.','R','u','n','C','o','m','m','a','n','d','O','u','t','p','u','t','I','n','f','o','\0','D','e','v','i','c','e','P',
            'o','r','t','a','l','.','I','s','c','I','n','t','e','r','f','a','c','e','s','I','n','f','o','\0','D','e','v','i','c','e',
            'P','o','r','t','a','l','.','S','o','f','t','A','P','S','e','t','t','i','n','g','s','I','n','f','o','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','A','l','l','J','o','y','n','S','e','t','t','i','n','g','s','I','n','f','o','\0','D',
            'e','v','i','c','e','P','o','r','t','a','l','.','R','e','m','o','t','e','S','e','t','t','i','n','g','s','S','t','a','t',
            'u','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','I','o','T','O','S','I','n','f','o','\0',
            'D','e','v','i','c','e','P','o','r','t','a','l','.','T','i','m','e','z','o','n','e','I','n','f','o','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','T','i','m','e','z','o','n','e','\0','A','r','r','a','y','O','f','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','T','i','m','e','z','o','n','e','\0','D','e','v','i','c','e','P','o','r','t','a','l','.',
            'D','a','t','e','T','i','m','e','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','D','a','t','e',
            'T','i','m','e','D','e','s','c','r','i','p','t','i','o','n','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','C',
            'o','n','t','r','o','l','l','e','r','D','r','i','v','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','D','i','s','p','l','a','y','O','r','i','e','n','t','a','t','i','o','n','I','n','f','o','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','D','i','s','p','l','a','y','R','e','s','o','l','u','t','i','o','n','I','n','f','o',
            '\0','D','e','v','i','c','e','P','o','r','t','a','l','.','R','e','s','o','l','u','t','i','o','n','\0','A','r','r','a','y',
            'O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','R','e','s','o','l','u','t','i','o','n','\0','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','S','t','a','t','u','s','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','U','p','d','a','t','e','I','n','s','t','a','l','l','T','i','m','e','I','n','f','o','\0','D','e','v','i','c',
            'e','P','o','r','t','a','l','.','S','a','n','d','b','o','x','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','S',
            'm','b','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t','a','l','.','U','s','e','r','L','i','s','t','\0','A',
            'r','r','a','y','O','f','D','e','v','i','c','e','P','o','r','t','a','l','.','U','s','e','r','I','n','f','o','\0','D','e',
            'v','i','c','e','P','o','r','t','a','l','.','U','s','e','r','I','n','f','o','\0','D','e','v','i','c','e','P','o','r','t',
            'a','l','.','X','b','o','x','S','e','t','t','i','n','g','L','i','s','t','\0','A','r','r','a','y','O','f','D','e','v','i',
            'c','e','P','o','r','t','a','l','.','X','b','o','x','S','e','t','t','i','n','g','\0','D','e','v','i','c','e','P','o','r',
            't','a','l','.','X','b','o','x','S','e','t','t','i','n','g','\0','A','r','r','a','y','O','f','a','n','y','T','y','p','e',
            '\0','C','o','d','e','\0','E','r','r','o','r','C','o','d','e','\0','E','r','r','o','r','M','e','s','s','a','g','e','\0','R',
            'e','a','s','o','n','\0','S','u','c','c','e','s','s','\0','s','t','a','t','e','\0','E','v','e','n','t','s','\0','F','r','e',
            'q','u','e','n','c','y','\0','k','e','y','\0','v','a','l','u','e','\0','P','r','o','c','e','s','s','e','s','\0','A','p','p',
            'N','a','m','e','\0','C','P','U','U','s','a','g','e','\0','I','m','a','g','e','N','a','m','e','\0','I','s','R','u','n','n',
            'i','n','g','\0','I','s','X','A','P','\0','P','a','c','k','a','g','e','F','u','l','l','N','a','m','e','\0','P','a','g','e',
            'F','i','l','e','U','s','a','g','e','\0','P','r','i','v','a','t','e','W','o','r','k','i','n','g','S','e','t','\0','P','r',
            'o','c','e','s','s','I','d','\0','P','u','b','l','i','s','h','e','r','\0','S','e','s','s','i','o','n','I','d','\0','T','o',
            't','a','l','C','o','m','m','i','t','\0','U','s','e','r','N','a','m','e','\0','V','e','r','s','i','o','n','\0','V','i','r',
            't','u','a','l','S','i','z','e','\0','W','o','r','k','i','n','g','S','e','t','S','i','z','e','\0','B','u','i','l','d','\0',
            'M','a','j','o','r','\0','M','i','n','o','r','\0','R','e','v','i','s','i','o','n','\0','A','v','a','i','l','a','b','l','e',
            'P','a','g','e','s','\0','C','o','m','m','i','t','L','i','m','i','t','\0','C','o','m','m','i','t','t','e','d','P','a','g',
            'e','s','\0','C','p','u','L','o','a','d','\0','G','P','U','D','a','t','a','\0','I','O','O','t','h','e','r','S','p','e','e',
            'd','\0','I','O','R','e','a','d','S','p','e','e','d','\0','I','O','W','r','i','t','e','S','p','e','e','d','\0','N','e','t',
            'w','o','r','k','i','n','g','D','a','t','a','\0','N','o','n','P','a','g','e','d','P','o','o','l','P','a','g','e','s','\0',
            'P','a','g','e','S','i','z','e','\0','P','a','g','e','d','P','o','o','l','P','a','g','e','s','\0','T','o','t','a','l','I',
            'n','s','t','a','l','l','e','d','I','n','K','b','\0','T','o','t','a','l','P','a','g','e','s','\0','A','v','a','i','l','a',
            'b','l','e','A','d','a','p','t','e','r','s','\0','D','e','d','i','c','a','t','e','d','M','e','m','o','r','y','\0','D','e',
            'd','i','c','a','t','e','d','M','e','m','o','r','y','U','s','e','d','\0','D','e','s','c','r','i','p','t','i','o','n','\0',
            'E','n','g','i','n','e','s','U','t','i','l','i','z','a','t','i','o','n','\0','S','y','s','t','e','m','M','e','m','o','r',
            'y','\0','S','y','s','t','e','m','M','e','m','o','r','y','U','s','e','d','\0','N','e','t','w','o','r','k','I','n','B','y',
            't','e','s','\0','N','e','t','w','o','r','k','O','u','t','B','y','t','e','s','\0','A','v','a','i','l','a','b','l','e','D',
            'e','v','i','c','e','s','\0','P','a','i','r','e','d','D','e','v','i','c','e','s','\0','P','a','i','r','R','e','s','u','l',
            't','\0','C','o','m','p','u','t','e','r','N','a','m','e','\0','L','a','n','g','u','a','g','e','\0','O','s','E','d','i','t',
            'i','o','n','\0','O','s','E','d','i','t','i','o','n','I','d','\0','O','s','V','e','r','s','i','o','n','\0','P','l','a','t',
            'f','o','r','m','\0','C','r','a','s','h','D','u','m','p','s','\0','F','i','l','e','D','a','t','e','\0','F','i','l','e','N',
            'a','m','e','\0','F','i','l','e','S','i','z','e','\0','C','r','a','s','h','D','u','m','p','E','n','a','b','l','e','d','\0',
            'I','n','s','t','a','l','l','e','d','P','a','c','k','a','g','e','s','\0','N','a','m','e','\0','P','a','c','k','a','g','e',
            'F','a','m','i','l','y','N','a','m','e','\0','P','a','c','k','a','g','e','O','r','i','g','i','n','\0','P','a','c','k','a',
            'g','e','R','e','l','a','t','i','v','e','I','d','\0','K','n','o','w','n','F','o','l','d','e','r','s','\0','I','t','e','m',
            's','\0','C','u','r','r','e','n','t','D','i','r','\0','D','a','t','e','C','r','e','a','t','e','d','\0','I','d','\0','S','u',
            'b','P','a','t','h','\0','T','y','p','e','\0','D','e','v','i','c','e','L','i','s','t','\0','C','l','a','s','s','\0','F','r',
            'i','e','n','d','l','y','N','a','m','e','\0','I','D','\0','M','a','n','u','f','a','c','t','u','r','e','r','\0','P','a','r',
            'e','n','t','I','D','\0','P','r','o','b','l','e','m','C','o','d','e','\0','S','t','a','t','u','s','C','o','d','e','\0','t',
            'a','g','s','\0','D','u','m','p','F','i','l','e','s','\0','a','u','t','o','r','e','b','o','o','t','\0','d','u','m','p','t',
            'y','p','e','\0','m','a','x','d','u','m','p','c','o','u','n','t','\0','o','v','e','r','w','r','i','t','e','\0','P','r','o',
            'v','i','d','e','r','s','\0','G','U','I','D','\0','A','d','a','p','t','e','r','s','\0','D','H','C','P','\0','G','a','t','e',
            'w','a','y','s','\0','H','a','r','d','w','a','r','e','A','d','d','r','e','s','s','\0','I','n','d','e','x','\0','I','p','A',
            'd','d','r','e','s','s','e','s','\0','A','d','d','r','e','s','s','\0','L','e','a','s','e','E','x','p','i','r','e','s','\0',
            'L','e','a','s','e','O','b','t','a','i','n','e','d','\0','I','p','A','d','d','r','e','s','s','\0','M','a','s','k','\0','D',
            'e','v','i','c','e','T','y','p','e','\0','A','c','t','i','v','e','P','o','w','e','r','S','c','h','e','m','e','\0','A','c',
            'O','n','l','i','n','e','\0','B','a','t','t','e','r','y','P','r','e','s','e','n','t','\0','C','h','a','r','g','i','n','g',
            '\0','D','e','f','a','u','l','t','A','l','e','r','t','1','\0','D','e','f','a','u','l','t','A','l','e','r','t','2','\0','E',
            's','t','i','m','a','t','e','d','T','i','m','e','\0','M','a','x','i','m','u','m','C','a','p','a','c','i','t','y','\0','R',
            'e','m','a','i','n','i','n','g','C','a','p','a','c','i','t','y','\0','L','o','w','P','o','w','e','r','S','t','a','t','e',
            '\0','L','o','w','P','o','w','e','r','S','t','a','t','e','A','v','a','i','l','a','b','l','e','\0','I','n','t','e','r','f',
            'a','c','e','s','\0','P','r','o','f','i','l','e','s','L','i','s','t','\0','G','r','o','u','p','P','o','l','i','c','y','P',
            'r','o','f','i','l','e','\0','P','e','r','U','s','e','r','P','r','o','f','i','l','e','\0','A','v','a','i','l','a','b','l',
            'e','N','e','t','w','o','r','k','s','\0','A','l','r','e','a','d','y','C','o','n','n','e','c','t','e','d','\0','A','u','t',
            'h','e','n','t','i','c','a','t','i','o','n','A','l','g','o','r','i','t','h','m','\0','B','S','S','I','D','\0','C','h','a',
            'n','n','e','l','\0','C','i','p','h','e','r','A','l','g','o','r','i','t','h','m','\0','C','o','n','n','e','c','t','a','b',
            'l','e','\0','I','n','f','r','a','s','t','r','u','c','t','u','r','e','T','y','p','e','\0','P','h','y','s','i','c','a','l',
            'T','y','p','e','s','\0','P','r','o','f','i','l','e','A','v','a','i','l','a','b','l','e','\0','P','r','o','f','i','l','e',
            'N','a','m','e','\0','S','S','I','D','\0','S','e','c','u','r','i','t','y','E','n','a','b','l','e','d','\0','S','i','g','n',
            'a','l','Q','u','a','l','i','t','y','\0','W','e','r','R','e','p','o','r','t','s','\0','R','e','p','o','r','t','s','\0','U',
            's','e','r','\0','C','r','e','a','t','i','o','n','T','i','m','e','\0','F','i','l','e','s','\0','S','i','z','e','\0','S','o',
            'f','t','w','a','r','e','S','t','a','t','u','s','\0','d','w','m','.','e','x','e','\0','h','o','l','o','s','h','e','l','l',
            'a','p','p','.','e','x','e','\0','h','o','l','o','s','i','.','e','x','e','\0','m','i','x','e','d','r','e','a','l','i','t',
            'y','c','a','p','t','u','r','e','.','e','x','e','\0','s','i','h','o','s','t','.','e','x','e','\0','s','p','e','c','t','r',
            'u','m','.','e','x','e','\0','E','x','p','e','c','t','e','d','\0','O','b','s','e','r','v','e','d','\0','i','p','d','\0','h',
            't','t','p','s','R','e','q','u','i','r','e','d','\0','s','t','r','e','a','m','I','d','\0','m','o','d','e','\0','C','u','r',
            'r','e','n','t','S','t','a','g','e','\0','M','r','c','R','e','c','o','r','d','i','n','g','s','\0','M','r','c','S','e','t',
            't','i','n','g','s','\0','S','e','t','t','i','n','g','\0','I','s','R','e','c','o','r','d','i','n','g','\0','P','r','o','c',
            'e','s','s','S','t','a','t','u','s','\0','M','r','c','P','r','o','c','e','s','s','\0','e','n','v','i','r','o','n','m','e',
            'n','t','\0','h','a','n','d','s','\0','h','e','a','d','\0','s','p','a','t','i','a','l','M','a','p','p','i','n','g','\0','r',
            'e','c','o','r','d','i','n','g','s','\0','r','e','c','o','r','d','i','n','g','\0','S','t','a','t','u','s','\0','T','P','M',
            'F','a','m','i','l','y','\0','T','P','M','F','i','r','m','w','a','r','e','\0','T','P','M','M','a','n','u','f','a','c','t',
            'u','r','e','r','\0','T','P','M','R','e','v','i','s','i','o','n','\0','T','P','M','S','t','a','t','u','s','\0','T','P','M',
            'T','y','p','e','\0','T','P','M','V','e','n','d','o','r','\0','A','c','p','i','T','a','b','l','e','s','\0','A','z','u','r',
            'e','U','r','i','\0','D','e','v','i','c','e','I','d','\0','A','z','u','r','e','T','o','k','e','n','\0','A','p','p','P','a',
            'c','k','a','g','e','s','\0','D','e','f','a','u','l','t','A','p','p','\0','I','s','S','t','a','r','t','u','p','\0','C','a',
            'p','t','u','r','e','N','a','m','e','\0','C','a','p','t','u','r','e','V','o','l','u','m','e','\0','L','a','b','e','l','E',
            'r','r','o','r','C','o','d','e','\0','L','a','b','e','l','S','t','a','t','u','s','\0','R','e','n','d','e','r','N','a','m',
            'e','\0','R','e','n','d','e','r','V','o','l','u','m','e','\0','o','u','t','p','u','t','\0','P','r','i','v','a','t','e','I',
            'n','t','e','r','f','a','c','e','s','\0','P','u','b','l','i','c','I','n','t','e','r','f','a','c','e','s','\0','S','o','f',
            't','A','P','E','n','a','b','l','e','d','\0','S','o','f','t','A','p','P','a','s','s','w','o','r','d','\0','S','o','f','t',
            'A','p','S','s','i','d','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i','n','g','D','e','f','a','u','l',
            't','D','e','s','c','r','i','p','t','i','o','n','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i','n','g',
            'D','e','f','a','u','l','t','M','a','n','u','f','a','c','t','u','r','e','r','\0','A','l','l','J','o','y','n','O','n','b',
            'o','a','r','d','i','n','g','E','n','a','b','l','e','d','\0','A','l','l','J','o','y','n','O','n','b','o','a','r','d','i',
            'n','g','M','o','d','e','l','N','u','m','b','e','r','\0','I','s','S','c','h','e','d','u','l','e','d','\0','D','e','v','i',
            'c','e','M','o','d','e','l','\0','D','e','v','i','c','e','N','a','m','e','\0','O','S','V','e','r','s','i','o','n','\0','C',
            'u','r','r','e','n','t','\0','T','i','m','e','z','o','n','e','s','\0','D','a','y','\0','H','o','u','r','\0','M','i','n','u',
            't','e','\0','M','o','n','t','h','\0','S','e','c','o','n','d','\0','Y','e','a','r','\0','C','o','n','t','r','o','l','l','e',
            'r','s','D','r','i','v','e','r','s','\0','C','u','r','r','e','n','t','D','r','i','v','e','r','\0','R','e','q','u','e','s',
            't','R','e','b','o','o','t','\0','O','r','i','e','n','t','a','t','i','o','n','\0','R','e','s','o','l','u','t','i','o','n',
            's','\0','R','e','s','o','l','u','t','i','o','n','\0','l','a','s','t','C','h','e','c','k','T','i','m','e','\0','l','a','s',
            't','F','a','i','l','T','i','m','e','\0','l','a','s','t','U','p','d','a','t','e','T','i','m','e','\0','s','t','a','g','i',
            'n','g','P','r','o','g','r','e','s','s','\0','u','p','d','a','t','e','S','t','a','t','e','\0','u','p','d','a','t','e','S',
            't','a','t','u','s','M','e','s','s','a','g','e','\0','S','a','n','d','b','o','x','\0','P','a','s','s','w','o','r','d','\0',
            'P','a','t','h','\0','U','s','e','r','n','a','m','e','\0','U','s','e','r','s','\0','A','u','t','o','S','i','g','n','I','n',
            '\0','D','e','l','e','t','e','\0','E','m','a','i','l','A','d','d','r','e','s','s','\0','G','a','m','e','r','t','a','g','\0',
            'S','i','g','n','e','d','I','n','\0','S','p','o','n','s','o','r','e','d','U','s','e','r','\0','U','s','e','r','I','d','\0',
            'X','b','o','x','U','s','e','r','I','d','\0','S','e','t','t','i','n','g','s','\0','C','a','t','e','g','o','r','y','\0','R',
            'e','q','u','i','r','e','s','R','e','b','o','o','t','\0'};
    }
}
